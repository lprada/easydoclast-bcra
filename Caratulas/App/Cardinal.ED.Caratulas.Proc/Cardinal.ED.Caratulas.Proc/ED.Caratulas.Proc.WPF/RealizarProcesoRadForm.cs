﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace ED.Caratulas.Proc.WPF
{
    public partial class RealizarProcessRadForm : Telerik.WinControls.UI.RadForm
    {
        public RealizarProcessRadForm()
        {
            InitializeComponent();
        }

        public string CodLote { get; set; }
        
        public string CodigoLote
        {
            get
            {
               return  radMaskedEditBox1.Text.TrimEnd('_');
               // return radMaskedEditBox1.Text.Replace("_", "").Trim();
            }
        }

        private void RealizarProcessRadForm_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                radMaskedEditBox1.Text = CodLote;
                radMaskedEditBox1.Focus();
            }
        }

        private void inboundRadButton_Click(object sender, EventArgs e)
        {

        }
    }
}