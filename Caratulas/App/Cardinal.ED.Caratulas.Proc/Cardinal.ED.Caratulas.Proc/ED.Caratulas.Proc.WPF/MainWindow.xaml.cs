﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cardinal.EasyApps.ED.Api.Caratulas.Access;
using Cardinal.EasyApps.ED.Model;
using Telerik.Windows.Controls;
using System.ComponentModel;
using System.Configuration;
using Cardinal.EasyApps.ED.Common.Dal;
using EFRepository = Cardinal.EasyApps.ED.Dal.EntityFramework;
using NLog;

namespace ED.Caratulas.Proc.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RadRibbonWindow
    {
        private Cardinal.EasyApps.ED.Model.LoginAuth _loginAuth;
        IRepository _repository;
        private string apiUrl = string.Empty;
        private string apiVersion = string.Empty;
        private bool ldap = false;
        Logger logger = LogManager.GetCurrentClassLogger();

        public MainWindow()
        {
            InitializeComponent();
            System.Reflection.Assembly assem = System.Reflection.Assembly.GetAssembly(typeof(MainWindow));
            System.Reflection.AssemblyName assemName = assem.GetName();
            Version ver = assemName.Version;
            string version = "Version {0}.{1}.{2}.{3}";
            version = System.String.Format(version,
                ver.Major,
                ver.Minor,
                ver.Build,
                ver.Revision);

            ribbon.ApplicationName = String.Format("EasyDoc ({0})", version);
            var appConfig = ConfigurationManager.AppSettings;

            apiUrl = appConfig["ApiUrl"];
            apiVersion = appConfig["ApiVersion"];
            ldap = bool.Parse(appConfig["UseLdap"]);
            _repository = new EFRepository.Repository(ConfigurationManager.ConnectionStrings["EDEntities"].ConnectionString);
        }

        private void CerrarAplicacion()
        {
            Application.Current.Shutdown();
        }

        private void RadRibbonWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                Cardinal.EasyApps.ED.Api.Caratulas.Access.Auth authApi =
                    new Cardinal.EasyApps.ED.Api.Caratulas.Access.Auth(apiUrl, apiVersion);

                string token = string.Empty;
                int cantProcesos = 0;

                do
                {
                    Cardinal.EasyApps.ED.WindowsControls.Auth.LoginForm form = new Cardinal.EasyApps.ED.WindowsControls.Auth.LoginForm();

                    bool resDialog = form.ShowDialog() == System.Windows.Forms.DialogResult.OK;
                    if (resDialog)
                    {
                        token = authApi.GetLoginToken(form.UserName, form.Password, ldap);
                    }
                    else
                    {
                        token = string.Empty;
                        cantProcesos = 100;
                    }
                    cantProcesos++;
                    bool tokenValido = EsTokenValido(token);
                    if ((String.IsNullOrWhiteSpace(token) || !tokenValido) && resDialog)
                    {
                        token = string.Empty;
                        MessageBox.Show("Ingreso denegado. Vuelva a intentar.");
                    }
                }
                while (String.IsNullOrWhiteSpace(token) && cantProcesos < 3);
                if (String.IsNullOrWhiteSpace(token))
                {
                    MessageBox.Show("Ingreso denegado. Cerrando Aplicacion");
                    CerrarAplicacion();
                }
                _loginAuth = authApi.LoginToken;
                RefreshDatos();
            }
        }

        private void RefreshDatos()
        {
            if (_loginAuth != null)
            {
                //Obtengo de template las sin procesar, luego para cada una busco la informacion de la caratula
                List<CaratulaPendienteProceso> caratulasSinProcesar = new List<CaratulaPendienteProceso>();

                string lastCaratula = String.Empty;
                //try
                //{
                var caratulas = _repository.GetNumeroCaratulasPendientesProcesar();

                Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula caratulaApi =
                    new Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula(apiUrl, apiVersion, _loginAuth);

                foreach (var item in caratulas)
                {
                    CaratulaPendienteProceso caratula = new CaratulaPendienteProceso();
                    //MessageBox.Show("antes de obtener caratula");
                    var carRemoto = caratulaApi.GetCaratula(item.Key);
                    //MessageBox.Show("obtuvo caratula");
                    if (carRemoto != null)
                    {
                        caratula.codigo = carRemoto.codigo;
                        caratula.codigo_3d = carRemoto.codigo_3d;
                        caratula.DetalleCampos = carRemoto.DetalleCampos;
                        caratula.empresa = carRemoto.empresa;
                        caratula.fecha = carRemoto.fecha;
                        caratula.FechaInbound = carRemoto.FechaInbound;
                        caratula.FechaOutbound = carRemoto.FechaOutbound;
                        caratula.nombre_empresa = carRemoto.nombre_empresa;
                        caratula.plantilla = carRemoto.plantilla;
                        caratula.usuario = carRemoto.usuario;

                        //Datos del Lote
                        caratula.Lote = item.Value.CodigoLote;
                        caratula.FechaCreacion = item.Value.FechaCreacion.Value;

                        caratulasSinProcesar.Add(caratula);

                        lastCaratula = carRemoto.codigo.ToString();
                    }
                    else
                    {
                        MessageBox.Show(String.Format("El lote {0} contiene una carátula mal indexada, su valor es: {1}", item.Value.CodigoLote, item.Key));
                    }
                }
                //}
                //catch (Exception e)
                //{
                //    MessageBox.Show("Mensaje: " + e.Message);
                //    if (e.InnerException != null)
                //    {
                //        MessageBox.Show("Inner: " + e.InnerException.Message);
                //    }
                //}

                detalleRadDataPager.Source = caratulasSinProcesar;
                detalleGrid.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void RadRibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            RealizarProcessRadForm form = new RealizarProcessRadForm();

            if (detalleRadGridView.SelectedItems.Count() != 0)
            {
                var caratula = (CaratulaPendienteProceso)detalleRadGridView.SelectedCells[0].Item;
                form.CodLote = caratula.Lote;
                
                List<Cardinal.EasyApps.ED.Model.CaratulaCS> caratulas = new List<Cardinal.EasyApps.ED.Model.CaratulaCS>();
                Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula caratulaApi =
                    new Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula(apiUrl, apiVersion, _loginAuth);
                DateTime fechaProceso = DateTime.Now;
                if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    try
                    {
                        _repository.CompletarValorImagenesCb(caratula.Lote);
                        ProcesarLote(form, caratulaApi, fechaProceso, caratulas);

                        //Poner reporte de Outbound
                        Reports.ProcessReportRadForm formReport = new Reports.ProcessReportRadForm(caratulas);
                        formReport.ShowDialog();
                        form.Close();
                        //form = new RealizarProcessRadForm();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(String.Format("Error: {0}, Stack: {1}", ex.Message, ex.ToString()));
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                   
                }

                RefreshDatos();
            }
        }
        
        private void ProcesarLote(RealizarProcessRadForm form, Caratula caratulaApi, DateTime fechaProceso, List<Cardinal.EasyApps.ED.Model.CaratulaCS> caratulas)
        {
            var items = _repository.GetCaratulasPorLote(form.CodigoLote);



            foreach (var item in items)
            {
                var car = caratulaApi.GetCaratula(item.codigo.ToString());

                //Mover a Lote Nuevo en Nuevo Template
                //Creo un lote, mismo nombre mas _NROCAR
                GenerarLoteCaratula(form.CodigoLote, car);

                //Realizar Merge de Datos

                car.FechaProceso = fechaProceso;
                caratulas.Add(car);
            }
        }
  
        private void GenerarLoteCaratula(string codLote, Cardinal.EasyApps.ED.Model.CaratulaCS car)
        {
            //Mover a Lote Nuevo en Nuevo Template
            //Creo un lote, mismo nombre mas _NROCAR
            string loteCaratula = string.Format("{0}_CAR{1:00000000}", codLote, car.codigo);

            //mount
            var imagenesCaratula = _repository.GetImagenesCaratula(codLote, car.codigo).ToList();

            //Cargo los datos la caratula

            //se supone que todas las imagenes estan en el mismo lote
            string dirLoteCaratula = System.IO.Path.Combine(imagenesCaratula[0].PathRedImagen, loteCaratula);
            bool exists = System.IO.Directory.Exists(@dirLoteCaratula);

            if (!exists)
            {
                logger.Debug(String.Format("Intentando crear directorio {0}", @dirLoteCaratula));
                System.IO.Directory.CreateDirectory(@dirLoteCaratula);
                logger.Debug(String.Format("Directorio {0} creado correctamente", @dirLoteCaratula));
            }
            else
            {
                logger.Debug(String.Format("Intentando borrar directorio {0} y crearlo nuevamente", @dirLoteCaratula));
                System.IO.Directory.Delete(@dirLoteCaratula, true);
                System.IO.Directory.CreateDirectory(@dirLoteCaratula);
                logger.Debug(String.Format("Directorio {0} borrado y creado correctamente", @dirLoteCaratula));
            }

            logger.Debug("iniciando copiado de imágenes");
            foreach (var img in imagenesCaratula)
            {
                string dirDest = System.IO.Path.Combine(dirLoteCaratula, System.IO.Path.GetFileName(img.PathRedImagenFull));
                System.IO.File.Copy(@img.PathRedImagenFull, dirDest, true);
            }
            logger.Debug("copiado de imágenes terminado");

            //Crear Lote nuevo
            logger.Debug("creo nuevo objeto Model.Lote");
            Cardinal.EasyApps.ED.Model.Lote lote = new Cardinal.EasyApps.ED.Model.Lote();

            lote.CantidadImagenes = imagenesCaratula.Count();
            lote.CodigoLote = loteCaratula;
            lote.FechaCreacion = DateTime.Now;
            lote.Plantilla = new Cardinal.EasyApps.ED.Model.Plantilla
            {
                CodigoPlantilla = car.CodigoPlantilla
            };
            lote.Tag = string.Format("Lote de Caratula: {0}", car.codigo);
            logger.Debug("objeto Model.Lote creado exitosamente");
            try
            {
                _repository.AddLote(lote, imagenesCaratula, car);
            }
            catch (Exception ex)
            {
                logger.Debug("Error: " + ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Debug("Inner Exception: " + ex.InnerException.Message);
                }
            }
        }

        private void RadRibbonButton_Click_2(object sender, RoutedEventArgs e)
        {
            RefreshDatos();
        }

        private void RadRibbonButton_Click_3(object sender, RoutedEventArgs e)
        {
            RealizarProcessRadForm form = new RealizarProcessRadForm();
            List<Cardinal.EasyApps.ED.Model.CaratulaCS> caratulas = new List<Cardinal.EasyApps.ED.Model.CaratulaCS>();
            Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula caratulaApi =
                new Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula(apiUrl, apiVersion, _loginAuth);
            DateTime fechaProceso = DateTime.Now;
            while (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ProcesarLoteMerge(form, caratulaApi, caratulas);

                form = new RealizarProcessRadForm();
            }
            //Poner reporte de Outbound
            Reports.ProcessReportRadForm formReport = new Reports.ProcessReportRadForm(caratulas);
            formReport.ShowDialog();
            RefreshDatos();
        }

        private void ProcesarLoteMerge(RealizarProcessRadForm form, Caratula caratulaApi, List<Cardinal.EasyApps.ED.Model.CaratulaCS> caratulas)
        {
            var items = _repository.GetCaratulasProcesadasPorLote(form.CodigoLote);

            foreach (var item in items)
            {
                var car = caratulaApi.GetCaratula(item.codigo.ToString());
                _repository.MergeLote(form.CodigoLote, car);
                caratulas.Add(car);
            }
        }

        private bool EsTokenValido(string token)
        {
            bool result = false;

            if (!token.Contains("error"))
            {
                result = true;
            }

            return result;
        }
    }
}