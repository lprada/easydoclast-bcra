﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cardinal.EasyApps.ED.Model;

namespace ED.Caratulas.Proc.WPF
{
   public class CaratulaPendienteProceso: CaratulaCS
    {
        /// <summary>
        /// Gets or sets the lote.
        /// </summary>
        /// <value>
        /// The lote.
        /// </value>
        public string Lote { get; set; }

        /// <summary>
        /// Gets or sets the fecha creacion.
        /// </summary>
        /// <value>
        /// The fecha creacion.
        /// </value>
        public DateTime FechaCreacion { get; set; }
    }
}
