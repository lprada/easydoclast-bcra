﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cardinal.EasyApps.ED.Model;
using System.Configuration;
using System.Net;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NLog;


namespace Cardinal.EasyApps.ED.Caratulas.WebRazor.Infraestructure
{
    /// <summary>
    /// Servicio de Coneccion a ED
    /// </summary>
    public class ConexionServiciosWebED : AccessComon, Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas
    {

        Logger logger = LogManager.GetCurrentClassLogger();

        public ConexionServiciosWebED(string apiUrl, string apiVersion)
            : base(apiUrl, apiVersion)
        {
        }
        /// <summary>
        /// Authenticates the JSON.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public LoginAuth AuthenticateJSON(string username, string password)
        {
            LoginAuth token = new LoginAuth();
            try
            {
                Uri address = new Uri(string.Format("{0}/Auth/GetLoginToken", ApiUrlFull));

                string data = string.Format("username={0}&password={1}", username, password);

                HttpWebRequest request = ResquestPost(address, data);

                logger.Debug(String.Format("Inbound Url Api: {0}?{1}", request.Address.AbsoluteUri, data));
                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);
                token = JsonConvert.DeserializeObject<LoginAuth>(jsonResponse);

            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                token = new LoginAuth()
                {
                    Result = "FAIL",
                    Token = string.Empty
                };

            }
            return token;
        }

        public LoginAuth AuthenticateJSONLDAP(string username, string password)
        {
            LoginAuth token = new LoginAuth();
            try
            {
                Uri address = new Uri(string.Format("{0}/Auth/GetLoginToken", ApiUrlFull));

                string data = string.Format("username={0}&password={1}", username, password);

                HttpWebRequest request = ResquestPost(address, data);

                logger.Debug(String.Format("Inbound Url Api: {0}?{1}", request.Address.AbsoluteUri, data));
                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);
                token = JsonConvert.DeserializeObject<LoginAuth>(jsonResponse);

            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                token = new LoginAuth()
                {
                    Result = "FAIL",
                    Token = string.Empty
                };

            }
            return token;
        }

        public List<Cliente> GetEmpresasJSON(LoginAuth token)
        {
            List<Cliente> listaEmpresasJSON = new List<Cliente>();
            try
            {
                Uri address = new Uri(string.Format("{0}/Configuracion/GetEmpresas", ApiUrlFull));
                string data = string.Format("logintoken={0}", token.Token);

                HttpWebRequest request = ResquestPost(address, data);

                logger.Debug(String.Format("Inbound Url Api: {0}?{1}", request.Address.AbsoluteUri, data));
                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);
                listaEmpresasJSON = JsonConvert.DeserializeObject<List<Cliente>>(jsonResponse);

            }
            catch (Exception e)
            {

                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                listaEmpresasJSON = new List<Cliente>();
            }

            return listaEmpresasJSON;
        }

        public List<Plantilla> GetPlantillasByEmpresaJSON(LoginAuth token, string codEmpresa)
        {
            List<Plantilla> listPlantillasJSON = new List<Plantilla>();
            try
            {
                Uri address = new Uri(string.Format("{0}/Configuracion/GetPlantillasPorEmpresa", ApiUrlFull));
                string data = string.Format("logintoken={0}&codigoEmpresa={1}", token.Token, codEmpresa);
                HttpWebRequest request = ResquestPost(address, data);

                logger.Debug(String.Format("Inbound Url Api: {0}?{1}", request.Address.AbsoluteUri, data));
                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);
                listPlantillasJSON = JsonConvert.DeserializeObject<List<Plantilla>>(jsonResponse);


            }
            catch (Exception e)
            {

                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                listPlantillasJSON = new List<Plantilla>();
            }
            return listPlantillasJSON;
        }

        public List<PlantillaCampo> GetCamposByPlantillaJSON(LoginAuth token, string codPlantilla)
        {
            List<PlantillaCampo> listCamposJSON = new List<PlantillaCampo>();
            try
            {

                Uri address = new Uri(string.Format("{0}/Configuracion/GetCamposPorPlantilla", ApiUrlFull));
                string data = string.Format("logintoken={0}&codigoPlantilla={1}", token.Token, codPlantilla);
                HttpWebRequest request = ResquestPost(address, data);

                logger.Debug(String.Format("Inbound Url Api: {0}?{1}", request.Address.AbsoluteUri, data));
                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);

                listCamposJSON = JsonConvert.DeserializeObject<List<PlantillaCampo>>(jsonResponse);


            }
            catch (Exception e)
            {

                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                listCamposJSON = new List<PlantillaCampo>();

            }
            return listCamposJSON.OrderBy(x => x.Orden).ToList();
        }

        public List<TablaExterna> GetTablasExternasJSON(LoginAuth token)
        {
            Uri address = new Uri(string.Format("{0}/Configuracion/GetTablasExternas", ApiUrlFull));
            List<TablaExterna> listaTablasExternasJSON = new List<TablaExterna>();
            try
            {
                string data = string.Format("logintoken={0}", token.Token);
                HttpWebRequest request = ResquestPost(address, data);

                logger.Debug(String.Format("Inbound Url Api: {0}?{1}", request.Address.AbsoluteUri, data));
                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);
                listaTablasExternasJSON = JsonConvert.DeserializeObject<List<TablaExterna>>(jsonResponse);
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                listaTablasExternasJSON = new List<TablaExterna>();
            }
            return listaTablasExternasJSON.ToList();
        }

        public bool AddValorTablaExterna(LoginAuth token, string tablaExterna, string campoId, string campoExterno, string key, string value)
        {
            Uri address = new Uri(string.Format("{0}/Configuracion/AgregarValorTablasExternas", ApiUrlFull));
            ResultadoOperacion res = new ResultadoOperacion();

            try
            {
                string data = string.Format("logintoken={0}&tablaExterna={1}&campoId={2}&campoExterno={3}&key={4}&value={5}", token.Token, tablaExterna, campoId, campoExterno, key, value);

                HttpWebRequest request = ResquestPost(address, data);

                logger.Debug(String.Format("Inbound Url Api: {0}?{1}", request.Address.AbsoluteUri, data));
                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);
                res = JsonConvert.DeserializeObject<ResultadoOperacion>(jsonResponse);
                return res.Result == "TRUE";
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                return false;
            }
        }

        public bool EditValorTablaExterna(LoginAuth token, string tablaExterna, string campoId, string campoExterno, string key, string newValue)
        {

            Uri address = new Uri(string.Format("{0}/Configuracion/EditarValorTablasExternas", ApiUrlFull));
            ResultadoOperacion res = new ResultadoOperacion();
            try
            {
                string data = string.Format("logintoken={0}&tablaExterna={1}&campoId={2}&campoExterno={3}&key={4}&newValue={5}", token.Token, tablaExterna, campoId, campoExterno, key, newValue);

                HttpWebRequest request = ResquestPost(address, data);

                logger.Debug(String.Format("Inbound Url Api: {0}?{1}", request.Address.AbsoluteUri, data));
                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);
                res = JsonConvert.DeserializeObject<ResultadoOperacion>(jsonResponse);
                return res.Result == "TRUE";
            }
            catch (Exception e)
            {

                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                return false;
            }

        }

        public Common.Dal.IRepository Repository
        {
            get
            {
                throw new InvalidOperationException();
            }
            set
            {
                throw new InvalidOperationException();
            }
        }


        public bool ChangePassword(LoginAuth token, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }


        public string GetCampoDescripcion(string plantillaId, string campoId, string campoValue)
        {
            throw new NotImplementedException();
        }
    }
}