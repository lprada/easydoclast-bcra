﻿using System.Web.Mvc;

namespace Cardinal.EasyApps.ED.Caratulas.WebRazor.Areas.Api1
{
    public class Api1AreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Api1";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Api1_default",
                "Api/1.0/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
