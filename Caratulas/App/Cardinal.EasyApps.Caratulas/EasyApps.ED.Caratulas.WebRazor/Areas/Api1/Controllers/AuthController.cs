﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cardinal.EasyApps.ED.Model;
using Cardinal.Caratulas.Controllers;
using Cardinal.EasyApps.ED.Common.Dal;

namespace Cardinal.EasyApps.ED.Caratulas.WebRazor.Areas.Api1.Controllers
{
    public class AuthController : Controller
    {

        IRepository _repository;
        Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas _servicio;
        public AuthController(IRepository repository, Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas servicio)
        {
            _repository = repository;
            _servicio = servicio;
            _servicio.Repository = _repository;
        }

        [HttpPost]
        public ActionResult Login(string username, string password, bool ldap)
        {
            LoginAuth loginAuth;

            if (ldap)
            {
                loginAuth = _servicio.AuthenticateJSONLDAP(username, password);
            }
            else
            {
                loginAuth = _servicio.AuthenticateJSON(username, password);
            }


            if (!String.IsNullOrEmpty(loginAuth.Token))
            {
                loginAuth.Result = "OK";
                loginAuth.Token = loginAuth.Token;
            }
            else
            {
                loginAuth.Result = "FAIL";
                loginAuth.Token = string.Empty;
            }
            return new JsonResult
            {
                Data = loginAuth
            };
        }
        
    }
}
