﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using Telerik.Web.Mvc.UI;

using Cardinal.EasyApps.ED.Common.Dal;
using Cardinal.EasyApps.ED.Model;


namespace Cardinal.Caratulas.Controllers
{
    [Authorize]
    public class HistoricoController : Controller
    {
       
        //HistoricoWCF.HitoricoServiceClient servicioWCF = new HistoricoWCF.HitoricoServiceClient();
        //LogerWCF.LogServiceClient logerWCF = new LogerWCF.LogServiceClient();

        IRepository _repository;
        public HistoricoController(IRepository repository)
        {
            _repository = repository;
        	
        }

        public ActionResult VerHistorico(long[] checkedRecords)
        {
            IQueryable<CaratulaCS> caratulas = _repository.GetCaratulas(Session["UserName"].ToString());

            checkedRecords = checkedRecords ?? new long[] { }; 
            ViewData["checkedRecords"] = checkedRecords;

            if (checkedRecords.Any())
            {
                ViewData["checkedOrders"] = caratulas.Where(o => checkedRecords.Contains(o.codigo));
            }

            return View(caratulas);
        }

        [GridAction]
        public ActionResult VerReporte()
        {
            return View();
        }

        [GridAction]
        public ActionResult AnularCaratula(int id)
        {
            bool anulada = _repository.AnularCaratulaCliente(id);

            _repository.LogOperationCaratulas(new LogCaratula { CD_MOVIMIENTO = 2, CD_USUARIO_MOVIMIENTO = Session["UserName"].ToString(), FC_MOVIMIENTO = DateTime.Now, TX_OBSERVACION = "Se anuló un documento" });

            return RedirectToAction("VerHistorico");
        }
    }
}
