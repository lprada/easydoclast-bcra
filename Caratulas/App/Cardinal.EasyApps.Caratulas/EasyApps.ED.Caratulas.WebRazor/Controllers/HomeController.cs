﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.ReportViewer;
using Telerik.Reporting;
using Cardinal.Caratulas.Models;
using Telerik.Web.Mvc;
using Telerik.Web.Mvc.UI;
using Cardinal.EasyApps.ED.Common.Dal;



namespace Cardinal.Caratulas.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

         Cardinal.EasyApps.ED.Common.Dal.IRepository _repository;
        Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas _servicio;

        public HomeController(IRepository repository, Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas servicio)
        {
            _repository = repository;
            _servicio = servicio;
            _servicio.Repository = _repository;
        }
        public ActionResult Index()
        {
          

            return View();
        }

        public ActionResult Footer()
        {
            ViewBag.Instancia = _repository.GetInstancia();
            return PartialView("_Footer");
        }
    }
}