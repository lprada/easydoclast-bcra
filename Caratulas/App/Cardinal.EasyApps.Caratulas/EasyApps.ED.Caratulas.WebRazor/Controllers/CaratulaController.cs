﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Cardinal.EasyApps.ED.Common.Dal;
using Cardinal.EasyApps.ED.Model;
using Telerik.Reporting.Processing;
using Cardinal.EasyApps.ED.Common.PDF;
using System.Web;
using System.IO;
using System.Configuration;

namespace Cardinal.Caratulas.Controllers
{
    [Authorize]
    public class CaratulaController : Controller
    {
        Cardinal.EasyApps.ED.Common.Dal.IRepository _repository;
        Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas _servicio;

        public CaratulaController(IRepository repository, Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas servicio)
        {
            _repository = repository;
            _servicio = servicio;
            _servicio.Repository = _repository;
        }

        public JsonResult _GetPlantillas(string codEmpresa)
        {
            var plantillas = _servicio.GetPlantillasByEmpresaJSON((LoginAuth)Session["TokenTicket"], codEmpresa);

            return Json(new SelectList(plantillas, "CodigoPlantilla", "Nombre"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            ViewBag.Controles = "";
            ViewBag.Empresas = _servicio.GetEmpresasJSON((LoginAuth)Session["TokenTicket"]);
            Session["EmpresasJSON"] = ViewBag.Empresas;
         
            _repository.LogOperationCaratulas(new LogCaratula { CD_MOVIMIENTO = 1, CD_USUARIO_MOVIMIENTO = Session["UserName"].ToString(), FC_MOVIMIENTO = DateTime.Now, TX_OBSERVACION = "Se consulto" });
     
            // ViewBag.PlantillaPorEmpresaServiceJSONURL = ConfigurationManager.AppSettings["PlantillaPorEmpresaServiceJSON"].ToString();
            return View();
        }

        public ActionResult CreateArchivo()
        {
            ViewBag.Controles = "";

            // SelectList lista = new SelectList(, "CodigoCliente", "Nombre");
            ViewBag.Empresas = _servicio.GetEmpresasJSON((LoginAuth)Session["TokenTicket"]);
            Session["EmpresasJSON"] = ViewBag.Empresas;

            _repository.LogOperationCaratulas(new LogCaratula { CD_MOVIMIENTO = 1, CD_USUARIO_MOVIMIENTO = Session["UserName"].ToString(), FC_MOVIMIENTO = DateTime.Now, TX_OBSERVACION = "Se consulto" });
     
            return View();
        }

        public ActionResult GetCamposPlantilla(string plantillaId)
        {
            var camposPlantillas = _servicio.GetCamposByPlantillaJSON((LoginAuth)Session["TokenTicket"], plantillaId);
            camposPlantillas = (from p in camposPlantillas
                                where p.CampoId != "VERSDOC" && p.CampoId != "VERSDOC2"
                                select p).ToList<PlantillaCampo>();

            ViewBag.PlantillaId = plantillaId;
            return PartialView("_CamposPlantilla", camposPlantillas);
        }

        public ActionResult GetDescripcionCampo(string campoId, string campoValue, string plantillaId)
        {

            var descrPlantilla = _servicio.GetCampoDescripcion(plantillaId, campoId, campoValue);
            var respuesta = new
            {

                descr = descrPlantilla
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateCaratulaArchivoConfirmado(string siguienteAccion)
        {
            var res = (List<CaratulaCS>)Session["CaratulasConArchivo"];
            var checkedRecords = new List<long>();
            foreach (CaratulaCS item in res)
            {
                var num = _repository.SaveCaratula(item);
                _repository.LogOperationCaratulas(new LogCaratula { CD_MOVIMIENTO = 1, CD_USUARIO_MOVIMIENTO = Session["UserName"].ToString(), FC_MOVIMIENTO = DateTime.Now, TX_OBSERVACION = "Se dió de alta el siguiente documento " + item.codigo_3d });
                checkedRecords.Add(num);
            }

            if (siguienteAccion == "NuevaCaratula")
            {
                return RedirectToAction("CreateArchivo", "Caratula", null);
            }
            else
            {
                Session["CaratulasId"] = checkedRecords.ToArray();
                return View("VerCaratulas", checkedRecords.ToArray());
                //var result = GeneratePdfCaratula(checkedRecords.ToArray());
                //return File(result, System.Net.Mime.MediaTypeNames.Application.Pdf, String.Format("Caratulas Varias.pdf"));
            }
        }

        [HttpPost]
        public ActionResult CreateCaratulaArchivo(IEnumerable<HttpPostedFileBase> attachments)
        {
            if (string.IsNullOrWhiteSpace(Request.Form["Plantilla"]))
            {
                ModelState.AddModelError("Plantilla", "Por favor Seleccione una Plantilla.");
            }
            if (attachments == null || attachments.Count() == 0)
            {
                ModelState.AddModelError("attachments", "Por favor ingrese un archivo.");
            }
            else
            {
                int attchSizes = 0;
                foreach (var file in attachments)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        attchSizes += file.ContentLength / 1024 / 1024;
                    }
                    var ext = System.IO.Path.GetExtension(file.FileName).ToUpper();

                    if (ext != ".TXT" && ext != ".CSV")
                    {
                        ModelState.AddModelError("attachments", "Solo pueden utilizarse archivos txt o csv.");
                        break;
                    }
                }

                if (attchSizes > 5)
                {
                    ModelState.AddModelError("attachments", "Los adjuntos no pueden superar los 5MB. en total.");
                }
            }

            if (ModelState.IsValid)
            {
                var plantillaId = Request.Form["Plantilla"];
                var empresaId = Request.Form["Empresa"];

                var plantilla =
                _servicio.GetPlantillasByEmpresaJSON((LoginAuth)Session["TokenTicket"], empresaId)
                         .Where(p => p.CodigoPlantilla == plantillaId)
                         .FirstOrDefault();
                var camposPlantillas = _servicio.GetCamposByPlantillaJSON((LoginAuth)Session["TokenTicket"], plantillaId);
                bool hayDetalles = camposPlantillas.Where(p => p.EsCampoDetalle == true).Count() > 0;
                try
                {
                    if (!hayDetalles)
                    {
                        var res = ProcesarArchivoCaratulaSinDetalle(attachments, plantilla, camposPlantillas);
                        Session["CaratulasConArchivo"] = res;
                        return View("CreateArchivoConfirmacion", res);
                    }
                    else
                    {
                        var res = ProcesarArchivoCaratulaConDetalle(attachments, plantilla, camposPlantillas);
                        Session["CaratulasConArchivo"] = res;
                        return View("CreateArchivoConfirmacion", res);
                    }
                }
                catch
                {
                    ModelState.AddModelError("attachments", "Los adjuntos cumplen con las reglas de la plantilla.");
                    ViewBag.Controles = "";

                    // SelectList lista = new SelectList(, "CodigoCliente", "Nombre");
                    ViewBag.Empresas = _servicio.GetEmpresasJSON((LoginAuth)Session["TokenTicket"]);
                    Session["EmpresasJSON"] = ViewBag.Empresas;

                    _repository.LogOperationCaratulas(new LogCaratula { CD_MOVIMIENTO = 1, CD_USUARIO_MOVIMIENTO = Session["UserName"].ToString(), FC_MOVIMIENTO = DateTime.Now, TX_OBSERVACION = "Se consulto" });

                    return View("CreateArchivo");
                }
            }
            else
            {
                ViewBag.Controles = "";

                // SelectList lista = new SelectList(, "CodigoCliente", "Nombre");
                ViewBag.Empresas = _servicio.GetEmpresasJSON((LoginAuth)Session["TokenTicket"]);
                Session["EmpresasJSON"] = ViewBag.Empresas;

                _repository.LogOperationCaratulas(new LogCaratula { CD_MOVIMIENTO = 1, CD_USUARIO_MOVIMIENTO = Session["UserName"].ToString(), FC_MOVIMIENTO = DateTime.Now, TX_OBSERVACION = "Se consulto" });

                return View("CreateArchivo");
            }
        }

        private List<CaratulaCS> ProcesarArchivoCaratulaSinDetalle(IEnumerable<HttpPostedFileBase> attachments, Plantilla plantilla, List<PlantillaCampo> camposPlantillas)
        {
            List<CaratulaCS> res = new List<CaratulaCS>();

            foreach (var file in attachments)
            {
                if (file != null && file.ContentLength > 0)
                {
                    //Proceso el Archivo y lo paso a confirmacion
                    string[] fields;
                    string[] delimiter = new string[] { "," };

                    using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(file.InputStream))
                    {
                        parser.Delimiters = delimiter;
                        while (!parser.EndOfData)
                        {
                            //Read in the fields for the current line
                            fields = parser.ReadFields();

                            int IDDocumentoMax = 0;
                            List<CamposCS> listaCampos = new List<CamposCS>();

                            CaratulaCS documento = new CaratulaCS
                            {
                                empresa = plantilla.Cliente.CodigoCliente,
                                codigo = 0,
                                CodigoPlantilla = plantilla.CodigoPlantilla,
                                fecha = DateTime.Now,
                                nombre_empresa = plantilla.Cliente.Nombre,
                                plantilla = plantilla.Nombre,
                                usuario = User.Identity.Name
                            }
                            ;
                            int i = 0;
                            foreach (var item in camposPlantillas.Where(p => p.EsCampoDetalle == false).OrderBy(p => p.Orden))
                            {
                                AgregarCampoCaratula(fields, listaCampos, i, item, plantilla.CodigoPlantilla);
                                i++;
                            }

                            documento.DetalleCampos = listaCampos;
                     
                            documento.codigo_3d = generarValores3d(IDDocumentoMax, listaCampos);
                            res.Add(documento);
                        }
                    }
                }
            }
            return res;
        }

        private List<CaratulaCS> ProcesarArchivoCaratulaConDetalle(IEnumerable<HttpPostedFileBase> attachments, Plantilla plantilla, List<PlantillaCampo> camposPlantillas)
        {
            List<CaratulaCS> res = new List<CaratulaCS>();

            foreach (var file in attachments)
            {
                if (file != null && file.ContentLength > 0)
                {
                    //Proceso el Archivo y lo paso a confirmacion
                    string[] fields;
                    string[] delimiter = new string[] { "," };

                    using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(file.InputStream))
                    {
                        parser.Delimiters = delimiter;
                      
                        List<CamposCS> listaCampos = new List<CamposCS>();
                        CaratulaCS documento = null;
                        int renglonDetalle = 0;
                        while (!parser.EndOfData)
                        {
                            //Read in the fields for the current line
                            fields = parser.ReadFields();
                            int i = 0;
                            if (fields[0] != "{DETALLE}")
                            {
                                renglonDetalle = 0;
                                listaCampos = new List<CamposCS>();

                                documento = new CaratulaCS
                                {
                                    empresa = plantilla.Cliente.CodigoCliente,
                                    codigo = 0,
                                    CodigoPlantilla = plantilla.CodigoPlantilla,
                                    fecha = DateTime.Now,
                                    nombre_empresa = plantilla.Cliente.Nombre,
                                    plantilla = plantilla.Nombre,
                                    usuario = User.Identity.Name
                                };
                                foreach (var item in camposPlantillas.Where(p => p.EsCampoDetalle == false).OrderBy(p => p.Orden))
                                {
                                    AgregarCampoCaratula(fields, listaCampos, i, item, plantilla.CodigoPlantilla);
                                    i++;
                                }
                                short ordenDetalle = 0;
                                foreach (var item in camposPlantillas.Where(p => p.EsCampoDetalle == true).OrderBy(p => p.Orden))
                                {
                                    AgregarCampoDetalleCaratula(fields, listaCampos, i, item, renglonDetalle, ordenDetalle);
                                    ordenDetalle++;
                                    i++;
                                }
                                documento.DetalleCampos = listaCampos;
                                documento.codigo_3d = generarValores3d(0, listaCampos);
                                res.Add(documento);
                            }
                            else
                            {
                                renglonDetalle++;
                                foreach (var item in camposPlantillas.Where(p => p.EsCampoDetalle == false))
                                {
                                    i++;
                                }
                                short ordenDetalle = 0;
                                foreach (var item in camposPlantillas.Where(p => p.EsCampoDetalle == true).OrderBy(p => p.Orden))
                                {
                                    AgregarCampoDetalleCaratula(fields, listaCampos, i, item, renglonDetalle, ordenDetalle);
                                    ordenDetalle++;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            return res;
        }

        private void AgregarCampoDetalleCaratula(string[] fields, List<CamposCS> listaCampos, int i, PlantillaCampo item, int renglonDetalle, short ordenDetalle)
        {
            ValidarCampo(fields, i, item);
            string campoId = string.Empty;
            if (renglonDetalle == 0)
            {
                campoId = string.Format("{0}", item.CampoId, renglonDetalle);
            }
            else
            {
                campoId = string.Format("{0}_{1}", item.CampoId, renglonDetalle);
            }
            CamposCS campo = new CamposCS
            {
                campo = campoId,
                valor = fields[i],
                txCampo = item.Nombre,
                EsDetalle = true,
                OrdenDetalle = (short)renglonDetalle
            };
            listaCampos.Add(campo);
        }

        private void AgregarCampoCaratula(string[] fields, List<CamposCS> listaCampos, int i, PlantillaCampo item, string plantillaId)
        {
            ValidarCampo(fields, i, item);
            CamposCS campo = null;
            if (item.UsaTablaDescripciones)
            {
                var valorDescr = _servicio.GetCampoDescripcion(plantillaId, item.CampoId, fields[i]);
                string valor = string.Empty;
                if (!string.IsNullOrWhiteSpace(valorDescr))
                {
                    valor = string.Format("{0} - {1}", fields[i], valorDescr);
                }
                else
                {
                    valor = string.Format("{0}", fields[i]);
                }
                campo = new CamposCS
                {
                    campo = item.CampoId,
                    valor = valor,
                    txCampo = item.Nombre,
                    EsDetalle = false,
                    OrdenDetalle = 0
                };
            }
            else
            {
                campo = new CamposCS
                {
                    campo = item.CampoId,
                    valor = fields[i],
                    txCampo = item.Nombre,
                    EsDetalle = false,
                    OrdenDetalle = 0
                };
            }
            listaCampos.Add(campo);
        }

        private string ValidarCampo(string[] fields, int i, PlantillaCampo item)
        {
            switch (item.TipoCampo)
            {
                case TipoCampo.Texto:
                    if (item.ValoresPosibles.Count() > 0)
                    {
                        string valorKey = string.Empty;
                        
                        foreach (var valorPosible in item.ValoresPosibles)
                        {
                            if (valorPosible.Value == fields[i])
                            {
                                return valorPosible.Key;
                            }
                        }
                        if (string.IsNullOrWhiteSpace(valorKey))
                        { 
                            throw new InvalidOperationException("Valor Combo no valido.");
                        }
                    }
                    break;
                case TipoCampo.Numero:
                    Convert.ToDouble(fields[i]);
                    break;
                case TipoCampo.Fecha:
                    DateTime.ParseExact(fields[i], "yyyy-MM-dd", null);
                    break;
                case TipoCampo.Boolean:
                    Convert.ToBoolean(fields[i].Replace("SI", "true").Replace("NO", "false"));
                    break;
                default:
                    break;
            }
            return fields[i];
        }

        [HttpPost]
        public ActionResult CreateCaratula()
        {
            if (string.IsNullOrWhiteSpace(Request.Form["Plantilla"]))
            {
                ModelState.AddModelError("Plantilla", "Por favor Seleccione una Plantilla.");
            }

            //Valido Campos

            var plantillaId = Request.Form["Plantilla"];
            var empresaId = Request.Form["Empresa"];

            var plantilla =
            _servicio.GetPlantillasByEmpresaJSON((LoginAuth)Session["TokenTicket"], empresaId)
                     .Where(p => p.CodigoPlantilla == plantillaId)
                     .FirstOrDefault();
            var camposPlantillas = _servicio.GetCamposByPlantillaJSON((LoginAuth)Session["TokenTicket"], plantillaId);

            foreach (var item in camposPlantillas.Where(p => p.EsCampoDetalle == false))
            {
                var valorCampo = Request.Form[item.CampoId];

                if (item.EsObligatorio)
                {
                    if (string.IsNullOrWhiteSpace(valorCampo))
                    {
                        ModelState.AddModelError(item.CampoId, String.Format("Por favor ingrese un valor para el campo {0}.", item.Nombre));
                    }
                }
                if (item.Mascara == "V:CUIT")
                {
                    if (!Cardinal.EasyApps.ED.Common.Utils.CUIT.ValidarCuit(valorCampo))
                    {
                        ModelState.AddModelError(item.CampoId, String.Format("El valor para el campo {0} no es un CUIT valido.", item.Nombre));
                    }
                }
            }

            if (ModelState.IsValid)
            {
                int IDDocumentoMax = 0;

                List<CamposCS> listaCampos = new List<CamposCS>();
                DocumentoEmpresa documento = new DocumentoEmpresa
                {
                    empresa = _servicio.GetEmpresasJSON((LoginAuth)Session["TokenTicket"]).Where(p => p.CodigoCliente == empresaId).FirstOrDefault(),
                    estadoDocumento = "1",
                    fechaCreacion = DateTime.Now,
                    usuarioCarga = User.Identity.Name,
                    codigoDocumento = IDDocumentoMax
                };
                foreach (var item in camposPlantillas.Where(p => p.EsCampoDetalle == false))
                {
                    CamposCS campo = AddCampoCaratula(item, plantillaId);
                    //CamposCS campo = new CamposCS
                    //{
                    //    campo = item.CampoId,
                    //    valor = Request.Form[item.CampoId],
                    //    txCampo = item.Nombre,
                    //    EsDetalle = false, OrdenDetalle = 0
                    //};
                    listaCampos.Add(campo);
                }

                documento.listaCampos = listaCampos;
                documento.plantilla = plantillaId;
                documento.txPlantilla = plantilla.Nombre;
                documento.codigo3d = generarValores3d(IDDocumentoMax, listaCampos);

                //Cargo los Detalles 
                foreach (var item in camposPlantillas.Where(p => p.EsCampoDetalle == true))
                {
                    CamposCS campo = AddCampoCaratula(item, plantillaId);
                    listaCampos.Add(campo);

                    short i = 1;
                    bool seguir = !String.IsNullOrWhiteSpace(Request.Form[String.Format("{0}{1}", item.CampoId, i)]);
                    while (seguir)
                    {
                        CamposCS campoDetalle = new CamposCS
                        {
                            campo = String.Format("{0}_{1}", item.CampoId, i),
                            valor = Request.Form[String.Format("{0}{1}", item.CampoId, i)],
                            txCampo = item.Nombre,
                            EsDetalle = true,
                            OrdenDetalle = i
                        };
                        listaCampos.Add(campoDetalle);
                        i++;
                        seguir = !String.IsNullOrWhiteSpace(Request.Form[String.Format("{0}{1}", item.CampoId, i)]);
                    }
                }

                var numDoc = _repository.saveDocumento(documento);

                //Session["Documento"] = documento.codigoDocumento;
                _repository.LogOperationCaratulas(new LogCaratula { CD_MOVIMIENTO = 1, CD_USUARIO_MOVIMIENTO = Session["UserName"].ToString(), FC_MOVIMIENTO = DateTime.Now, TX_OBSERVACION = "Se dió de alta el siguiente documento " + documento.codigo3d });
                //return RedirectToAction("Index", "Message", new { message = "El documento fue creado con éxito" });

                string siguienteAccion = Request.Form["siguienteAccion"];

                if (siguienteAccion == "NuevaCaratula")
                {
                    return RedirectToAction("Create", "Caratula", null);
                }
                else
                {
                    return RedirectToAction("VerReporte", "Historico", new { id = numDoc });
                }
            }
            else
            {
                return ErroCargaCaratula();
            }
        }
  
        private CamposCS AddCampoCaratula(PlantillaCampo item, string plantillaId)
        {
            CamposCS campo = null;

            if (item.UsaTablaDescripciones)
            {
                var valorDescr = _servicio.GetCampoDescripcion(plantillaId, item.CampoId, Request.Form[item.CampoId]);
                string valor = string.Empty;
                if (!string.IsNullOrWhiteSpace(valorDescr))
                {
                    valor = string.Format("{0} - {1}", Request.Form[item.CampoId], valorDescr);
                }
                else
                {
                    valor = string.Format("{0}", Request.Form[item.CampoId]);
                }
                campo = new CamposCS
                {
                    campo = item.CampoId,
                    valor = valor,
                    txCampo = item.Nombre,
                    EsDetalle = false,
                    OrdenDetalle = 0
                };
            }
            else
            {
                campo = new CamposCS
                {
                    campo = item.CampoId,
                    valor = Request.Form[item.CampoId],
                    txCampo = item.Nombre,
                    EsDetalle = false,
                    OrdenDetalle = 0
                };
            }
            return campo;
        }

        private ActionResult ErroCargaCaratula()
        {
            ViewBag.Empresas = _servicio.GetEmpresasJSON((LoginAuth)Session["TokenTicket"]);
            Session["EmpresasJSON"] = ViewBag.Empresas;

            //Debo Guardar Empresa, Plantilla

            return View("Create");
        }

        private string generarValores3d(int IDDocumentoMax, List<CamposCS> listaCampos)
        {
            StringBuilder sb3d = new StringBuilder();
            sb3d.Append(IDDocumentoMax);
            sb3d.Append(",");
            foreach (CamposCS item in listaCampos)
            {
                sb3d.Append(item.valor);
                sb3d.Append(",");
            }
            return sb3d.ToString();
        }

        public ActionResult GetPdfCaratula(int caratulaId)
        {
            RenderingResult result = GeneratePdfCaratula(caratulaId);
            return File(result.DocumentBytes, System.Net.Mime.MediaTypeNames.Application.Pdf, String.Format("Caratula {0}.pdf", caratulaId));
        }

        public ActionResult VerCaratulas(long[] checkedRecords)
        {
            Session["CaratulasId"] = checkedRecords;
            return View("VerCaratulas", checkedRecords);
        }

        public ActionResult GetMultiplesPdfCaratula(long[] checkedRecords)
        {
            if (checkedRecords == null)
            {
                checkedRecords = (long[])Session["CaratulasId"];
            }

            var result = GeneratePdfCaratula(checkedRecords);
            return File(result, System.Net.Mime.MediaTypeNames.Application.Pdf, String.Format("Caratulas Varias.pdf"));
        }

        private RenderingResult GeneratePdfCaratula(int caratulaId)
        {
            var data = _repository.GetCaratula(caratulaId);
          
            return GeneratePdfCaratulaData(data);
        }

        private Byte[] GeneratePdfCaratula(long[] caratulas)
        {
            var data = _repository.GetCaratulas(caratulas);

            SimplePDF sPDF = new SimplePDF();
            PDFPermissions permisos = new PDFPermissions();
            sPDF.open_file("", permisos);

            foreach (var caratula in data)
            {
                var res = GeneratePdfCaratulaData(caratula);
                sPDF.pdf_en_nueva_pagina(res.DocumentBytes, 0);
            }
            sPDF.close_file();
            byte[] buf = sPDF.get_file_bytes();

            if (buf.Length > 0)
            {
                return buf;
            }
            else
            {
                buf = System.IO.File.ReadAllBytes(Server.MapPath("~/App_Data/SinImagen.pdf"));
                return buf;
            }
        }

        private static RenderingResult GeneratePdfCaratulaData(CaratulaCS data)
        {
            Cardinal.EasyApps.ED.Caratulas.WebRazor.Reportes.CaratulaBCRAReport reporte = new Cardinal.EasyApps.ED.Caratulas.WebRazor.Reportes.CaratulaBCRAReport();

            reporte.ReportParameters["Empresa"].Value = data.nombre_empresa;
            reporte.ReportParameters["Plantilla"].Value = data.plantilla;
            reporte.ReportParameters["Usuario"].Value = data.usuario;
            reporte.ReportParameters["Fecha"].Value = data.fecha;
            reporte.ReportParameters["NroCar"].Value = data.codigo;
            reporte.ReportParameters["NOMDOC"].Value = String.Format("{0} - {1}",data.DetalleCampos.FirstOrDefault(p => p.campo == "TIPDOC").valor, data.DetalleCampos.FirstOrDefault(p => p.campo == "NOMDOC").valor);
            reporte.ReportParameters["FECLOTE"].Value = data.DetalleCampos.FirstOrDefault(p => p.campo == "FECLOTE").valor;
            reporte.ReportParameters["DETALLE"].Value = data.DetalleCampos.FirstOrDefault(p => p.campo == "DETALLE").valor;
            string dependencia = data.DetalleCampos.FirstOrDefault(p => p.campo == "DEPEND").valor;
            int sepaDependencia = dependencia.LastIndexOf("-");
            reporte.ReportParameters["Dependencia"].Value = dependencia.Substring(sepaDependencia + 1 , dependencia.Length - sepaDependencia - 1);
            reporte.ReportParameters["CodigoDependencia"].Value = dependencia.Substring(0, sepaDependencia);

            //reporte.DataSource = data.DetalleCampos;
            MessagingToolkit.QRCode.Codec.QRCodeEncoder qr = new MessagingToolkit.QRCode.Codec.QRCodeEncoder();
            string str = data.codigo_3d.ToUpper();//(reporte.Items.Find("codigo_3d_not_visible", true)[0] as Telerik.Reporting.TextBox).Value;
            str = DecodearTexto(EncodearTexto(str));

            System.Drawing.Bitmap bmp = qr.Encode(str.Substring(0,40));
            (reporte.Items.Find("picBoxQR", true)[0] as Telerik.Reporting.PictureBox).Value = bmp;

            //BarCode Caratula
            (reporte.Items.Find("barcode2", true)[0] as Telerik.Reporting.Barcode).Value = data.codigo.ToString();

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("PDF", reporte, null);
            return result;
        }

        public ActionResult GetCaratulaJSON(int caratulaId)
        {
            return new JsonResult
            {
                Data = _repository.GetCaratula(caratulaId),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        private static byte[] EncodearTexto(string texto)
        {
            byte[] resultado = System.Text.Encoding.UTF8.GetBytes(texto);

            return resultado;
        }

        private static string DecodearTexto(byte[] array)
        {
            string resultado = Convert.ToBase64String(array);

            return resultado;
        }
    }
}