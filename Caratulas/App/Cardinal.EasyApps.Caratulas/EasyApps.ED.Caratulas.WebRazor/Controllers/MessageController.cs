﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cardinal.Caratulas.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        //
        // GET: /Message/

        public ActionResult Index(string message)
        {
            ViewBag.Message = message;
            ViewBag.Documento = Convert.ToInt32(Session["Documento"].ToString());
            return View();
        }

    }
}
