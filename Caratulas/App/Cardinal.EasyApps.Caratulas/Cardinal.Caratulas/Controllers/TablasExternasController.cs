﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cardinal.EasyApps.ED.Common.Dal;
using Telerik.Web.Mvc;
using Cardinal.EasyApps.ED.Model;

namespace Cardinal.Caratulas.Controllers
{

    [Authorize]
    public class TablasExternasController : Controller
    {
         IRepository _repository;
         public TablasExternasController(IRepository repository)
        {
            _repository = repository;
        	
        }

          [GridAction]
        public ActionResult VerTablasExternas()
        {
          var tablas =  ConexionServiciosED.GetTablasExternasJSON((Token)Session["TokenTicket"]);


            return View(tablas);
        }

         [GridAction]
        public ActionResult VerDatosTablaExterna(string tablaExterna, string campoId, string campoExterno)
        {
            var tablas = ConexionServiciosED.GetTablasExternasJSON((Token)Session["TokenTicket"]);

            foreach (var item in tablas)
            {
                if (item.Nombre == tablaExterna && item.CampoId == campoId && item.CampoExterno == campoExterno)
                {
                    return View(item);
                }
                
            }
            return View("TablaExternaConError");

           
        }

         [HttpPost]
         public ActionResult AgregarDato(string tablaExterna, string campoId, string campoExterno)
        {
            //Create a new instance of the Customer class.
            Cardinal.Caratulas.Models.ValorTablaExterna dato = new Models.ValorTablaExterna();

            //Perform model binding (fill the customer properties and validate it).
            if (TryUpdateModel(dato))
            {
                ConexionServiciosED.AddValorTablaExterna((Token)Session["TokenTicket"], tablaExterna, campoId, campoExterno, dato.Key, dato.Value);

                return RedirectToAction("VerDatosTablaExterna", new { tablaExterna = tablaExterna, campoId = campoId, campoExterno = campoExterno });
            }

            //The model is invalid - render the current view to show any validation errors.
            return RedirectToAction("VerDatosTablaExterna", new { tablaExterna = tablaExterna, campoId = campoId, campoExterno = campoExterno });
        }

         [HttpPost]
         public ActionResult EditDato(string tablaExterna, string campoId, string campoExterno,string id)
         {
             Cardinal.Caratulas.Models.ValorTablaExterna dato = new Models.ValorTablaExterna();
           

             ////Perform model binding (fill the customer properties and validate it).
             if (TryUpdateModel(dato))
             {
                 ConexionServiciosED.EditValorTablaExterna((Token)Session["TokenTicket"], tablaExterna, campoId, campoExterno, id, dato.Value);

                 return RedirectToAction("VerDatosTablaExterna", new { tablaExterna = tablaExterna, campoId = campoId, campoExterno = campoExterno });
             }

         

             return RedirectToAction("VerDatosTablaExterna", new { tablaExterna = tablaExterna, campoId = campoId, campoExterno = campoExterno });
         }
      
    }
}
