﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Cardinal.Caratulas.Reportes.Report3>" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VerReporte1
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>VerReporte1</h2>

    <script runat="server">
        
        public override void VerifyRenderingInServerForm(Control control)
        {
            // to avoid the server form (<form runat="server">) requirement
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            // bind the report viewer
            ReportViewer2.Report = new Cardinal.Caratulas.Reportes.Report3();
        }
    </script>
    <rsweb:ReportViewer ID="ReportViewer2" runat="server">
    </rsweb:ReportViewer>

</asp:Content>
