﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Cardinal.EasyApps.ED.Model.Cliente>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Alta de carátulas por empresa</h2>

<script src="../../Scripts/jquery.validate.js" type="text/javascript"></script>

<script type="text/javascript">



    var $j = jQuery.noConflict();
//    $j("#datepicker").datepicker();


//    $j(function () {$j("#fecdoc").datepicker();});


    $j("#Plantillas").attr('disabled', true)

    $j(function () {

        var urlemp = '@Url.Action("GetPlantillasPorEmpresa")';

        $j("#Empresas").change(function () {

            var categoriaId = $j(this).val();


            $j.ajax({
                type: 'GET',

                url: 'GetPlantillasPorEmpresa',
                data: { valor: categoriaId },
                datatype: 'JSON',


                success: function (data) {

                    if (data.length > 0) {
                        var options = '';
                        for (p in data) {
                            var subcategorias = data[p];
                            options += "<option value='" + subcategorias.CodigoPlantilla + "'>" + subcategorias.Nombre + "</option>";

                        }

                        $j("#Plantillas").removeAttr('disabled').html(options);
                        $j("#MainContent_CrearControles").attr('disabled', false)

                    } else {

                        $j("#Plantillas").attr('disabled', true).html('');
                        $j("#MainContent_CrearControles").attr('disabled', true)

                    }
                }, //function,


                error: function (xhr, type, exception) {
                    window.alert(urlemp + "/" + categoriaId);

                    alert("Error: " + type);
                    alert("Error: " + exception);
                    alert("Error: " + xhr.status);

                }
            })


        });
    });
    $j(document).ready(function () {
        $j("#formCaratulas").validate();
    });


</script> 
  <style type="text/css">
      label.error { float: none; color: red; padding-left: .5em; vertical-align: top; }
p { clear: both; }
 .HelperPlantillas
        {
            display:block;
            padding:10px;

  border:1px solid black;
 
  background-color:#EDE8EA; color:#696868;
  text-align: center; 
  font-family:Calibri;
  font-size: small;
        }
         .HelperCampos
        {
            padding-left:100px;padding-bottom:50px;position:relative;
            border:1px solid black;
 
            background-color:#EDE8EA; color:#696868;
            text-align: center; 
             font-family:Calibri;
  font-size: small;
        }
        .field-validation-error { color: red; position: absolute; margin: 0 0 0 5px; }
   .validation-summary-errors {border:1px solid #DFDFDF; background: #FDFDFD; color:Gray;}
    </style>
<%Html.EnableClientValidation(); %>
<%= Html.ValidationSummary() %>
<% using (Html.BeginForm("Create", "Caratula", FormMethod.Post, new { id = "formCaratulas", name = "formCaratulas" }))
 { %>
   
      <fieldset style="padding:5px;">
        <legend>Datos de carga</legend>
        <table style="width: 625px">
        <tr>
        <td class="style2">
        <div class="display-label">Empresa</div>
        </td>
        <td class="style3">
            <div class="editor-label">
            <%: Html.DropDownList("Empresas", String.Empty)%>
            <%= Html.ValidationMessage("Error", "*")%>
            <%: Html.ValidationMessageFor(model => model.CodigoCliente) %>
        </div></td>
          <td class="style1" rowspan="2">
            <div style="width: 234px" id="HelperPlantillas" class="HelperPlantillas">
             Seleccione empresa y plantilla y luego presione <span style="color:#5772BD; font-weight:bold;">'Cargar Datos'</span>
            </div></td>
        <td class="style1" rowspan="2">
           </td>
        </tr>
        <tr>
        <td class="style2">
       
        <div class="display-label">Plantilla</div>
       
        </td>
        <td class="style3">
         <div class="editor-label">
         <%: Html.DropDownList("Plantillas",String.Empty)%>

        </div>
        </td>
        </tr>
        </table>
       
        <br /><br />
        
         <input type="submit" id="CrearControles" runat="server" value="Cargar datos" /><br />
     
        <%= ViewBag.Controles%>
        
              
      
        <input type="submit" id="Generar" runat="server" value="Genera reporte" /><br />
    <%=ViewBag.Scripts %>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Limpiar formulario", "Index") %>
</div>

</asp:Content>
