﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.Model
{
    [Serializable]
    public class ClienteJSON
    {
        public string CodigoCliente { get; set; }

        public string Nombre { get; set; }
    }
}
