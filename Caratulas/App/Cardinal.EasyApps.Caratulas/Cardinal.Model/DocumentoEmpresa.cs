﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.Model
{
   public  class DocumentoEmpresa
    {
       public EmpresaJSON empresa{get;set;}
       public int codigoDocumento { get; set; }
       public string plantilla { get; set; }
       public string txPlantilla { get; set; }
       public string usuarioCarga { get; set; }
       public string estadoDocumento { get; set; }
       public DateTime fechaCreacion { get; set; }
       public string codigo3d { get; set; }
       public IList<CamposCS> listaCampos { get; set; }
    }
}
