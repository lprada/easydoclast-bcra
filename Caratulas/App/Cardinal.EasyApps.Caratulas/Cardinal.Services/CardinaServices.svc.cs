﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Cardinal.Model;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;



namespace Cardinal.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CardinaServices" in code, svc and config file together.
    public class CardinaServices : ICardinaServices
    {
        public List<EmpresaJSON> getEmpresasJSON(Token _token)
        {
            Uri address = new Uri(ConfigurationManager.AppSettings["EmpresaServiceJSON"].ToString());
            List<EmpresaJSON> listaEmpresasJSON = new List<EmpresaJSON>();
            #region WebRequest
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            string data = string.Format("logintoken={0}", _token.TokenTicket);

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);


            request.ContentLength = byteData.Length;


            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            #endregion

            #region WebResponse [JSON SERIALIZATION]
            string jsonResponse = "";

            // Get response 
            Token token = new Token();

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();
                listaEmpresasJSON = JsonConvert.DeserializeObject<List<EmpresaJSON>>(jsonResponse);
                
                reader.Close();
            }
            #endregion

            return listaEmpresasJSON;
        }


        public List<PlantillaJSON> getPlantillasByEmpresaJSON(Token _token, string codEmpresa)
        {
            Uri address = new Uri(ConfigurationManager.AppSettings["PlantillaPorEmpresaServiceJSON"].ToString());
            List<PlantillaJSON> listPlantillasJSON = new List<PlantillaJSON>();
            #region WebRequest
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            string data = string.Format("logintoken={0}&codigoEmpresa={1}", _token.TokenTicket, codEmpresa);

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);


            request.ContentLength = byteData.Length;


            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            #endregion

            #region WebResponse [JSON SERIALIZATION]
            string jsonResponse = "";

            // Get response 
            Token token = new Token();

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();
                listPlantillasJSON = JsonConvert.DeserializeObject<List<PlantillaJSON>>(jsonResponse);

                reader.Close();
            }
            #endregion

            return listPlantillasJSON;
        }


        public List<CamposJSON> getCamposByPlantillaJSON(Token _token, string codPlantilla)
        {
            Uri address = new Uri(ConfigurationManager.AppSettings["CamposPlantillaPorEmpresaServiceJSON"].ToString());
            List<CamposJSON> listCamposJSON = new List<CamposJSON>();
            #region WebRequest
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            string data = string.Format("logintoken={0}&codigoPlantilla={1}", _token.TokenTicket, codPlantilla);

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);


            request.ContentLength = byteData.Length;


            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            #endregion

            #region WebResponse [JSON SERIALIZATION]
            string jsonResponse = "";

            // Get response 
            Token token = new Token();

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();
                listCamposJSON = JsonConvert.DeserializeObject<List<CamposJSON>>(jsonResponse);

                reader.Close();
            }
            #endregion

            return listCamposJSON.OrderBy(x => x.Orden).ToList();
        }

        public RessultMessage saveDocumento(DocumentoEmpresa documento)
        {
           RessultMessage resultMessage = new RessultMessage { status = "OK" };
           using (CardinalEntities ctx = new CardinalEntities())
            {
                List<TB_DOCUMENTO> listDocs = new List<TB_DOCUMENTO>();

                TB_EMPRESA_DOCUMENTOS empresaDoc = new TB_EMPRESA_DOCUMENTOS
                    {
                          CD_CODIGO_3D = documento.codigo3d,
                          CD_USUARIO_CARGA = documento.usuarioCarga,
                          CD_EMPRESA = documento.empresa.CodigoCliente , 
                          CD_CODIGO_DOCUMENTO = documento.codigoDocumento,
                          TX_EMPRESA = documento.empresa.Nombre ,
                          FC_CREACION =  documento.fechaCreacion,
                          FL_ESTADO_DOCUMENTO = documento.estadoDocumento
                    };

                ctx.TB_EMPRESA_DOCUMENTOS.AddObject(empresaDoc);
               
                foreach (CamposCS item in documento.listaCampos)
                {
                    TB_DOCUMENTO documentoEntity = new TB_DOCUMENTO
                    {
                        CD_CODIGO_DOCUMENTO = documento.codigoDocumento,
                        CD_CAMPO = item.campo,
                        TX_CAMPO = item.txCampo,
                        CD_PLANTILLA = documento.plantilla,
                        TX_PLANTILLA = documento.txPlantilla,
                        VL_CAMPO = item.valor

                    };
                    
                    ctx.TB_DOCUMENTO.AddObject(documentoEntity);
                    
                }

                ctx.SaveChanges();
            }
           return resultMessage;
        }
    }
}
