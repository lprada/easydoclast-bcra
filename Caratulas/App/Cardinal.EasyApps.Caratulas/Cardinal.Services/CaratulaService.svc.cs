﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Cardinal.Model;


namespace Cardinal.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CaratulaService" in code, svc and config file together.
    public class CaratulaService : ICaratulaService
    {

        public IList<CaratulaCS> GetCaratulaVistaPorId(int nroCaratula)
        {
            using (CardinalEntities ctx = new CardinalEntities())
            {
                var query = (from empresa in ctx.TB_EMPRESA_DOCUMENTOS
                             join documentos in ctx.TB_DOCUMENTO on empresa.CD_CODIGO_DOCUMENTO equals documentos.CD_CODIGO_DOCUMENTO
                             where empresa.CD_CODIGO_DOCUMENTO == nroCaratula
                             select new CaratulaCS
                             {
                                 empresa = empresa.CD_EMPRESA,
                                 usuario = empresa.CD_USUARIO_CARGA,
                                 fecha = empresa.FC_CREACION,
                                 codigo = documentos.CD_CODIGO_DOCUMENTO,
                                 valor = documentos.VL_CAMPO,
                                 campo = documentos.TX_CAMPO,
                                 plantilla = documentos.TX_PLANTILLA,
                                 nombre_empresa = empresa.TX_EMPRESA,
                                 codigo_3d = empresa.CD_CODIGO_3D,
                             }).ToList();
                return query;
            }
        }

        public IList<CaratulaCS> GetCaratulaCompleta()
        {
            using (CardinalEntities ctx = new CardinalEntities())
            {
                var query = (from empresa in ctx.TB_EMPRESA_DOCUMENTOS
                             join documentos in ctx.TB_DOCUMENTO on empresa.CD_CODIGO_DOCUMENTO equals documentos.CD_CODIGO_DOCUMENTO
                             select new CaratulaCS
                             {
                                 empresa = empresa.CD_EMPRESA,
                                 usuario = empresa.CD_USUARIO_CARGA,
                                 fecha = empresa.FC_CREACION,
                                 codigo = documentos.CD_CODIGO_DOCUMENTO,
                                 valor = documentos.VL_CAMPO,
                                 campo = documentos.TX_CAMPO,
                                 plantilla = documentos.TX_PLANTILLA,
                                 nombre_empresa = empresa.TX_EMPRESA,
                                 codigo_3d = empresa.CD_CODIGO_3D,
                             }).ToList();
                return query;
            }
        }


        public int GetMaxDocumentID()
        { 
            using (CardinalEntities ctx = new CardinalEntities())
            {
               return Convert.ToInt32(ctx.TB_EMPRESA_DOCUMENTOS.Max(d => d.CD_CODIGO_DOCUMENTO));
            }
        }
    }
}
