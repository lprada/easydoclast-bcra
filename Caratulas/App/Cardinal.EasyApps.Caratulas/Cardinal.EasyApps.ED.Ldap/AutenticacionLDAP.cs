﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.DirectoryServices;
using Cardinal.EasyApps.ED.Dal.EntityFramework;
using Cardinal.EasyApps.ED.Model;


namespace Cardinal.EasyApps.ED.Ldap
{

    public class AutenticacionLDAP
    {

        private string _path;

        private string _filterAttribute;
        public AutenticacionLDAP(string path)
        {
            _path = path;
        }

        public static bool AutenticarUsuario(string srvr, string usr, string pwd)
        {
            string ldapString = "LDAP://" + srvr;
            bool authenticated = false;
            UsuarioLDAP usuario = new UsuarioLDAP();

            try
            {
                DirectoryEntry entry = new DirectoryEntry(ldapString, usr, pwd);
                object nativeObject = entry.NativeObject;
                DirectorySearcher busqueda = new DirectorySearcher(entry);
                busqueda.Filter = "(SAMAccountName=" + usr + ")";
                busqueda.PropertiesToLoad.Add("cn");
                busqueda.PropertiesToLoad.Add("memberOf");
                busqueda.PropertiesToLoad.Add("givenName");
                busqueda.PropertiesToLoad.Add("sn");
                busqueda.PropertiesToLoad.Add("mail");

                SearchResult resultado = default(SearchResult);
                resultado = busqueda.FindOne();


                string nombre = null;
                string apellido = null;
                string email = null;

                try
                {
                    nombre = resultado.Properties["givenName"][0].ToString();
                }
                catch (Exception ex)
                {
                }

                try
                {
                    apellido = resultado.Properties["sn"][0].ToString();
                }
                catch (Exception ex)
                {
                }

                try
                {
                    email = resultado.Properties["mail"][0].ToString();
                }
                catch (Exception ex)
                {
                }



                //authenticated = True
                usuario.ID = usr;
                usuario.Nombre = nombre + " " + apellido;
                usuario.Email = email;

                if (usuario.ID != null)
                {
                    //si no existe usuario, lo creo
                    MatchOdoUser(usuario);

                    //devuelvo el LoginAuth OK
                    authenticated = true; 
                }

                //no autenticado por cex
            }
            catch (DirectoryServicesCOMException cex)
            {
                //no autenticado por algun otro motivo
            }
            catch (Exception ex)
            {
            }            

            return authenticated;
        }

        private static void MatchOdoUser(UsuarioLDAP usr)
        {
            EDEntities ctx = new EDEntities();
            var odoUser = ctx.Usuarios.SingleOrDefault(p => p.Nombre == usr.ID);

            if (odoUser == null)
            {
                Usuario nuevoUsr = new Usuario();
                nuevoUsr.UsuarioId = Guid.NewGuid();
                nuevoUsr.AplicacionId = new Guid("551238AC-7488-4F48-8699-4EEA398E40A7");
                nuevoUsr.Email = usr.Email;
                nuevoUsr.Nombre = usr.ID;
                nuevoUsr.Descripcion = usr.Nombre;
                nuevoUsr.PasswordFormat = 1;
                nuevoUsr.Password = String.Empty;
                nuevoUsr.PasswordSalt = String.Empty;
                nuevoUsr.EstaAprobado = true;
                nuevoUsr.EstaBloqueado = false;
                nuevoUsr.EstaEliminado = false;
                nuevoUsr.FechaCreacion = DateTime.Now;
                nuevoUsr.CantidadFallasIngresoPassword = 0;
                nuevoUsr.CantidadFallasIngresoRespuestaPassword = 0;
                nuevoUsr.DebeCambiarPassword = false;
                Guid grupoId = new Guid("FD93269C-7A96-4C7A-9AC8-3A192D5FB356");
                var grupo = ctx.Grupoes.SingleOrDefault(p => p.GrupoId == grupoId);
                nuevoUsr.Grupoes.Add(grupo);

                ctx.Usuarios.AddObject(nuevoUsr);

                ctx.SaveChanges();
            }
        }

    }
}