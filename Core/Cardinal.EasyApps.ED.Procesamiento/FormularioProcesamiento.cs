﻿using System;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;
using Accusoft.ImagXpressSdk;

namespace Cardinal.EasyApps.ED.Procesamiento
{
    /// <summary>
    ///
    /// </summary>
    public partial class FormularioProcesamiento : Form
    {
        Cardinal.EasyApps.ED.Common.Dal.IRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="FormularioProcesamiento"/> class.
        /// </summary>
        public FormularioProcesamiento()
        {
            InitializeComponent();
            _repository = new Cardinal.EasyApps.ED.Dal.EntityFramework.Repository(ConfigurationManager.ConnectionStrings["EDEntities"].ConnectionString);
        }

        private void FormularioProcesamiento_Load(object sender, EventArgs e)
        {
            CambiarMensajeTarea("Sin Tarea");
        }

        private void CambiarMensajeTarea(string msg)
        {
            tareaTextBox.Text = msg;
            Application.DoEvents();
        }

        /// <summary>
        /// Loads the image.
        /// </summary>
        /// <param name="filename">The filename.</param>
        public void LoadImage(string filename)
        {
            CambiarMensajeTarea("Cargando Imagen");

            if (imageXView.Image != null)
            {
                imageXView.Image.Dispose();
                imageXView.Image = null;
            }
            imageXView.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress, filename);
            Application.DoEvents();
        }

        /// <summary>
        /// Detectars the imagen en blanco.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns></returns>
        public bool DetectarImagenEnBlanco(string filename)
        {
            CambiarMensajeTarea("Detectando Imagen en Blanco");

            System.IO.FileInfo fileinfo = new System.IO.FileInfo(@filename);
            long filesize = fileinfo.Length;

            return filesize <= 5000;
        }

        /// <summary>
        /// Detectars the patch code.
        /// </summary>
        /// <returns></returns>
        public Cardinal.EasyApps.ED.Model.PatchCodeValues DetectarPatchCode()
        {
            CambiarMensajeTarea("Detectando Patch Codes");

            Cardinal.EasyApps.ED.Model.PatchCodeValues nivel = Model.PatchCodeValues.INVALIDO;
            imageXViewTemp.Image = imageXView.Image.Copy();

            if (imageXViewTemp.Image.ImageXData.BitsPerPixel != 0)
            {
                processor = new Processor(imagXpress, imageXViewTemp.Image);
                processor.ColorDepth(1, 0, 0);
            }

            int DIB;
            DIB = imageXViewTemp.Image.ToHdib(false).ToInt32();
            System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
            currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PatchCodeBarcode, 0);
            barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes;
            PegasusImaging.WinForms.BarcodeXpress5.Result[] results = barcodeXpress1.reader.Analyze(DIB);

            for (int i = 0; i < results.Length; i++)
            {
                PegasusImaging.WinForms.BarcodeXpress5.Result curResult = (PegasusImaging.WinForms.BarcodeXpress5.Result)results.GetValue(i);

                if (results[i].BarcodeType == PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PatchCodeBarcode)
                {
                    switch (results[i].BarcodeValue)
                    {
                        case "1001":
                            nivel = Model.PatchCodeValues.KIPATCH1;
                            break;
                        case "1010":
                        case "0101":
                            nivel = Model.PatchCodeValues.KIPATCH2;
                            break;
                        default:
                            nivel = Model.PatchCodeValues.INVALIDO;
                            break;
                    }

                    break;
                }
            }
            return nivel;
        }

        /// <summary>
        /// Deskews this instance.
        /// </summary>
        public void Deskew()
        {
            CambiarMensajeTarea("Realizando Deskew");

            processor = new Processor(imagXpress, imageXView.Image);
            if (imageXView.Image.ImageXData.BitsPerPixel != 1)
            {
                processor.Deskew(DeskewType.Normal);
                float porcentaje = 0.95F;
                processor.CropBorder(porcentaje, CropType.CropBorderBlackColor);
            }
            else
            {
                processor.DocumentDeskew(0.2, 50, Color.White, false, 90);
                processor.DocumentBorderCrop();
                //float porcentaje = 0.95F;
                //processor.CropBorder(porcentaje, CropType.CropBorderBlackColor);
            }
            Application.DoEvents();
        }

        /// <summary>
        /// Realizars the procesamiento default.
        /// </summary>
        public void RealizarProcesamientoDefault()
        {
            CambiarMensajeTarea("Realizando Procesamiento Default");

            processor = new Processor(imagXpress, imageXView.Image);
            if (imageXView.Image.ImageXData.BitsPerPixel != 1)
            {
                processor.Despeckle();
            }
            else
            {
                processor.DocumentLineRemoval(50, 20, 12, 1, 20);
                processor.DocumentDespeckle(1, 1);
            }
            Application.DoEvents();
        }

        /// <summary>
        /// Procesars the imagen.
        /// </summary>
        public void ProcesarImagen(string filename, Model.Lote lote, int nroOrden, int indice)
        {
            CambiarMensajeTarea("Procesando Imagen");
            //System.Threading.Thread.Sleep(5000);
            //Deskew
            Deskew();
            //MinSize
            var result = DetectarImagenEnBlanco(filename);

            if (result)
            {
                _repository.PonerEnPapelera(lote, nroOrden);
            }
            //PatchCodeRecognition
            var patchCode = DetectarPatchCode();
            if (patchCode != Model.PatchCodeValues.INVALIDO)
            {
                _repository.MarcarComoSubLote(lote, indice + 1, patchCode, true);
            }
            //BarCode

            RealizarProcesamientoDefault();
        }

        /// <summary>
        /// Saves the image.
        /// </summary>
        /// <param name="filename">The filename.</param>
        public void SaveImage(string filename)
        {
            CambiarMensajeTarea("Grabando Imagen");

            SaveOptions saveOptions = new SaveOptions();
            if (imageXView.Image.ImageXData.BitsPerPixel == 1)
            {
                saveOptions.Tiff.Compression = Compression.Group4;
            }

            imageXView.Image.Save(filename, saveOptions);
            Application.DoEvents();
        }
    }
}