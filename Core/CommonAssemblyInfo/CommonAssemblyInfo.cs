using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyCompany("Cardinal Systems")]
[assembly: AssemblyProduct("EasyDoc")]
[assembly: AssemblyCopyright("Copyright  Cardinal Systems 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: CLSCompliant(false)]
//[assembly: NeutralResourcesLanguageAttribute("es", UltimateResourceFallbackLocation.Satellite)]


// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]


// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:
[assembly: AssemblyVersion("2011.2.0901.1214")]
[assembly: AssemblyFileVersion("2011.2.0901.1214")]