﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model.ScanLocal
{
    /// <summary>
    /// Tipo de Campo
    /// </summary>
    public enum TipoCampoLocal
    {
        /// <summary>
        /// Tipo Texto
        /// </summary>
        Texto = 1,
        /// <summary>
        /// Tipo Fecha
        /// </summary>
        Fecha = 2,
        /// <summary>
        /// Tipo Numerico
        /// </summary>
        Numerico = 4,
        /// <summary>
        /// Tipo Combo
        /// </summary>
        Combo = 3
    }
}
