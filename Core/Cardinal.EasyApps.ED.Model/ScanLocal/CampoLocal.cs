﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model.ScanLocal
{

  

    /// <summary>
    /// Campo 
    /// </summary>
    public class CampoLocal
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CampoLocal"/> class.
        /// </summary>
        public CampoLocal()
        {
            //ValoresPosibles = new List<KeyValuePair<string, string>>();
        }
        /// <summary>
        /// Gets or sets the campo id.
        /// </summary>
        /// <value>
        /// The campo id.
        /// </value>
        public int CampoId { get; set; }
        /// <summary>
        /// Gets or sets the nombre.
        /// </summary>
        /// <value>
        /// The nombre.
        /// </value>
        public string Nombre { get; set; }
        

        /// <summary>
        /// Gets or sets a value indicating whether [es obligatorio].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [es obligatorio]; otherwise, <c>false</c>.
        /// </value>
        public bool EsObligatorio { get; set; }
        /// <summary>
        /// Gets or sets the cantidad caracteres maximos.
        /// </summary>
        /// <value>
        /// The cantidad caracteres maximos.
        /// </value>
        public int? CantidadCaracteresMaximos { get; set; }
        /// <summary>
        /// Gets or sets the max valor.
        /// </summary>
        /// <value>
        /// The max valor.
        /// </value>
        public string MaxValor { get; set; }
        /// <summary>
        /// Gets or sets the min valor.
        /// </summary>
        /// <value>
        /// The min valor.
        /// </value>
        public string MinValor { get; set; }

        ///// <summary>
        ///// Gets or sets the valores posibles.
        ///// </summary>
        ///// <value>
        ///// The valores posibles.
        ///// </value>
        //public List<KeyValuePair<string, string>> ValoresPosibles { get; set; }


        /// <summary>
        /// Gets or sets the tipo campo.
        /// </summary>
        /// <value>
        /// The tipo campo.
        /// </value>
        public TipoCampoLocal TipoCampo
        {
            get
            {
                return (TipoCampoLocal)Tipo;
            }
            set
            {
                Tipo = (Int16)value;	
            }
        }


        /// <summary>
        /// Gets or sets the tipo.
        /// </summary>
        /// <value>
        /// The tipo.
        /// </value>
        public Int16 Tipo { get; set; }



        /// <summary>
        /// Gets or sets the codigo.
        /// </summary>
        /// <value>
        /// The codigo.
        /// </value>
        public string Codigo { get; set; }

        /// <summary>
        /// Gets or sets the mascara.
        /// </summary>
        /// <value>
        /// The mascara.
        /// </value>
        public string Mascara { get; set; }

    }
}
