﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Lote para Importar
    /// </summary>
    public class LoteImportar : Lote
    {
        /// <summary>
        /// Gets or sets the codigo plantilla volumen.
        /// </summary>
        /// <value>
        /// The codigo plantilla volumen.
        /// </value>
        public string CodigoPlantillaVolumen { get; set; }

        /// <summary>
        /// Gets or sets the codigo caja.
        /// </summary>
        /// <value>
        /// The codigo caja.
        /// </value>
        public string CodigoCaja { get; set; }

        /// <summary>
        /// Gets or sets the path local lote.
        /// </summary>
        /// <value>
        /// The path local lote.
        /// </value>
        public string PathLocalLote { get; set; }

        /// <summary>
        /// Gets or sets the codigo lote easy doc.
        /// </summary>
        /// <value>
        /// The codigo lote easy doc.
        /// </value>
        public string CodigoLoteEasyDoc { get; set; }

        /// <summary>
        /// Gets the codigo plantilla.
        /// </summary>
        public string CodigoPlantilla
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(CodigoPlantillaVolumen))
                {
                    string codigoPlantilla;
                    string codigoVolumen;
                    SepararPlantillaVolumen(out codigoPlantilla, out codigoVolumen);

                 
                        return codigoPlantilla;
                  
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets the codigo volumen.
        /// </summary>
        public string CodigoVolumen
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(CodigoPlantillaVolumen))
                {
                    string codigoPlantilla;
                    string codigoVolumen;
                    SepararPlantillaVolumen(out codigoPlantilla, out codigoVolumen);


                    return codigoVolumen;

                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [usa transferencia].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [usa transferencia]; otherwise, <c>false</c>.
        /// </value>
        public bool UsaTransferencia { get; set; }

        /// <summary>
        /// Gets or sets the codigo estacion escaneo importacion.
        /// </summary>
        /// <value>
        /// The codigo estacion escaneo importacion.
        /// </value>
        public string CodigoEstacionEscaneoImportacion { get; set; }
            




        /// <summary>
        /// Separars the plantilla volumen.
        /// </summary>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <param name="codigoVolumen">The codigo volumen.</param>
        private void SepararPlantillaVolumen(out string codigoPlantilla, out string codigoVolumen)
        {
            var info = CodigoPlantillaVolumen.Split(new string[] { ";***;" }, StringSplitOptions.None);
            codigoPlantilla = info[0];
            codigoVolumen = info[1];
        }
    }
}
