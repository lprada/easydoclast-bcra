﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// 
    /// </summary>
   public  class FTPHost
    {
        private string _id;

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        /// <value>
        /// The ID.
        /// </value>
        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _host;

        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        /// <value>
        /// The host.
        /// </value>
        public string Host
        {
            get { return _host; }
            set { _host = value; }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="FTPHost"/> class.
        /// </summary>
        /// <param name="id">The id.</param>
        public FTPHost(string id)
            
        {
            this.Host = "";
            ID = id;
        }
    }
}
