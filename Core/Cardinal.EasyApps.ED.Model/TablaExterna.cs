﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Tabla Externa
    /// </summary>
    public class TablaExterna
    {
        /// <summary>
        /// Gets or sets the nombre.
        /// </summary>
        /// <value>
        /// The nombre.
        /// </value>
        public string Nombre { get; set; }

        /// <summary>
        /// Gets or sets the campo id.
        /// </summary>
        /// <value>
        /// The campo id.
        /// </value>
        public string CampoId { get; set; }

        /// <summary>
        /// Gets or sets the campo externo.
        /// </summary>
        /// <value>
        /// The campo externo.
        /// </value>
        public string CampoExterno { get; set; }

        /// <summary>
        /// Gets or sets the valores posibles.
        /// </summary>
        /// <value>
        /// The valores posibles.
        /// </value>
        public List<KeyValuePair<string, string>> ValoresPosibles { get; set; }


        /// <summary>
        /// Gets or sets the codigo plantilla.
        /// </summary>
        /// <value>
        /// The codigo plantilla.
        /// </value>
        public string CodigoPlantilla { get; set; }
    }
}
