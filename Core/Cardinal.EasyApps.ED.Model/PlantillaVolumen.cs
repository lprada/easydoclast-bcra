﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Plantilla con los Volumenes Habilitados para la misma
    /// </summary>
    public class PlantillaVolumen : Plantilla
    {
        private Volumen _volumen;

        /// <summary>
        /// Gets or sets the volumen.
        /// </summary>
        /// <value>
        /// The volumen.
        /// </value>
        public Volumen Volumen
        {
            get { return _volumen; }
            set { _volumen = value; }
        }

        /// <summary>
        /// Gets or sets the codigo plantilla volumen.
        /// </summary>
        /// <value>
        /// The codigo plantilla volumen.
        /// </value>
        public string CodigoPlantillaVolumen { get; set; }

    }
}
