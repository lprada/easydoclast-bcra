﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// 
    /// </summary>
   public  class DocumentoEmpresa
    {
        /// <summary>
        /// Gets or sets the empresa.
        /// </summary>
        /// <value>
        /// The empresa.
        /// </value>
       public Cliente empresa{get;set;}
       /// <summary>
       /// Gets or sets the codigo documento.
       /// </summary>
       /// <value>
       /// The codigo documento.
       /// </value>
       public int codigoDocumento { get; set; }
       /// <summary>
       /// Gets or sets the plantilla.
       /// </summary>
       /// <value>
       /// The plantilla.
       /// </value>
       public string plantilla { get; set; }
       /// <summary>
       /// Gets or sets the tx plantilla.
       /// </summary>
       /// <value>
       /// The tx plantilla.
       /// </value>
       public string txPlantilla { get; set; }
       /// <summary>
       /// Gets or sets the usuario carga.
       /// </summary>
       /// <value>
       /// The usuario carga.
       /// </value>
       public string usuarioCarga { get; set; }
       /// <summary>
       /// Gets or sets the estado documento.
       /// </summary>
       /// <value>
       /// The estado documento.
       /// </value>
       public string estadoDocumento { get; set; }
       /// <summary>
       /// Gets or sets the fecha creacion.
       /// </summary>
       /// <value>
       /// The fecha creacion.
       /// </value>
       public DateTime fechaCreacion { get; set; }
       /// <summary>
       /// Gets or sets the codigo3d.
       /// </summary>
       /// <value>
       /// The codigo3d.
       /// </value>
       public string codigo3d { get; set; }
       /// <summary>
       /// Gets or sets the lista campos.
       /// </summary>
       /// <value>
       /// The lista campos.
       /// </value>
       public IList<CamposCS> listaCampos { get; set; }
    }
}
