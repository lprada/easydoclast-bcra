﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{

  

    /// <summary>
    /// Campo de una Plantilla
    /// </summary>
    public class PlantillaCampo
    {
        /// <summary>
        /// Gets or sets the campo id.
        /// </summary>
        /// <value>
        /// The campo id.
        /// </value>
        public string CampoId { get; set; }
        /// <summary>
        /// Gets or sets the nombre.
        /// </summary>
        /// <value>
        /// The nombre.
        /// </value>
        public string Nombre { get; set; }
        /// <summary>
        /// Gets or sets the orden.
        /// </summary>
        /// <value>
        /// The orden.
        /// </value>
        public int Orden { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [es obligatorio].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [es obligatorio]; otherwise, <c>false</c>.
        /// </value>
        public bool EsObligatorio { get; set; }
        /// <summary>
        /// Gets or sets the cantidad caracteres maximos.
        /// </summary>
        /// <value>
        /// The cantidad caracteres maximos.
        /// </value>
        public int CantidadCaracteresMaximos { get; set; }
        /// <summary>
        /// Gets or sets the max valor.
        /// </summary>
        /// <value>
        /// The max valor.
        /// </value>
        public string MaxValor { get; set; }
        /// <summary>
        /// Gets or sets the min valor.
        /// </summary>
        /// <value>
        /// The min valor.
        /// </value>
        public string MinValor { get; set; }

        /// <summary>
        /// Gets or sets the valores posibles.
        /// </summary>
        /// <value>
        /// The valores posibles.
        /// </value>
        public List<KeyValuePair<string, string>> ValoresPosibles { get; set; }



        /// <summary>
        /// Gets or sets a value indicating whether [usa tabla descripciones].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [usa tabla descripciones]; otherwise, <c>false</c>.
        /// </value>
        public bool UsaTablaDescripciones { get; set; }


        /// <summary>
        /// Gets or sets the tipo campo.
        /// </summary>
        /// <value>
        /// The tipo campo.
        /// </value>
        public TipoCampo TipoCampo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [es campo detalle].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [es campo detalle]; otherwise, <c>false</c>.
        /// </value>
        public bool EsCampoDetalle{ get; set; }

        /// <summary>
        /// Mascara del Campo
        /// </summary>
        public string Mascara {get; set; }


    }
}
