﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// 
    /// </summary>
    public  class CaratulaCS
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CaratulaCS"/> class.
        /// </summary>
        public CaratulaCS()
        {
            DetalleCampos = new List<CamposCS>();
        }


        /// <summary>
        /// Gets or sets the empresa.
        /// </summary>
        /// <value>
        /// The empresa.
        /// </value>
        public string empresa { get; set; }
        /// <summary>
        /// Gets or sets the nombre_empresa.
        /// </summary>
        /// <value>
        /// The nombre_empresa.
        /// </value>
        public string nombre_empresa { get; set; }
        /// <summary>
        /// Gets or sets the usuario.
        /// </summary>
        /// <value>
        /// The usuario.
        /// </value>
        public string usuario { get; set; }
        /// <summary>
        /// Gets or sets the fecha.
        /// </summary>
        /// <value>
        /// The fecha.
        /// </value>
        public DateTime fecha { get; set; }
     
        ///// <summary>
        ///// Gets or sets the valor.
        ///// </summary>
        ///// <value>
        ///// The valor.
        ///// </value>
        //public string valor { get; set; }
        /// <summary>
        /// Gets or sets the plantilla.
        /// </summary>
        /// <value>
        /// The plantilla.
        /// </value>
        public string plantilla { get; set; }

        /// <summary>
        /// Gets or sets the codigo plantilla.
        /// </summary>
        /// <value>
        /// The codigo plantilla.
        /// </value>
        public string CodigoPlantilla { get; set; }
        /// <summary>
        /// Gets or sets the codigo_3d.
        /// </summary>
        /// <value>
        /// The codigo_3d.
        /// </value>
        public string codigo_3d { get; set; }



        /// <summary>
        /// Gets or sets the codigo.
        /// </summary>
        /// <value>
        /// The codigo.
        /// </value>
        public long codigo { get; set; }


        /// <summary>
        /// Gets or sets the detalle campos.
        /// </summary>
        /// <value>
        /// The detalle campos.
        /// </value>
        public List<CamposCS> DetalleCampos { get; set; }

        /// <summary>
        /// Gets or sets the fecha inbound.
        /// </summary>
        /// <value>
        /// The fecha inbound.
        /// </value>
        public DateTime? FechaInbound { get; set; }

        /// <summary>
        /// Gets or sets the fecha outbound.
        /// </summary>
        /// <value>
        /// The fecha outbound.
        /// </value>
        public DateTime? FechaOutbound { get; set; }


        /// <summary>
        /// Gets or sets the fecha proceso.
        /// </summary>
        /// <value>
        /// The fecha proceso.
        /// </value>
        public DateTime? FechaProceso { get; set; }





    }
}
