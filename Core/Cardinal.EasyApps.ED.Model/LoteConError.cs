﻿namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class LoteConError : Lote
    {
        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public string Error { get; set; }
    }
}