﻿using System;
using System.Globalization;
using System.Collections.Generic;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Imagen
    /// </summary>
    public class Imagen
    {
        /// <summary>
        /// Gets or sets the codigo lote.
        /// </summary>
        /// <value>
        /// The codigo lote.
        /// </value>
        public string CodigoLote { get; set; }

        /// <summary>
        /// Gets or sets the nro orden.
        /// </summary>
        /// <value>
        /// The nro orden.
        /// </value>
        public int NroOrden { get; set; }

        /// <summary>
        /// Gets or sets the indice.
        /// </summary>
        /// <value>
        /// The indice.
        /// </value>
        public int Indice { get; set; }

        /// <summary>
        /// Gets or sets the volumen.
        /// </summary>
        /// <value>
        /// The volumen.
        /// </value>
        public string Volumen { get; set; }

        /// <summary>
        /// Gets or sets the extension.
        /// </summary>
        /// <value>
        /// The extension.
        /// </value>
        public string Extension { get; set; }

        /// <summary>
        /// Gets or sets the tipo.
        /// </summary>
        /// <value>
        /// The tipo.
        /// </value>
        public int Tipo { get; set; }

        /// <summary>
        /// Gets or sets the path red imagen.
        /// </summary>
        /// <value>
        /// The path red imagen.
        /// </value>
        public string PathRedImagen { get; set; }

        /// <summary>
        /// Gets the path red imagen full.
        /// </summary>
        public string PathRedImagenFull
        {
            get
            {
                return GetPathFull(PathRedImagen, false);
            }
        }

      

        /// <summary>
        /// Gets the path full.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="addVolumen">if set to <c>true</c> [add volumen].</param>
        /// <returns></returns>
        public string GetPathFull(string path, bool addVolumen)
        {
            string pathFull = string.Empty;

            if (!string.IsNullOrWhiteSpace(path))
            {
                if (addVolumen)
                {
                    pathFull = System.IO.Path.Combine(path, Volumen);
                }
                else
                {
                    pathFull = path;
                }

                pathFull = System.IO.Path.Combine(pathFull, CodigoLote);
                pathFull = System.IO.Path.Combine(pathFull, String.Format(CultureInfo.InvariantCulture, "{0:00000000}.{1}", NroOrden, Extension));
            }
            return pathFull;
        }
    }
}