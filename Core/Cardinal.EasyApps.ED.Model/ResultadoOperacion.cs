﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Rsultado Operacion
    /// </summary>
    public class ResultadoOperacion
    {
        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public string Result { get; set; }
    }
}
