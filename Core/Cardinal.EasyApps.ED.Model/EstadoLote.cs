﻿namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Estado de Lote
    /// </summary>
    public enum EstadoLote
    {
        /// <summary>
        /// Eliminado
        /// </summary>
        Eliminado = -100,
        /// <summary>
        /// Error al Transferir desde el Escaner
        /// </summary>
        ErrorTransferirEscaner = -89,
        /// <summary>
        /// Error al Transferir a Procesamienta
        /// </summary>
        ErrorTransferirProcesamiento = -88,
        /// <summary>
        /// Error al Procesar
        /// </summary>
        ErrorProcesar = -87,

        /// <summary>
        /// Pendiente de Transferir desde el Escaner o Importacion
        /// </summary>
        PendienteTransferirEscaner = -10,
        /// <summary>
        /// Pendiente de Transferir al Procesamiento
        /// </summary>
        PendienteTransferirProcesamiento = -9,
        /// <summary>
        /// Pendiente de Procesar
        /// </summary>
        PendienteProcesar = -8,
        /// <summary>
        /// En Uso
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "En")]
        EnUso = -1,

      /// <summary>
      /// Disponible
      /// </summary>
        Disponible = 0
    }
}