/*
   Tuesday, June 02, 20099:44:33 AM
   User: sa
   Server: 192.168.16.2
   Database: EasyDocNet
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OperacionEntregas
	DROP CONSTRAINT FK_OperacionEntregas_Empresas
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Empresas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Empresas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Empresas', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_OperacionEntregas
	(
	OperacionEntregaId int NOT NULL IDENTITY (1, 1),
	LoginEmpresa varchar(20) NOT NULL,
	OperacionId varchar(50) NOT NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_OperacionEntregas OFF
GO
IF EXISTS(SELECT * FROM dbo.OperacionEntregas)
	 EXEC('INSERT INTO dbo.Tmp_OperacionEntregas (LoginEmpresa, OperacionId)
		SELECT LoginEmpresa, OperacionId FROM dbo.OperacionEntregas WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.OperacionEntregas
GO
EXECUTE sp_rename N'dbo.Tmp_OperacionEntregas', N'OperacionEntregas', 'OBJECT' 
GO
ALTER TABLE dbo.OperacionEntregas ADD CONSTRAINT
	PK_OperacionEntregas PRIMARY KEY CLUSTERED 
	(
	OperacionEntregaId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.OperacionEntregas ADD CONSTRAINT
	FK_OperacionEntregas_Empresas FOREIGN KEY
	(
	LoginEmpresa
	) REFERENCES dbo.Empresas
	(
	LoginEmpresa
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.OperacionEntregas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.OperacionEntregas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.OperacionEntregas', 'Object', 'CONTROL') as Contr_Per 