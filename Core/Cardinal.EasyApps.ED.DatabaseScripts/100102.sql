/*
   Tuesday, May 31, 20112:20:27 PM
   User: 
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.CajaActividad
	(
	ActividadCajaId bigint NOT NULL IDENTITY (1, 1),
	cod_caja varchar(30) NOT NULL,
	TareaUsuarioId bigint NOT NULL,
	FechaInicio datetime NOT NULL,
	FechaFin datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.CajaActividad SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.CajaActividad', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.CajaActividad', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.CajaActividad', 'Object', 'CONTROL') as Contr_Per 