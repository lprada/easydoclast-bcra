/*
   Monday, November 23, 20095:07:17 PM
   User: sa
   Server: THEFORCE\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Notificacion
	DROP CONSTRAINT DF_Notificacion_Leida
GO
ALTER TABLE dbo.Notificacion
	DROP CONSTRAINT DF_Notificacion_Archivada
GO
CREATE TABLE dbo.Tmp_Notificacion
	(
	NotificacionId uniqueidentifier NOT NULL,
	FechaAlta datetime NOT NULL,
	NumIdentificacion bigint NOT NULL IDENTITY (1, 1),
	Identificador nchar(20) NOT NULL,
	UsuarioId char(50) NOT NULL,
	Mensaje nvarchar(MAX) NOT NULL,
	Leida bit NOT NULL,
	Archivada bit NOT NULL,
	FechaLectura datetime NULL,
	FechaArchivo datetime NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Notificacion SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Notificacion ADD CONSTRAINT
	DF_Notificacion_Leida DEFAULT ('False') FOR Leida
GO
ALTER TABLE dbo.Tmp_Notificacion ADD CONSTRAINT
	DF_Notificacion_Archivada DEFAULT ('False') FOR Archivada
GO
SET IDENTITY_INSERT dbo.Tmp_Notificacion OFF
GO
IF EXISTS(SELECT * FROM dbo.Notificacion)
	 EXEC('INSERT INTO dbo.Tmp_Notificacion (NotificacionId, FechaAlta, Identificador, UsuarioId, Mensaje, Leida, Archivada, FechaLectura, FechaArchivo)
		SELECT NotificacionId, FechaAlta, Identificador, UsuarioId, Mensaje, Leida, Archivada, FechaLectura, FechaArchivo FROM dbo.Notificacion WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Notificacion
GO
EXECUTE sp_rename N'dbo.Tmp_Notificacion', N'Notificacion', 'OBJECT' 
GO
ALTER TABLE dbo.Notificacion ADD CONSTRAINT
	PK_Notificacion PRIMARY KEY CLUSTERED 
	(
	NotificacionId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.Notificacion', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Notificacion', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Notificacion', 'Object', 'CONTROL') as Contr_Per 