/*
   Tuesday, June 02, 20099:45:47 AM
   User: sa
   Server: 192.168.16.2
   Database: EasyDocNet
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.OperacionEntregaDetalle
	(
	OperacionEntregaDetalleId int NOT NULL IDENTITY (1, 1),
	OperacionEntregaId int NOT NULL,
	Orden smallint NOT NULL,
	Proceso varchar(500) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.OperacionEntregaDetalle ADD CONSTRAINT
	DF_OperacionEntregaDetalle_Orden DEFAULT 0 FOR Orden
GO
ALTER TABLE dbo.OperacionEntregaDetalle ADD CONSTRAINT
	PK_OperacionEntregaDetalle PRIMARY KEY CLUSTERED 
	(
	OperacionEntregaDetalleId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.OperacionEntregaDetalle', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.OperacionEntregaDetalle', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.OperacionEntregaDetalle', 'Object', 'CONTROL') as Contr_Per 