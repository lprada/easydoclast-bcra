/*
   Thursday, July 31, 20084:18:23 PM
   User: 
   Server: ANAKIN\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.EmpresaSector
	(
	LoginEmpresa varchar(20) NOT NULL,
	SectorId uniqueidentifier NOT NULL,
	Nombre varchar(100) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.EmpresaSector ADD CONSTRAINT
	PK_EmpresaSector PRIMARY KEY CLUSTERED 
	(
	SectorId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'CONTROL') as Contr_Per 