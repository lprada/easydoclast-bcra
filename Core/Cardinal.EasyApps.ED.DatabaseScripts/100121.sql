/*
   Thursday, September 29, 20114:37:03 PM
   User: sa
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.user_T ADD
	UsuarioFull bit NULL,
	UsuarioServicio bit NULL
GO
ALTER TABLE dbo.user_T SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
