/*
   Wednesday, November 18, 20099:35:17 AM
   User: sa
   Server: THEFORCE\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DocumentoTag ADD
	Eliminada bit NOT NULL CONSTRAINT DF_DocumentoTag_Eliminada DEFAULT 'False'
GO
ALTER TABLE dbo.DocumentoTag SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.DocumentoTag', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.DocumentoTag', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.DocumentoTag', 'Object', 'CONTROL') as Contr_Per 