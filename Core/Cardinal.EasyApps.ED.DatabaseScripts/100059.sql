INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (262
           ,'es_AR'     
           ,'Importacion de Imagenes')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (262
           ,'en_us'     
           ,'Imagen Import')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (263
           ,'es_AR'     
           ,'Escaneo Web de Imagenes')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (263
           ,'en_us'     
           ,'Imagen Web Scan')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmBusqueda.lnkImportacionArchivos'
           ,262)
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmBusqueda.lnkEscaneoWeb'
           ,263)
GO

INSERT INTO role_task_T
           (role_task_id
           ,descr
           ,lastNumber_bi)
     VALUES
           ('IMPOIMG'
           ,'Importacion de Imanges'
           ,0)
GO

INSERT INTO role_task_T
           (role_task_id
           ,descr
           ,lastNumber_bi)
     VALUES
           ('WEBSCANIMG'
           ,'Escaneo Web de Imanges'
           ,0)
GO

INSERT INTO task_T
           (task_id
           ,descr
           ,lastNumber_bi)
     VALUES
           ('ADDIMG'
           ,'Agregado de Imagenes'
           ,0)
GO





update Version
set DBVersion = '100059'
where Product = 'EasyDoc'
GO

select * from Version
where Product = 'EasyDoc'
GO