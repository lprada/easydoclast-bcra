/*
   Thursday, September 29, 20114:47:48 PM
   User: sa
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_user_T
	(
	user_id char(50) NOT NULL,
	nameComplete varchar(200) NOT NULL,
	descr varchar(200) NULL,
	changePassword char(1) NULL,
	notAllowChangePassword char(1) NULL,
	bloq char(1) NULL,
	del char(1) NULL,
	password char(100) NOT NULL,
	networkAlias varchar(200) NULL,
	fechalogin datetime NULL,
	fechapassword datetime NULL,
	reintetoslogin int NULL,
	email varchar(200) NULL,
	TipoUsuario char(1) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_user_T SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.user_T)
	 EXEC('INSERT INTO dbo.Tmp_user_T (user_id, nameComplete, descr, changePassword, notAllowChangePassword, bloq, del, password, networkAlias, fechalogin, fechapassword, reintetoslogin, email, TipoUsuario)
		SELECT user_id, nameComplete, descr, changePassword, notAllowChangePassword, bloq, del, password, networkAlias, fechalogin, fechapassword, reintetoslogin, email, TipoUsuario FROM dbo.user_T WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.membership_user_role_t
	DROP CONSTRAINT fk_membership_user_role_t_user_T
GO
ALTER TABLE dbo.membership_T
	DROP CONSTRAINT fk_membership_T_user_T
GO
ALTER TABLE dbo.audit_T
	DROP CONSTRAINT fk_audit_T_user_T
GO
ALTER TABLE dbo.permissions_users_T
	DROP CONSTRAINT fk_permissions_users_T_user_T
GO
ALTER TABLE dbo.ActividadUsuario
	DROP CONSTRAINT FK_ActividadUsuario_user_T
GO
DROP TABLE dbo.user_T
GO
EXECUTE sp_rename N'dbo.Tmp_user_T', N'user_T', 'OBJECT' 
GO
ALTER TABLE dbo.user_T ADD CONSTRAINT
	pk_user_T PRIMARY KEY CLUSTERED 
	(
	user_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ActividadUsuario ADD CONSTRAINT
	FK_ActividadUsuario_user_T FOREIGN KEY
	(
	user_id
	) REFERENCES dbo.user_T
	(
	user_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ActividadUsuario SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.permissions_users_T ADD CONSTRAINT
	fk_permissions_users_T_user_T FOREIGN KEY
	(
	user_id_fk
	) REFERENCES dbo.user_T
	(
	user_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.permissions_users_T SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.audit_T ADD CONSTRAINT
	fk_audit_T_user_T FOREIGN KEY
	(
	user_id_fk
	) REFERENCES dbo.user_T
	(
	user_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.audit_T SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.membership_T ADD CONSTRAINT
	fk_membership_T_user_T FOREIGN KEY
	(
	user_id_fk
	) REFERENCES dbo.user_T
	(
	user_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.membership_T SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.membership_user_role_t ADD CONSTRAINT
	fk_membership_user_role_t_user_T FOREIGN KEY
	(
	user_id_fk
	) REFERENCES dbo.user_T
	(
	user_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.membership_user_role_t SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
