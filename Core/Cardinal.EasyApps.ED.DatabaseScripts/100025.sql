/*
   Thursday, July 31, 20084:21:21 PM
   User: 
   Server: ANAKIN\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
COMMIT
select Has_Perms_By_Name(N'dbo.TmplatesResponsable', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.TmplatesResponsable', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.TmplatesResponsable', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	FK_TmplatesDatosComerciales_EmpresaSector FOREIGN KEY
	(
	SectorId
	) REFERENCES dbo.EmpresaSector
	(
	SectorId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	FK_TmplatesDatosComerciales_TmplatesResponsable FOREIGN KEY
	(
	ResponsableId
	) REFERENCES dbo.TmplatesResponsable
	(
	ResponsableId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.TmplatesDatosComerciales', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.TmplatesDatosComerciales', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.TmplatesDatosComerciales', 'Object', 'CONTROL') as Contr_Per 