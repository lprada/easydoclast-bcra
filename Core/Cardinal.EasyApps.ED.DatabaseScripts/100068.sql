/*
   Monday, November 23, 20093:20:49 PM
   User: sa
   Server: THEFORCE\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Notificacion
	(
	NotificacionId uniqueidentifier NOT NULL,
	FechaAlta datetime NOT NULL,
	Identificador nchar(20) NOT NULL,
	UsuarioId char(50) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Notificacion ADD CONSTRAINT
	PK_Notificacion PRIMARY KEY CLUSTERED 
	(
	NotificacionId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Notificacion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Notificacion', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Notificacion', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Notificacion', 'Object', 'CONTROL') as Contr_Per 