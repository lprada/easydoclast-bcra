INSERT INTO dicdatos
           ([campo]
           ,[descripcio]
           ,[tipo]
           ,[longitud]
           ,[decimales]
           ,[rango_min]
           ,[rango_max]
           ,[mascara]
           ,[interno])
     VALUES
           ('NROCAR'
           ,'NUMERO CARATULA'
           ,12
           ,70
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,0)
INSERT INTO dicdatos
           ([campo]
           ,[descripcio]
           ,[tipo]
           ,[longitud]
           ,[decimales]
           ,[rango_min]
           ,[rango_max]
           ,[mascara]
           ,[interno])
     VALUES
           ('TMPL'
           ,'PLANTILLA'
           ,12
           ,10
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,0)
		   
INSERT INTO tmplates
           ([tmplate]
           ,[descripcio]
           ,[tipoindex]
           ,[periodica]
           ,[nro_zonas]
           ,[tipo]
           ,[parent]
           ,[datasrc]
           ,[ocr]
           ,[produccion]
           ,[LoginEmpresa]
           ,[BillingType]
           ,[usaMergeDatos]
           ,[diasprocesoWarning]
           ,[diasprocesoError]
           ,[firmadigi]
           ,[SeteosCaptura]
           ,[FormatoCapturaBN]
           ,[FormatoCapturaGrey]
           ,[FormatoCapturaColor]
           ,[orderImagenes]
           ,[cajasMensuales]
           ,[SeteosIndexacion])
     VALUES
           ('ZCARAT'
           ,'Caratulas'
           ,0
           ,0
           ,0
           ,'T'
           ,1
           ,1
           ,1
           ,0
           ,'INT'
           ,'IMG'
           ,0
           ,0
           ,0
           ,'N'
           ,null
           ,null
           ,null
           ,null
           ,'A'
           ,0
           ,null)

INSERT INTO zona
           ([tmplate]
           ,[campo]
           ,[orden]
           ,[tp]
           ,[lft]
           ,[bttm]
           ,[rght]
           ,[mndtory]
           ,[ocr1]
           ,[ocr2]
           ,[ocr3]
           ,[ocr4]
           ,[ocr5]
           ,[reftab]
           ,[campoext]
           ,[datasrc]
           ,[visual]
           ,[filtrotabext])
     VALUES
           ('ZCARAT'
           ,'NROCAR'
           ,1
           ,0
           ,0
           ,0
           ,0
           ,null
           ,null
           ,null
           ,null
           ,null
           ,0
           ,null
           ,null
           ,1
           ,'00'
           ,null)
INSERT INTO zona
           ([tmplate]
           ,[campo]
           ,[orden]
           ,[tp]
           ,[lft]
           ,[bttm]
           ,[rght]
           ,[mndtory]
           ,[ocr1]
           ,[ocr2]
           ,[ocr3]
           ,[ocr4]
           ,[ocr5]
           ,[reftab]
           ,[campoext]
           ,[datasrc]
           ,[visual]
           ,[filtrotabext])
     VALUES
           ('ZCARAT'
           ,'TMPL'
           ,2
           ,0
           ,0
           ,0
           ,0
           ,null
           ,null
           ,null
           ,null
           ,null
           ,0
           ,null
           ,null
           ,1
           ,'01'
           ,null)
		   
		   
CREATE TABLE ZCARATAB(
	[COD_LOTE] [varchar](20) NULL,
	[NRO_ORDEN] [int] NULL,
	[VOLUMEN] [varchar](8)NULL,
	[ROTACION] [int] NULL,
	[TIPO] [int] NULL,
	[TAMANO] [int] NULL,
	[INDICE] [int] NULL,
	NROCAR [varchar](70) NULL,
		TMPL [varchar](10) NULL
)
CREATE TABLE ZCARATAA(
	[COD_LOTE] [varchar](20) NULL,
	[NRO_ORDEN] [int] NULL,
	[VOLUMEN] [varchar](8)NULL,
	[ROTACION] [int] NULL,
	[TIPO] [int] NULL,
	[TAMANO] [int] NULL,
	[INDICE] [int] NULL,
	NROCAR [varchar](70) NULL,
		TMPL [varchar](10) NULL

)
CREATE TABLE ZCARATAC(
	[COD_LOTE] [varchar](20) NULL,
	[NRO_ORDEN] [int] NULL,
	[VOLUMEN] [varchar](8)NULL,
	[ROTACION] [int] NULL,
	[TIPO] [int] NULL,
	[TAMANO] [int] NULL,
	[INDICE] [int] NULL,
		NROCAR [varchar](70) NULL,
		TMPL [varchar](10) NULL

)