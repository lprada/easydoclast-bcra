/*
   Thursday, July 31, 20084:23:55 PM
   User: 
   Server: ANAKIN\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.TmplatesDatosComerciales
	DROP CONSTRAINT FK_TmplatesDatosComerciales_TmplatesResponsable
GO
COMMIT
select Has_Perms_By_Name(N'dbo.TmplatesResponsable', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.TmplatesResponsable', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.TmplatesResponsable', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.TmplatesDatosComerciales
	DROP CONSTRAINT FK_TmplatesDatosComerciales_EmpresaSector
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.TmplatesDatosComerciales
	DROP CONSTRAINT DF_TmplatesDatosComerciales_BillingType
GO
ALTER TABLE dbo.TmplatesDatosComerciales
	DROP CONSTRAINT DF_TmplatesDatosComerciales_DiasProcesoWarning
GO
ALTER TABLE dbo.TmplatesDatosComerciales
	DROP CONSTRAINT DF_TmplatesDatosComerciales_DiasProcesoError
GO
ALTER TABLE dbo.TmplatesDatosComerciales
	DROP CONSTRAINT DF_TmplatesDatosComerciales_CajasMensuales
GO
ALTER TABLE dbo.TmplatesDatosComerciales
	DROP CONSTRAINT DF_TmplatesDatosComerciales_PeriodicidadDias
GO
ALTER TABLE dbo.TmplatesDatosComerciales
	DROP CONSTRAINT DF_TmplatesDatosComerciales_VolumenEsperadoMensual
GO
CREATE TABLE dbo.Tmp_TmplatesDatosComerciales
	(
	Tmplate varchar(8) NOT NULL,
	BillingType nvarchar(50) NOT NULL,
	DiasProcesoWarning int NOT NULL,
	DiasProcesoError int NOT NULL,
	CajasMensuales int NOT NULL,
	SectorId uniqueidentifier NOT NULL,
	ResponsableId uniqueidentifier NOT NULL,
	PeriodicidadDias tinyint NOT NULL,
	VolumenEsperadoDiario bigint NOT NULL,
	VolumenEsperadoSemanal bigint NOT NULL,
	VolumenEsperadoMensual bigint NOT NULL,
	Instructivo nvarchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_BillingType DEFAULT ('IMG') FOR BillingType
GO
ALTER TABLE dbo.Tmp_TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_DiasProcesoWarning DEFAULT ((0)) FOR DiasProcesoWarning
GO
ALTER TABLE dbo.Tmp_TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_DiasProcesoError DEFAULT ((0)) FOR DiasProcesoError
GO
ALTER TABLE dbo.Tmp_TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_CajasMensuales DEFAULT ((0)) FOR CajasMensuales
GO
ALTER TABLE dbo.Tmp_TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_PeriodicidadDias DEFAULT ((0)) FOR PeriodicidadDias
GO
ALTER TABLE dbo.Tmp_TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_VolumenEsperadoMensual DEFAULT ((0)) FOR VolumenEsperadoMensual
GO
IF EXISTS(SELECT * FROM dbo.TmplatesDatosComerciales)
	 EXEC('INSERT INTO dbo.Tmp_TmplatesDatosComerciales (Tmplate, BillingType, DiasProcesoWarning, DiasProcesoError, CajasMensuales, SectorId, ResponsableId, PeriodicidadDias, VolumenEsperadoDiario, VolumenEsperadoSemanal, VolumenEsperadoMensual, Instructivo)
		SELECT CONVERT(varchar(8), Tmplate), BillingType, DiasProcesoWarning, DiasProcesoError, CajasMensuales, SectorId, ResponsableId, PeriodicidadDias, VolumenEsperadoDiario, VolumenEsperadoSemanal, VolumenEsperadoMensual, Instructivo FROM dbo.TmplatesDatosComerciales WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.TmplatesDatosComerciales
GO
EXECUTE sp_rename N'dbo.Tmp_TmplatesDatosComerciales', N'TmplatesDatosComerciales', 'OBJECT' 
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	PK_TmplatesDatosComerciales PRIMARY KEY CLUSTERED 
	(
	Tmplate
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	FK_TmplatesDatosComerciales_EmpresaSector FOREIGN KEY
	(
	SectorId
	) REFERENCES dbo.EmpresaSector
	(
	SectorId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	FK_TmplatesDatosComerciales_TmplatesResponsable FOREIGN KEY
	(
	ResponsableId
	) REFERENCES dbo.TmplatesResponsable
	(
	ResponsableId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	FK_TmplatesDatosComerciales_TmplatesDatosComerciales FOREIGN KEY
	(
	Tmplate
	) REFERENCES dbo.TmplatesDatosComerciales
	(
	Tmplate
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.TmplatesDatosComerciales', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.TmplatesDatosComerciales', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.TmplatesDatosComerciales', 'Object', 'CONTROL') as Contr_Per 