/*
   Tuesday, October 27, 20092:41:54 PM
   User: sa
   Server: THEFORCE\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.DocumentoImagen
	(
	DocumentoId uniqueidentifier NOT NULL,
	cod_lote varchar(50) NOT NULL,
	nro_orden int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.DocumentoImagen ADD CONSTRAINT
	PK_DocumentoImagen PRIMARY KEY CLUSTERED 
	(
	DocumentoId,
	cod_lote,
	nro_orden
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.DocumentoImagen SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.DocumentoImagen', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.DocumentoImagen', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.DocumentoImagen', 'Object', 'CONTROL') as Contr_Per 