/*
   Tuesday, May 31, 20112:20:56 PM
   User: 
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.TareaUsuario SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.TareaUsuario', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.TareaUsuario', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.TareaUsuario', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Cajas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Cajas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Cajas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Cajas', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.CajaActividad ADD CONSTRAINT
	PK_CajaActividad PRIMARY KEY CLUSTERED 
	(
	ActividadCajaId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.CajaActividad ADD CONSTRAINT
	FK_CajaActividad_Cajas FOREIGN KEY
	(
	cod_caja
	) REFERENCES dbo.Cajas
	(
	cod_caja
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.CajaActividad ADD CONSTRAINT
	FK_CajaActividad_TareaUsuario FOREIGN KEY
	(
	TareaUsuarioId
	) REFERENCES dbo.TareaUsuario
	(
	TareaUsuarioId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.CajaActividad SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.CajaActividad', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.CajaActividad', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.CajaActividad', 'Object', 'CONTROL') as Contr_Per 