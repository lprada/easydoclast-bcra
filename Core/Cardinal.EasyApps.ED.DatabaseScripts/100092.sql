CREATE SCHEMA ODO
GO
/****** Object:  Table [ODO].[Objeto]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ODO].[Objeto](
	[ObjetoId] [uniqueidentifier] NOT NULL,
	[AplicacionId] [uniqueidentifier] NOT NULL,
	[TipoObjetoId] [uniqueidentifier] NOT NULL,
	[Descripcion] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Objecto_1] PRIMARY KEY CLUSTERED 
(
	[ObjetoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ODO].[Aplicacion]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ODO].[Aplicacion](
	[AplicacionId] [uniqueidentifier] NOT NULL,
	[NombreAplicacion] [nvarchar](256) NOT NULL,
	[Descripcion] [nvarchar](256) NULL,
 CONSTRAINT [PK_Aplicacion] PRIMARY KEY CLUSTERED 
(
	[AplicacionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ODO].[Tarea]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ODO].[Tarea](
	[TareaId] [uniqueidentifier] NOT NULL,
	[AplicacionId] [uniqueidentifier] NOT NULL,
	[Nombre] [nvarchar](256) NOT NULL,
	[UsaObjecto] [bit] NOT NULL,
	[UltimoNumero] [bigint] NOT NULL,
	[UsaObjectoExterno] [bit] NOT NULL,
	[EstaEliminada] [bit] NOT NULL,
	[LastUpdated] [timestamp] NOT NULL,
 CONSTRAINT [PK_Tarea_1] PRIMARY KEY CLUSTERED 
(
	[TareaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ODO].[Usuario]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ODO].[Usuario](
	[UsuarioId] [uniqueidentifier] NOT NULL,
	[AplicacionId] [uniqueidentifier] NOT NULL,
	[Email] [nvarchar](500) NULL,
	[Nombre] [nvarchar](200) NOT NULL,
	[Descripcion] [nvarchar](250) NULL,
	[Password] [nvarchar](1000) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](100) NOT NULL,
	[PasswordPregunta] [nvarchar](1000) NULL,
	[PasswordRespuesta] [nvarchar](500) NULL,
	[EstaAprobado] [bit] NOT NULL,
	[EstaEliminado] [bit] NOT NULL,
	[EstaBloqueado] [bit] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaUltimoLogin] [datetime] NULL,
	[FechaUltimoCambioPassword] [datetime] NULL,
	[FechaUltimoBloqueo] [datetime] NULL,
	[CantidadFallasIngresoPassword] [int] NULL,
	[FechaPrimerIntentoFallidoIngresoPassword] [datetime] NULL,
	[CantidadFallasIngresoRespuestaPassword] [int] NULL,
	[FechaPrimerIntentoFallidoRespuestaPassword] [datetime] NULL,
	[Comentario] [nvarchar](1000) NULL,
	[LastUpdated] [timestamp] NOT NULL,
	[DebeCambiarPassword] [bit] NULL,
	[CodigoIdenficatorio] [nvarchar](50) NULL,
	[IPAcceso] [nvarchar](100) NULL,
	[InicioHorarioAcceso] [tinyint] NULL,
	[FinHorarioAcceso] [tinyint] NULL,
 CONSTRAINT [PK_Usuario_1] PRIMARY KEY CLUSTERED 
(
	[UsuarioId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ODO].[PermisosUsuario]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ODO].[PermisosUsuario](
	[PermisosUsuarioId] [uniqueidentifier] NOT NULL,
	[UsuarioId] [uniqueidentifier] NOT NULL,
	[TareaId] [uniqueidentifier] NOT NULL,
	[ObjetoId] [uniqueidentifier] NULL,
	[ObjetoExternoId] [nvarchar](500) NULL,
 CONSTRAINT [PK_PermisosUsuario] PRIMARY KEY CLUSTERED 
(
	[PermisosUsuarioId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ODO].[LoginToken]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ODO].[LoginToken](
	[LoginTokenId] [uniqueidentifier] NOT NULL,
	[Token] [varchar](25) NOT NULL,
	[Creado] [datetime] NOT NULL,
	[IPAddress] [varchar](50) NULL,
	[UsuarioId] [uniqueidentifier] NOT NULL,
	[LastChanged] [timestamp] NOT NULL,
 CONSTRAINT [PK_LoginToken_1] PRIMARY KEY CLUSTERED 
(
	[LoginTokenId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ODO].[Grupo]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ODO].[Grupo](
	[GrupoId] [uniqueidentifier] NOT NULL,
	[AplicacionId] [uniqueidentifier] NOT NULL,
	[Nombre] [nvarchar](256) NOT NULL,
	[Descripcion] [nvarchar](500) NULL,
 CONSTRAINT [PK_Grupo_1] PRIMARY KEY CLUSTERED 
(
	[GrupoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ODO].[UsuarioGrupo]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ODO].[UsuarioGrupo](
	[UsuarioId] [uniqueidentifier] NOT NULL,
	[GrupoId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_UsuarioGrupo] PRIMARY KEY CLUSTERED 
(
	[UsuarioId] ASC,
	[GrupoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ODO].[PermisosGrupo]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ODO].[PermisosGrupo](
	[PermisosGrupoId] [uniqueidentifier] NOT NULL,
	[GrupoId] [uniqueidentifier] NOT NULL,
	[TareaId] [uniqueidentifier] NOT NULL,
	[ObjetoId] [uniqueidentifier] NULL,
	[ObjetoExternoId] [nvarchar](500) NULL,
 CONSTRAINT [PK_PermisosGrupo] PRIMARY KEY CLUSTERED 
(
	[PermisosGrupoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ODO].[TipoObjeto]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ODO].[TipoObjeto](
	[AplicacionId] [uniqueidentifier] NOT NULL,
	[TipoObjetoId] [uniqueidentifier] NOT NULL,
	[Descripcion] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_TipoObjeto_1] PRIMARY KEY CLUSTERED 
(
	[TipoObjetoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ODO].[Auditoria]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ODO].[Auditoria](
	[AuditoriaId] [uniqueidentifier] NOT NULL,
	[AplicacionId] [uniqueidentifier] NOT NULL,
	[UsuarioId] [uniqueidentifier] NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[TareaId] [uniqueidentifier] NULL,
	[TareaNumero] [int] NULL,
	[Comentario] [nvarchar](100) NULL,
 CONSTRAINT [PK_Auditoria_1] PRIMARY KEY CLUSTERED 
(
	[AuditoriaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ODO].[UsuarioHistoriaPassword]    Script Date: 04/30/2011 09:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ODO].[UsuarioHistoriaPassword](
	[UsuarioId] [uniqueidentifier] NOT NULL,
	[PasswordHash] [nvarchar](400) NOT NULL,
	[FechaCambio] [datetime] NOT NULL,
 CONSTRAINT [PK_UsuarioHistoriaPassword] PRIMARY KEY CLUSTERED 
(
	[UsuarioId] ASC,
	[PasswordHash] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_Tarea_UsaObjectoExterno]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[Tarea] ADD  CONSTRAINT [DF_Tarea_UsaObjectoExterno]  DEFAULT ((0)) FOR [UsaObjectoExterno]
GO
/****** Object:  ForeignKey [FK_Auditoria_Aplicacion]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[Auditoria]  WITH CHECK ADD  CONSTRAINT [FK_Auditoria_Aplicacion] FOREIGN KEY([AplicacionId])
REFERENCES [ODO].[Aplicacion] ([AplicacionId])
GO
ALTER TABLE [ODO].[Auditoria] CHECK CONSTRAINT [FK_Auditoria_Aplicacion]
GO
/****** Object:  ForeignKey [FK_Auditoria_Tarea]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[Auditoria]  WITH CHECK ADD  CONSTRAINT [FK_Auditoria_Tarea] FOREIGN KEY([TareaId])
REFERENCES [ODO].[Tarea] ([TareaId])
GO
ALTER TABLE [ODO].[Auditoria] CHECK CONSTRAINT [FK_Auditoria_Tarea]
GO
/****** Object:  ForeignKey [FK_Auditoria_Usuario1]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[Auditoria]  WITH CHECK ADD  CONSTRAINT [FK_Auditoria_Usuario1] FOREIGN KEY([UsuarioId])
REFERENCES [ODO].[Usuario] ([UsuarioId])
GO
ALTER TABLE [ODO].[Auditoria] CHECK CONSTRAINT [FK_Auditoria_Usuario1]
GO
/****** Object:  ForeignKey [FK_Grupo_Aplicacion]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[Grupo]  WITH CHECK ADD  CONSTRAINT [FK_Grupo_Aplicacion] FOREIGN KEY([AplicacionId])
REFERENCES [ODO].[Aplicacion] ([AplicacionId])
GO
ALTER TABLE [ODO].[Grupo] CHECK CONSTRAINT [FK_Grupo_Aplicacion]
GO
/****** Object:  ForeignKey [FK_LoginToken_Usuario]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[LoginToken]  WITH CHECK ADD  CONSTRAINT [FK_LoginToken_Usuario] FOREIGN KEY([UsuarioId])
REFERENCES [ODO].[Usuario] ([UsuarioId])
GO
ALTER TABLE [ODO].[LoginToken] CHECK CONSTRAINT [FK_LoginToken_Usuario]
GO
/****** Object:  ForeignKey [FK_Objecto_Aplicacion]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[Objeto]  WITH CHECK ADD  CONSTRAINT [FK_Objecto_Aplicacion] FOREIGN KEY([AplicacionId])
REFERENCES [ODO].[Aplicacion] ([AplicacionId])
GO
ALTER TABLE [ODO].[Objeto] CHECK CONSTRAINT [FK_Objecto_Aplicacion]
GO
/****** Object:  ForeignKey [FK_Objeto_TipoObjeto]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[Objeto]  WITH CHECK ADD  CONSTRAINT [FK_Objeto_TipoObjeto] FOREIGN KEY([TipoObjetoId])
REFERENCES [ODO].[TipoObjeto] ([TipoObjetoId])
GO
ALTER TABLE [ODO].[Objeto] CHECK CONSTRAINT [FK_Objeto_TipoObjeto]
GO
/****** Object:  ForeignKey [FK_PermisosGrupo_Grupo]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[PermisosGrupo]  WITH CHECK ADD  CONSTRAINT [FK_PermisosGrupo_Grupo] FOREIGN KEY([GrupoId])
REFERENCES [ODO].[Grupo] ([GrupoId])
GO
ALTER TABLE [ODO].[PermisosGrupo] CHECK CONSTRAINT [FK_PermisosGrupo_Grupo]
GO
/****** Object:  ForeignKey [FK_PermisosGrupo_Objecto]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[PermisosGrupo]  WITH CHECK ADD  CONSTRAINT [FK_PermisosGrupo_Objecto] FOREIGN KEY([ObjetoId])
REFERENCES [ODO].[Objeto] ([ObjetoId])
GO
ALTER TABLE [ODO].[PermisosGrupo] CHECK CONSTRAINT [FK_PermisosGrupo_Objecto]
GO
/****** Object:  ForeignKey [FK_PermisosGrupo_Tarea]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[PermisosGrupo]  WITH CHECK ADD  CONSTRAINT [FK_PermisosGrupo_Tarea] FOREIGN KEY([TareaId])
REFERENCES [ODO].[Tarea] ([TareaId])
GO
ALTER TABLE [ODO].[PermisosGrupo] CHECK CONSTRAINT [FK_PermisosGrupo_Tarea]
GO
/****** Object:  ForeignKey [FK_PermisosUsuario_Objecto]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[PermisosUsuario]  WITH CHECK ADD  CONSTRAINT [FK_PermisosUsuario_Objecto] FOREIGN KEY([ObjetoId])
REFERENCES [ODO].[Objeto] ([ObjetoId])
GO
ALTER TABLE [ODO].[PermisosUsuario] CHECK CONSTRAINT [FK_PermisosUsuario_Objecto]
GO
/****** Object:  ForeignKey [FK_PermisosUsuario_Tarea]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[PermisosUsuario]  WITH CHECK ADD  CONSTRAINT [FK_PermisosUsuario_Tarea] FOREIGN KEY([TareaId])
REFERENCES [ODO].[Tarea] ([TareaId])
GO
ALTER TABLE [ODO].[PermisosUsuario] CHECK CONSTRAINT [FK_PermisosUsuario_Tarea]
GO
/****** Object:  ForeignKey [FK_PermisosUsuario_Usuario]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[PermisosUsuario]  WITH CHECK ADD  CONSTRAINT [FK_PermisosUsuario_Usuario] FOREIGN KEY([UsuarioId])
REFERENCES [ODO].[Usuario] ([UsuarioId])
GO
ALTER TABLE [ODO].[PermisosUsuario] CHECK CONSTRAINT [FK_PermisosUsuario_Usuario]
GO
/****** Object:  ForeignKey [FK_Tarea_Aplicacion]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[Tarea]  WITH CHECK ADD  CONSTRAINT [FK_Tarea_Aplicacion] FOREIGN KEY([AplicacionId])
REFERENCES [ODO].[Aplicacion] ([AplicacionId])
GO
ALTER TABLE [ODO].[Tarea] CHECK CONSTRAINT [FK_Tarea_Aplicacion]
GO
/****** Object:  ForeignKey [FK_TipoObjeto_Aplicacion]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[TipoObjeto]  WITH CHECK ADD  CONSTRAINT [FK_TipoObjeto_Aplicacion] FOREIGN KEY([AplicacionId])
REFERENCES [ODO].[Aplicacion] ([AplicacionId])
GO
ALTER TABLE [ODO].[TipoObjeto] CHECK CONSTRAINT [FK_TipoObjeto_Aplicacion]
GO
/****** Object:  ForeignKey [FK_Usuario_Aplicacion]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Aplicacion] FOREIGN KEY([AplicacionId])
REFERENCES [ODO].[Aplicacion] ([AplicacionId])
GO
ALTER TABLE [ODO].[Usuario] CHECK CONSTRAINT [FK_Usuario_Aplicacion]
GO
/****** Object:  ForeignKey [FK_UsuarioGrupo_Grupo]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[UsuarioGrupo]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioGrupo_Grupo] FOREIGN KEY([GrupoId])
REFERENCES [ODO].[Grupo] ([GrupoId])
GO
ALTER TABLE [ODO].[UsuarioGrupo] CHECK CONSTRAINT [FK_UsuarioGrupo_Grupo]
GO
/****** Object:  ForeignKey [FK_UsuarioGrupo_Usuario]    Script Date: 04/30/2011 09:51:43 ******/
ALTER TABLE [ODO].[UsuarioGrupo]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioGrupo_Usuario] FOREIGN KEY([UsuarioId])
REFERENCES [ODO].[Usuario] ([UsuarioId])
GO
ALTER TABLE [ODO].[UsuarioGrupo] CHECK CONSTRAINT [FK_UsuarioGrupo_Usuario]
GO
