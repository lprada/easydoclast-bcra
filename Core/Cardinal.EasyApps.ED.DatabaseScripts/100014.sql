UPDATE Empresas set DescEmpresa ='Empresa Interna' where LoginEmpresa='INT'

INSERT INTO dicdatos
           ([campo]
           ,[descripcio]
           ,[tipo]
           ,[longitud]
           ,[decimales]
           ,[rango_min]
           ,[rango_max]
           ,[mascara]
           ,[interno])
     VALUES
           ('PRUEBA'
           ,'PRUEBA'
           ,12
           ,50
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,0)

INSERT INTO tmplates
           ([tmplate]
           ,[descripcio]
           ,[tipoindex]
           ,[periodica]
           ,[nro_zonas]
           ,[tipo]
           ,[parent]
           ,[datasrc]
           ,[ocr]
           ,[produccion]
           ,[LoginEmpresa]
           ,[BillingType]
           ,[usaMergeDatos]
           ,[diasprocesoWarning]
           ,[diasprocesoError]
           ,[firmadigi]
           ,[SeteosCaptura]
           ,[FormatoCapturaBN]
           ,[FormatoCapturaGrey]
           ,[FormatoCapturaColor]
           ,[orderImagenes]
           ,[cajasMensuales]
           ,[SeteosIndexacion])
     VALUES
           ('ZAAAAA'
           ,'PRUEBA'
           ,0
           ,0
           ,0
           ,'T'
           ,1
           ,1
           ,1
           ,0
           ,'INT'
           ,'IMG'
           ,0
           ,0
           ,0
           ,'N'
           ,null
           ,null
           ,null
           ,null
           ,'A'
           ,0
           ,null)

INSERT INTO zona
           ([tmplate]
           ,[campo]
           ,[orden]
           ,[tp]
           ,[lft]
           ,[bttm]
           ,[rght]
           ,[mndtory]
           ,[ocr1]
           ,[ocr2]
           ,[ocr3]
           ,[ocr4]
           ,[ocr5]
           ,[reftab]
           ,[campoext]
           ,[datasrc]
           ,[visual]
           ,[filtrotabext])
     VALUES
           ('ZAAAAA'
           ,'PRUEBA'
           ,1
           ,0
           ,0
           ,0
           ,0
           ,null
           ,null
           ,null
           ,null
           ,null
           ,0
           ,null
           ,null
           ,1
           ,'00'
           ,null)

CREATE TABLE ZAAAAAAB(
	[COD_LOTE] [varchar](20) NULL,
	[NRO_ORDEN] [int] NULL,
	[VOLUMEN] [varchar](8)NULL,
	[ROTACION] [int] NULL,
	[TIPO] [int] NULL,
	[TAMANO] [int] NULL,
	[INDICE] [int] NULL,
	PRUEBA [varchar](50) NULL
)
CREATE TABLE ZAAAAAAA(
	[COD_LOTE] [varchar](20) NULL,
	[NRO_ORDEN] [int] NULL,
	[VOLUMEN] [varchar](8)NULL,
	[ROTACION] [int] NULL,
	[TIPO] [int] NULL,
	[TAMANO] [int] NULL,
	[INDICE] [int] NULL,
	PRUEBA [varchar](50) NULL
)
CREATE TABLE ZAAAAAAC(
	[COD_LOTE] [varchar](20) NULL,
	[NRO_ORDEN] [int] NULL,
	[VOLUMEN] [varchar](8)NULL,
	[ROTACION] [int] NULL,
	[TIPO] [int] NULL,
	[TAMANO] [int] NULL,
	[INDICE] [int] NULL,
	PRUEBA [varchar](50) NULL
)

INSERT INTO vols
           ([volumen]
           ,[coddev]
           ,[finiciovalidez]
           ,[ffinvalidez])
     VALUES
           ('VOLPRUE'
           ,1
           ,null
           ,null)

INSERT INTO montaje
           ([pt]
           ,[volumen]
           ,[mount]
           ,[acceso]
           ,[host])
     VALUES
           (0
           ,'VOLPRUE'
           ,'c:\tmp\VOLPRUE'
           ,0
           ,null)
