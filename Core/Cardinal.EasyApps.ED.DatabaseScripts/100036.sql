/*
   Thursday, December 11, 20082:22:41 PM
   User: sa
   Server: csds-svr
   Database: EasyDocNet
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.idlotes
	DROP CONSTRAINT fk_idlotes_tmplates
GO
COMMIT
select Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.idlotes
	DROP CONSTRAINT idlotesfeccracion
GO
ALTER TABLE dbo.idlotes
	DROP CONSTRAINT DF_idlotes_controlrealizado
GO
CREATE TABLE dbo.Tmp_idlotes
	(
	cod_lote varchar(50) NOT NULL,
	tmplate varchar(8) NOT NULL,
	act_nro smallint NULL,
	ult_nro smallint NULL,
	indexando smallint NULL,
	caja int NULL,
	feccreacion datetime NULL,
	scanner varchar(20) NULL,
	controlrealizado smallint NULL,
	tag varchar(255) NULL,
	usrindexando varchar(255) NULL,
	ipindexando varchar(255) NULL,
	enIndexacion varchar(50) NULL,
	fechaUltAcceso datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_idlotes ADD CONSTRAINT
	idlotesfeccracion DEFAULT (getdate()) FOR feccreacion
GO
ALTER TABLE dbo.Tmp_idlotes ADD CONSTRAINT
	DF_idlotes_controlrealizado DEFAULT ((0)) FOR controlrealizado
GO
IF EXISTS(SELECT * FROM dbo.idlotes)
	 EXEC('INSERT INTO dbo.Tmp_idlotes (cod_lote, tmplate, act_nro, ult_nro, indexando, caja, feccreacion, scanner, controlrealizado, tag, usrindexando, ipindexando, enIndexacion, fechaUltAcceso)
		SELECT cod_lote, tmplate, act_nro, ult_nro, indexando, caja, feccreacion, scanner, controlrealizado, tag, usrindexando, ipindexando, CONVERT(varchar(50), enIndexacion), fechaUltAcceso FROM dbo.idlotes WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.invalidos
	DROP CONSTRAINT fk_invalidos_idlotes
GO
ALTER TABLE dbo.papelera
	DROP CONSTRAINT fk_papelera_idlotes
GO
ALTER TABLE dbo.LD_auditoriaDocumentos_T
	DROP CONSTRAINT fk_LD_auditoriaDocumentos_T_idlotes
GO
ALTER TABLE dbo.SecuenciaLotesDetalle
	DROP CONSTRAINT FK_SecuenciaLotesDetalle_idlotes
GO
ALTER TABLE dbo.SecuenciaLotes
	DROP CONSTRAINT FK_SecuenciaLotes_idlotes
GO
ALTER TABLE dbo.SubLotes
	DROP CONSTRAINT fk_SubLotes_idlotes
GO
ALTER TABLE dbo.lotes
	DROP CONSTRAINT fk_lotes_idlotes
GO
ALTER TABLE dbo.anotacion
	DROP CONSTRAINT fk_Anotacion_Idlotes
GO
ALTER TABLE dbo.LotesCajas
	DROP CONSTRAINT fk_LotesCajas_idlotes
GO
DROP TABLE dbo.idlotes
GO
EXECUTE sp_rename N'dbo.Tmp_idlotes', N'idlotes', 'OBJECT' 
GO
ALTER TABLE dbo.idlotes ADD CONSTRAINT
	PK__idlotes__6E01572D PRIMARY KEY CLUSTERED 
	(
	cod_lote
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.idlotes ADD CONSTRAINT
	fk_idlotes_tmplates FOREIGN KEY
	(
	tmplate
	) REFERENCES dbo.tmplates
	(
	tmplate
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.LotesCajas ADD CONSTRAINT
	fk_LotesCajas_idlotes FOREIGN KEY
	(
	cod_lote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.LotesCajas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.LotesCajas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.LotesCajas', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.anotacion ADD CONSTRAINT
	fk_Anotacion_Idlotes FOREIGN KEY
	(
	codlote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.anotacion', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.anotacion', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.anotacion', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.lotes ADD CONSTRAINT
	fk_lotes_idlotes FOREIGN KEY
	(
	cod_lote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.lotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.lotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.lotes', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.SubLotes ADD CONSTRAINT
	fk_SubLotes_idlotes FOREIGN KEY
	(
	cod_lote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.SubLotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.SubLotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.SubLotes', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.SecuenciaLotes ADD CONSTRAINT
	FK_SecuenciaLotes_idlotes FOREIGN KEY
	(
	LoteInicioSecuencia
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.SecuenciaLotesDetalle ADD CONSTRAINT
	FK_SecuenciaLotesDetalle_idlotes FOREIGN KEY
	(
	Lote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.SecuenciaLotesDetalle', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotesDetalle', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotesDetalle', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.LD_auditoriaDocumentos_T ADD CONSTRAINT
	fk_LD_auditoriaDocumentos_T_idlotes FOREIGN KEY
	(
	lote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.LD_auditoriaDocumentos_T', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.LD_auditoriaDocumentos_T', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.LD_auditoriaDocumentos_T', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.papelera ADD CONSTRAINT
	fk_papelera_idlotes FOREIGN KEY
	(
	cod_lote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.papelera', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.papelera', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.papelera', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.invalidos ADD CONSTRAINT
	fk_invalidos_idlotes FOREIGN KEY
	(
	cod_lote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.invalidos', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.invalidos', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.invalidos', 'Object', 'CONTROL') as Contr_Per 