/*
This script was created by Visual Studio on 4/30/2011 at 10:01 AM.
Run this script on [THEFORCEULTIMAT.EasyDoc.Database] to make it the same as [THEFORCEULTIMAT.PruebaODOED].
This script performs its actions in the following order:
1. Disable foreign-key constraints.
2. Perform DELETE commands. 
3. Perform UPDATE commands.
4. Perform INSERT commands.
5. Re-enable foreign-key constraints.
Please back up your target database before running this script.
*/
SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
/*Pointer used for text / image updates. This might not be needed, but is declared here just in case*/
DECLARE @pv binary(16)
BEGIN TRANSACTION
ALTER TABLE [ODO].[LoginToken] DROP CONSTRAINT [FK_LoginToken_Usuario]
ALTER TABLE [ODO].[PermisosUsuario] DROP CONSTRAINT [FK_PermisosUsuario_Objecto]
ALTER TABLE [ODO].[PermisosUsuario] DROP CONSTRAINT [FK_PermisosUsuario_Tarea]
ALTER TABLE [ODO].[PermisosUsuario] DROP CONSTRAINT [FK_PermisosUsuario_Usuario]
ALTER TABLE [ODO].[Grupo] DROP CONSTRAINT [FK_Grupo_Aplicacion]
ALTER TABLE [ODO].[Tarea] DROP CONSTRAINT [FK_Tarea_Aplicacion]
ALTER TABLE [ODO].[TipoObjeto] DROP CONSTRAINT [FK_TipoObjeto_Aplicacion]
ALTER TABLE [ODO].[UsuarioGrupo] DROP CONSTRAINT [FK_UsuarioGrupo_Grupo]
ALTER TABLE [ODO].[UsuarioGrupo] DROP CONSTRAINT [FK_UsuarioGrupo_Usuario]
ALTER TABLE [ODO].[Usuario] DROP CONSTRAINT [FK_Usuario_Aplicacion]
ALTER TABLE [ODO].[PermisosGrupo] DROP CONSTRAINT [FK_PermisosGrupo_Grupo]
ALTER TABLE [ODO].[PermisosGrupo] DROP CONSTRAINT [FK_PermisosGrupo_Objecto]
ALTER TABLE [ODO].[PermisosGrupo] DROP CONSTRAINT [FK_PermisosGrupo_Tarea]
ALTER TABLE [ODO].[Objeto] DROP CONSTRAINT [FK_Objecto_Aplicacion]
ALTER TABLE [ODO].[Objeto] DROP CONSTRAINT [FK_Objeto_TipoObjeto]
ALTER TABLE [ODO].[Auditoria] DROP CONSTRAINT [FK_Auditoria_Aplicacion]
ALTER TABLE [ODO].[Auditoria] DROP CONSTRAINT [FK_Auditoria_Tarea]
ALTER TABLE [ODO].[Auditoria] DROP CONSTRAINT [FK_Auditoria_Usuario1]
INSERT INTO [ODO].[Aplicacion] ([AplicacionId], [NombreAplicacion], [Descripcion]) VALUES (N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ODO', N'Sistema de Administracion de Seguridad y Auditoria')
INSERT INTO [ODO].[Objeto] ([ObjetoId], [AplicacionId], [TipoObjetoId], [Descripcion]) VALUES (N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'486ea9bc-4745-4776-8949-7c51d37cc92a', N'Sistema de Administracion de Seguridad y Auditoria')
INSERT INTO [ODO].[Objeto] ([ObjetoId], [AplicacionId], [TipoObjetoId], [Descripcion]) VALUES (N'b16a466c-2f1e-4352-adda-5d567cd34784', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'486ea9bc-4745-4776-8949-7c51d37cc92a', N'Sistema de Administracion de Archivos')
INSERT INTO [ODO].[Usuario] ([UsuarioId], [AplicacionId], [Email], [Nombre], [Descripcion], [Password], [PasswordFormat], [PasswordSalt], [PasswordPregunta], [PasswordRespuesta], [EstaAprobado], [EstaEliminado], [EstaBloqueado], [FechaCreacion], [FechaUltimoLogin], [FechaUltimoCambioPassword], [FechaUltimoBloqueo], [CantidadFallasIngresoPassword], [FechaPrimerIntentoFallidoIngresoPassword], [CantidadFallasIngresoRespuestaPassword], [FechaPrimerIntentoFallidoRespuestaPassword], [Comentario], [DebeCambiarPassword], [CodigoIdenficatorio], [IPAcceso], [InicioHorarioAcceso], [FinHorarioAcceso]) VALUES (N'42b69af9-8259-4102-99fc-9f8ac5d0133f', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'sindrunas@cardinalsystems.com.ar', N'sindrunas', N'Sebastian Indrunas', N'y4sadxCMd92J3Lk4u5AdhthIysaoOrD7nHJ93cUesgM=', 1, N'', N'', N'', 1, 0, 1, '20110210 09:40:26.487', '20110215 12:13:30.523', '20110210 09:40:26.487', '20110216 13:48:33.823', 0, NULL, NULL, NULL, N'', 0, NULL, NULL, NULL, NULL)
INSERT INTO [ODO].[Usuario] ([UsuarioId], [AplicacionId], [Email], [Nombre], [Descripcion], [Password], [PasswordFormat], [PasswordSalt], [PasswordPregunta], [PasswordRespuesta], [EstaAprobado], [EstaEliminado], [EstaBloqueado], [FechaCreacion], [FechaUltimoLogin], [FechaUltimoCambioPassword], [FechaUltimoBloqueo], [CantidadFallasIngresoPassword], [FechaPrimerIntentoFallidoIngresoPassword], [CantidadFallasIngresoRespuestaPassword], [FechaPrimerIntentoFallidoRespuestaPassword], [Comentario], [DebeCambiarPassword], [CodigoIdenficatorio], [IPAcceso], [InicioHorarioAcceso], [FinHorarioAcceso]) VALUES (N'5ba43ac1-f867-496a-8b9d-85e4770e1004', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'lnale@cardinalsystems.com.ar', N'lnale', N'Luis Nale', N'aezX2WFUHT07GfiOzbF/0/au0ZOXQXDdwVngpfG7ycE=', 1, N'', N'', N'', 1, 0, 1, '20110202 20:53:09.280', '20110207 09:48:23.403', '20110202 20:53:09.280', '20110216 13:41:37.237', 1, '20110210 09:39:00.017', NULL, NULL, N'', 1, NULL, NULL, NULL, NULL)
INSERT INTO [ODO].[Usuario] ([UsuarioId], [AplicacionId], [Email], [Nombre], [Descripcion], [Password], [PasswordFormat], [PasswordSalt], [PasswordPregunta], [PasswordRespuesta], [EstaAprobado], [EstaEliminado], [EstaBloqueado], [FechaCreacion], [FechaUltimoLogin], [FechaUltimoCambioPassword], [FechaUltimoBloqueo], [CantidadFallasIngresoPassword], [FechaPrimerIntentoFallidoIngresoPassword], [CantidadFallasIngresoRespuestaPassword], [FechaPrimerIntentoFallidoRespuestaPassword], [Comentario], [DebeCambiarPassword], [CodigoIdenficatorio], [IPAcceso], [InicioHorarioAcceso], [FinHorarioAcceso]) VALUES (N'99064d8b-7009-40a5-b776-1dbfc7eceb94', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'admin@odo.com', N'ADMIN', N'Usuario Administrador por defecto de ODO', N'F5+D4CCGe6l1FWOkwv2BOhnKRSHgoYl1OqMtkAVyYFA=', 1, N'', NULL, NULL, 1, 0, 0, '20080212 13:15:27.023', '20110215 12:24:41.063', '20110216 13:36:37.030', '20110202 18:59:08.720', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT INTO [ODO].[Usuario] ([UsuarioId], [AplicacionId], [Email], [Nombre], [Descripcion], [Password], [PasswordFormat], [PasswordSalt], [PasswordPregunta], [PasswordRespuesta], [EstaAprobado], [EstaEliminado], [EstaBloqueado], [FechaCreacion], [FechaUltimoLogin], [FechaUltimoCambioPassword], [FechaUltimoBloqueo], [CantidadFallasIngresoPassword], [FechaPrimerIntentoFallidoIngresoPassword], [CantidadFallasIngresoRespuestaPassword], [FechaPrimerIntentoFallidoRespuestaPassword], [Comentario], [DebeCambiarPassword], [CodigoIdenficatorio], [IPAcceso], [InicioHorarioAcceso], [FinHorarioAcceso]) VALUES (N'fdf6a4e8-030c-4c92-ae7d-ab764a96b864', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'jonatan.trevisan@addoc.com.ar', N'jtrevisan', N'Jonatan Trevisan', N'myfCBG6Nz1j8YXYWCoYtexpf2PfUFdvG5y2kByR74Kk=', 1, N'', N'desarrollo', N'', 1, 0, 0, '20110201 18:39:34.750', '20110216 13:22:32.440', '20110201 18:39:34.750', NULL, 0, NULL, NULL, NULL, N'', 1, NULL, NULL, NULL, NULL)
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'42b69af9-8259-4102-99fc-9f8ac5d0133f', N'4a9667af-9c09-4d54-a3d9-f1feef295731')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'42b69af9-8259-4102-99fc-9f8ac5d0133f', N'4a9667af-9c09-4d54-a3d9-f1feef295732')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'42b69af9-8259-4102-99fc-9f8ac5d0133f', N'4a9667af-9c09-4d54-a3d9-f1feef295733')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'42b69af9-8259-4102-99fc-9f8ac5d0133f', N'4a9667af-9c09-4d54-a3d9-f1feef295734')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'42b69af9-8259-4102-99fc-9f8ac5d0133f', N'4a9667af-9c09-4d54-a3d9-f1feef295735')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'42b69af9-8259-4102-99fc-9f8ac5d0133f', N'4a9667af-9c09-4d54-a3d9-f1feef295736')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'42b69af9-8259-4102-99fc-9f8ac5d0133f', N'4a9667af-9c09-4d54-a3d9-f1feef295737')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'42b69af9-8259-4102-99fc-9f8ac5d0133f', N'4a9667af-9c09-4d54-a3d9-f1feef295738')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'42b69af9-8259-4102-99fc-9f8ac5d0133f', N'63e42ccb-4c32-40eb-a8e0-d9e38df94345')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'42b69af9-8259-4102-99fc-9f8ac5d0133f', N'8081e7e8-d29f-46a8-9555-b486bfde2308')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'5ba43ac1-f867-496a-8b9d-85e4770e1004', N'4a9667af-9c09-4d54-a3d9-f1feef295731')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'5ba43ac1-f867-496a-8b9d-85e4770e1004', N'4a9667af-9c09-4d54-a3d9-f1feef295732')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'5ba43ac1-f867-496a-8b9d-85e4770e1004', N'4a9667af-9c09-4d54-a3d9-f1feef295733')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'5ba43ac1-f867-496a-8b9d-85e4770e1004', N'4a9667af-9c09-4d54-a3d9-f1feef295734')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'5ba43ac1-f867-496a-8b9d-85e4770e1004', N'4a9667af-9c09-4d54-a3d9-f1feef295735')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'5ba43ac1-f867-496a-8b9d-85e4770e1004', N'4a9667af-9c09-4d54-a3d9-f1feef295736')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'5ba43ac1-f867-496a-8b9d-85e4770e1004', N'4a9667af-9c09-4d54-a3d9-f1feef295737')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'5ba43ac1-f867-496a-8b9d-85e4770e1004', N'4a9667af-9c09-4d54-a3d9-f1feef295738')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'5ba43ac1-f867-496a-8b9d-85e4770e1004', N'63e42ccb-4c32-40eb-a8e0-d9e38df94345')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'5ba43ac1-f867-496a-8b9d-85e4770e1004', N'8081e7e8-d29f-46a8-9555-b486bfde2308')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'fdf6a4e8-030c-4c92-ae7d-ab764a96b864', N'4a9667af-9c09-4d54-a3d9-f1feef295731')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'fdf6a4e8-030c-4c92-ae7d-ab764a96b864', N'4a9667af-9c09-4d54-a3d9-f1feef295732')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'fdf6a4e8-030c-4c92-ae7d-ab764a96b864', N'4a9667af-9c09-4d54-a3d9-f1feef295733')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'fdf6a4e8-030c-4c92-ae7d-ab764a96b864', N'4a9667af-9c09-4d54-a3d9-f1feef295734')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'fdf6a4e8-030c-4c92-ae7d-ab764a96b864', N'4a9667af-9c09-4d54-a3d9-f1feef295735')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'fdf6a4e8-030c-4c92-ae7d-ab764a96b864', N'4a9667af-9c09-4d54-a3d9-f1feef295736')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'fdf6a4e8-030c-4c92-ae7d-ab764a96b864', N'4a9667af-9c09-4d54-a3d9-f1feef295737')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'fdf6a4e8-030c-4c92-ae7d-ab764a96b864', N'4a9667af-9c09-4d54-a3d9-f1feef295738')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'fdf6a4e8-030c-4c92-ae7d-ab764a96b864', N'63e42ccb-4c32-40eb-a8e0-d9e38df94345')
INSERT INTO [ODO].[UsuarioGrupo] ([UsuarioId], [GrupoId]) VALUES (N'fdf6a4e8-030c-4c92-ae7d-ab764a96b864', N'8081e7e8-d29f-46a8-9555-b486bfde2308')
INSERT INTO [ODO].[TipoObjeto] ([TipoObjetoId], [AplicacionId], [Descripcion]) VALUES (N'486ea9bc-4745-4776-8949-7c51d37cc92a', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'Aplicaciones')
INSERT INTO [ODO].[TipoObjeto] ([TipoObjetoId], [AplicacionId], [Descripcion]) VALUES (N'65cc8c32-396f-4412-b96c-9bf52b99df45', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'VARIOS')
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'1661e7db-2197-4eda-9c7a-a7d68a62d052', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ELIMINAR OBJETO', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'1a4f94a4-93fd-4807-922c-e41d3eb391b1', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ELIMINAR USUARIO', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'1a4f94a4-93fd-4807-922c-e41d3eb391b2', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'MODIFICAR PASSWORD USUARIO', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'1a4f94a4-93fd-4807-922c-e41d3eb391b5', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'MODIFICAR USUARIO', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'1a4f94a4-93fd-4807-922c-e41d3eb391b6', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'AGREGAR USUARIO', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'1a4f94a4-93fd-4807-922c-e41d3eb391b7', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'LISTA USUARIOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'1a4f94a4-93fd-4807-922c-e41d3eb391b8', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'AGREGAR APLICACIONES', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'1a4f94a4-93fd-4807-922c-e41d3eb391b9', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'VER APLICACION', 1, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'2661e7db-2197-4eda-9c7a-a7d68a62d052', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ELIMINAR TIPO OBJETO', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'3661e7db-2197-4eda-9c7a-a7d68a62d050', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ELIMINAR TAREAS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'3661e7db-2197-4eda-9c7a-a7d68a62d051', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'LISTAR TAREAS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'3661e7db-2197-4eda-9c7a-a7d68a62d052', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ELIMINAR GRUPOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'3661e7db-2197-4eda-9c7a-a7d68a62d053', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'AGREGAR TAREAS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'3661e7db-2197-4eda-9c7a-a7d68a62d054', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'MODIFICAR TAREAS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'752a348d-1966-48a6-a952-fc25d77fd30d', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'VER AUDITORIA', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'752a348d-1966-48a6-a952-fc25d77fd31d', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'VER REPORTES', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'7a1ba645-4ade-4f58-b63f-52205c54c631', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'LOGIN', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'96219203-d6bd-4a66-968d-226b46c09b11', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'AGREGAR OBJETOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'96219203-d6bd-4a66-968d-226b46c09b13', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'LISTAR OBJETOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'96219203-d6bd-4a66-968d-226b46c09b15', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'MODIFICAR TIPOS OBJETOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'96219203-d6bd-4a66-968d-226b46c09b16', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'AGREGAR TIPOS OBJETOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'96219203-d6bd-4a66-968d-226b46c09b17', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'LISTA TIPOS OBJETOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'96219203-d6bd-4a66-968d-226b46c09b27', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'MODIFICAR OBJETOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'cdb5be22-1824-4da2-8822-59377707dcc8', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'MODIFICAR APLICACION', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'd68d1543-2034-4364-ab39-1e95b16e64f6', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'LISTAR APLICACIONES', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'd74d3509-eb77-406d-87fd-44ec81465881', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ASIGNACION PERMISOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'da574227-a901-4bef-ae46-1d18eaad1b1d', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'AGREGAR GRUPOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'da574227-a901-4bef-ae46-1d18eaad1b2d', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'MODIFICAR GRUPOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Tarea] ([TareaId], [AplicacionId], [Nombre], [UsaObjecto], [UltimoNumero], [UsaObjectoExterno], [EstaEliminada]) VALUES (N'da574227-a901-4bef-ae46-1d18eaad1b6d', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'LISTA GRUPOS', 0, 0, 0, 0)
INSERT INTO [ODO].[Grupo] ([GrupoId], [AplicacionId], [Nombre], [Descripcion]) VALUES (N'4a9667af-9c09-4d54-a3d9-f1feef295731', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ADMIN_APLICACION', N'Administrador de Aplicaciones')
INSERT INTO [ODO].[Grupo] ([GrupoId], [AplicacionId], [Nombre], [Descripcion]) VALUES (N'4a9667af-9c09-4d54-a3d9-f1feef295732', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ADMIN_USUARIO', N'Administrador de Usuarios')
INSERT INTO [ODO].[Grupo] ([GrupoId], [AplicacionId], [Nombre], [Descripcion]) VALUES (N'4a9667af-9c09-4d54-a3d9-f1feef295733', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ADMIN_GRUPO', N'Administrador de Grupos')
INSERT INTO [ODO].[Grupo] ([GrupoId], [AplicacionId], [Nombre], [Descripcion]) VALUES (N'4a9667af-9c09-4d54-a3d9-f1feef295734', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ADMIN_OBJETO', N'Administrador de Objetos')
INSERT INTO [ODO].[Grupo] ([GrupoId], [AplicacionId], [Nombre], [Descripcion]) VALUES (N'4a9667af-9c09-4d54-a3d9-f1feef295735', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ADMIN_TAREA', N'Administrador de Tareas')
INSERT INTO [ODO].[Grupo] ([GrupoId], [AplicacionId], [Nombre], [Descripcion]) VALUES (N'4a9667af-9c09-4d54-a3d9-f1feef295736', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ADMIN_PERMISO', N'Administrador de Permisos')
INSERT INTO [ODO].[Grupo] ([GrupoId], [AplicacionId], [Nombre], [Descripcion]) VALUES (N'4a9667af-9c09-4d54-a3d9-f1feef295737', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ADMIN_AUDITORIA', N'Administrador de Auditoria')
INSERT INTO [ODO].[Grupo] ([GrupoId], [AplicacionId], [Nombre], [Descripcion]) VALUES (N'4a9667af-9c09-4d54-a3d9-f1feef295738', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ADMIN_REPORTES', N'Administrador de Reportes')
INSERT INTO [ODO].[Grupo] ([GrupoId], [AplicacionId], [Nombre], [Descripcion]) VALUES (N'63e42ccb-4c32-40eb-a8e0-d9e38df94345', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'USUARIO', N'Usuario del Sistema')
INSERT INTO [ODO].[Grupo] ([GrupoId], [AplicacionId], [Nombre], [Descripcion]) VALUES (N'8081e7e8-d29f-46a8-9555-b486bfde2308', N'7a1ba645-4ade-4f58-b63f-52205c54c634', N'ADMIN', N'Administrador del Sistema')
ALTER TABLE [ODO].[LoginToken] ADD CONSTRAINT [FK_LoginToken_Usuario] FOREIGN KEY ([UsuarioId]) REFERENCES [ODO].[Usuario] ([UsuarioId])
ALTER TABLE [ODO].[PermisosUsuario] ADD CONSTRAINT [FK_PermisosUsuario_Objecto] FOREIGN KEY ([ObjetoId]) REFERENCES [ODO].[Objeto] ([ObjetoId])
ALTER TABLE [ODO].[PermisosUsuario] ADD CONSTRAINT [FK_PermisosUsuario_Tarea] FOREIGN KEY ([TareaId]) REFERENCES [ODO].[Tarea] ([TareaId])
ALTER TABLE [ODO].[PermisosUsuario] ADD CONSTRAINT [FK_PermisosUsuario_Usuario] FOREIGN KEY ([UsuarioId]) REFERENCES [ODO].[Usuario] ([UsuarioId])
ALTER TABLE [ODO].[Grupo] ADD CONSTRAINT [FK_Grupo_Aplicacion] FOREIGN KEY ([AplicacionId]) REFERENCES [ODO].[Aplicacion] ([AplicacionId])
ALTER TABLE [ODO].[Tarea] ADD CONSTRAINT [FK_Tarea_Aplicacion] FOREIGN KEY ([AplicacionId]) REFERENCES [ODO].[Aplicacion] ([AplicacionId])
ALTER TABLE [ODO].[TipoObjeto] ADD CONSTRAINT [FK_TipoObjeto_Aplicacion] FOREIGN KEY ([AplicacionId]) REFERENCES [ODO].[Aplicacion] ([AplicacionId])
ALTER TABLE [ODO].[UsuarioGrupo] ADD CONSTRAINT [FK_UsuarioGrupo_Grupo] FOREIGN KEY ([GrupoId]) REFERENCES [ODO].[Grupo] ([GrupoId])
ALTER TABLE [ODO].[UsuarioGrupo] ADD CONSTRAINT [FK_UsuarioGrupo_Usuario] FOREIGN KEY ([UsuarioId]) REFERENCES [ODO].[Usuario] ([UsuarioId])
ALTER TABLE [ODO].[Usuario] ADD CONSTRAINT [FK_Usuario_Aplicacion] FOREIGN KEY ([AplicacionId]) REFERENCES [ODO].[Aplicacion] ([AplicacionId])
ALTER TABLE [ODO].[PermisosGrupo] ADD CONSTRAINT [FK_PermisosGrupo_Grupo] FOREIGN KEY ([GrupoId]) REFERENCES [ODO].[Grupo] ([GrupoId])
ALTER TABLE [ODO].[PermisosGrupo] ADD CONSTRAINT [FK_PermisosGrupo_Objecto] FOREIGN KEY ([ObjetoId]) REFERENCES [ODO].[Objeto] ([ObjetoId])
ALTER TABLE [ODO].[PermisosGrupo] ADD CONSTRAINT [FK_PermisosGrupo_Tarea] FOREIGN KEY ([TareaId]) REFERENCES [ODO].[Tarea] ([TareaId])
ALTER TABLE [ODO].[Objeto] ADD CONSTRAINT [FK_Objecto_Aplicacion] FOREIGN KEY ([AplicacionId]) REFERENCES [ODO].[Aplicacion] ([AplicacionId])
ALTER TABLE [ODO].[Objeto] ADD CONSTRAINT [FK_Objeto_TipoObjeto] FOREIGN KEY ([TipoObjetoId]) REFERENCES [ODO].[TipoObjeto] ([TipoObjetoId])
ALTER TABLE [ODO].[Auditoria] ADD CONSTRAINT [FK_Auditoria_Aplicacion] FOREIGN KEY ([AplicacionId]) REFERENCES [ODO].[Aplicacion] ([AplicacionId])
ALTER TABLE [ODO].[Auditoria] ADD CONSTRAINT [FK_Auditoria_Tarea] FOREIGN KEY ([TareaId]) REFERENCES [ODO].[Tarea] ([TareaId])
ALTER TABLE [ODO].[Auditoria] ADD CONSTRAINT [FK_Auditoria_Usuario1] FOREIGN KEY ([UsuarioId]) REFERENCES [ODO].[Usuario] ([UsuarioId])
COMMIT TRANSACTION
