/*
   Tuesday, June 02, 20099:42:07 AM
   User: sa
   Server: 192.168.16.2
   Database: EasyDocNet
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Empresas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Empresas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Empresas', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.OperacionEntregas ADD CONSTRAINT
	FK_OperacionEntregas_Empresas FOREIGN KEY
	(
	LoginEmpresa
	) REFERENCES dbo.Empresas
	(
	LoginEmpresa
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.OperacionEntregas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.OperacionEntregas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.OperacionEntregas', 'Object', 'CONTROL') as Contr_Per 