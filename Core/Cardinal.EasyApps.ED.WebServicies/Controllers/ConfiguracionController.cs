﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcMiniProfiler;

namespace Cardinal.EasyApps.ED.WebServicies.Controllers
{
    public class ConfiguracionController : Controller
    {
        private readonly Cardinal.EasyApps.ED.Common.Dal.IRepository _repository;

        public ConfiguracionController(Cardinal.EasyApps.ED.Common.Dal.IRepository repository)
        {
            _repository = repository;
        }

        [HttpPost]
        public ActionResult GetEmpresas(string logintoken)
        {
            //Valido el login Token
            if (!_repository.IsLoginTokenValid(logintoken))
            {
                return new JsonResult
                {
                    Data = "TOKEN INVALID"
                };
            }

            return new JsonResult
            {
                Data = _repository.GetEmpresas(logintoken)
            };
        }

        [HttpPost]
        public ActionResult GetPlantillas(string logintoken)
        { //Valido el login Token
            if (!_repository.IsLoginTokenValid(logintoken))
            {
                return new JsonResult
                {
                    Data = "TOKEN INVALID"
                };
            }

            return new JsonResult
            {
                Data = _repository.GetPlantillasWithLoginToken(logintoken)
            };
        }

        [HttpPost]
        public ActionResult GetPlantillasPorEmpresa(string logintoken, string codigoEmpresa)
        {
            //Valido el login Token
            if (!_repository.IsLoginTokenValid(logintoken))
            {
                return new JsonResult
                {
                    Data = "TOKEN INVALID"
                };
            }
            return new JsonResult
            {
                Data = _repository.GetPlantillas(logintoken,codigoEmpresa)
            };
        }

        [HttpPost]
        public ActionResult GetCamposPorPlantilla(string logintoken, string codigoPlantilla)
        {
            //Valido el login Token
            if (!_repository.IsLoginTokenValid(logintoken))
            {
                return new JsonResult
                {
                    Data = "TOKEN INVALID"
                };
            }
            return new JsonResult
            {
                Data = _repository.GetPlantillaCampos(codigoPlantilla)
            };
        }

        [HttpPost]
        public ActionResult GetTablasExternas(string logintoken)
        {
            var profiler = MiniProfiler.Current; // it's ok if this is null
            using (profiler.Step("Get Tablas Externas"))
            {
                //Valido el login Token
                if (!_repository.IsLoginTokenValid(logintoken))
                {
                    return new JsonResult
                    {
                        Data = "TOKEN INVALID"
                    };
                }
                return new JsonResult
                {

                    Data = _repository.GetTablasExternas()
                };
            }
        }

        //[HttpPost]
        //public ActionResult AddValorTablasExternas(string logintoken,string key string value)
        //{
        //    var profiler = MiniProfiler.Current; // it's ok if this is null
        //    using (profiler.Step("Add Valor Tablas Externas"))
        //    {
        //        //Valido el login Token
        //        if (!_repository.IsLoginTokenValid(logintoken))
        //        {
        //            return new JsonResult
        //            {
        //                Data = "TOKEN INVALID"
        //            };
        //        }
        //       //Valido si existe el key
        //    }
        //}

        [HttpPost]
        public ActionResult AgregarValorTablasExternas(string logintoken, string tablaExterna, string campoId, string campoExterno, 
            string key, string value)
        {
            //Valido el login Token
            if (!_repository.IsLoginTokenValid(logintoken))
            {
                return new JsonResult
                {
                    Data = "TOKEN INVALID"
                };
            }

            Cardinal.EasyApps.ED.Model.ResultadoOperacion resultado = new Cardinal.EasyApps.ED.Model.ResultadoOperacion();
            if (!_repository.ExisteKeyValorTablaExterna(tablaExterna, campoId, campoExterno, key))
            {
                if (_repository.AgregarValorTablaExterna(tablaExterna, campoId, campoExterno, key, value))
                {
                    resultado.Result = "TRUE";
                }
                else
                {
                    resultado.Result = "FALSE";
                }
            }
            else
            {
                resultado.Result = "FALSE";
            }
            return new JsonResult
            {
                Data = resultado
            };
        }

        [HttpPost]
        public ActionResult EditarValorTablasExternas(string logintoken, string tablaExterna, string campoId, string campoExterno,
            string key, string newValue)
        {
            //Valido el login Token
            if (!_repository.IsLoginTokenValid(logintoken))
            {
                return new JsonResult
                {
                    Data = "TOKEN INVALID"
                };
            }

            Cardinal.EasyApps.ED.Model.ResultadoOperacion resultado = new Cardinal.EasyApps.ED.Model.ResultadoOperacion();
            if (_repository.ExisteKeyValorTablaExterna(tablaExterna, campoId, campoExterno, key))
            {
                if (_repository.EditarValorTablaExterna(tablaExterna, campoId, campoExterno, key,newValue))
                {
                    resultado.Result = "TRUE";
                }
                else
                {
                    resultado.Result = "FALSE";
                }
            }
            else
            {
                resultado.Result = "FALSE";
            }
            return new JsonResult
            {
                Data = resultado
            };
        }


        [HttpPost]
        public ActionResult ExisteKeyValorTablaExterna(string logintoken, string tablaExterna, string campoId, string campoExterno,
            string key)
        {
            //Valido el login Token
            if (!_repository.IsLoginTokenValid(logintoken))
            {
                return new JsonResult
                {
                    Data = "TOKEN INVALID"
                };
            }

            Cardinal.EasyApps.ED.Model.ResultadoOperacion resultado = new Cardinal.EasyApps.ED.Model.ResultadoOperacion();
       
            if (_repository.ExisteKeyValorTablaExterna(tablaExterna, campoId, campoExterno, key))
            {
                resultado.Result = "TRUE";
            }
            else
            {
                resultado.Result = "FALSE";
            }
            return new JsonResult
            {
                Data = resultado
            };
        }
    }
}
