[assembly: WebActivator.PreApplicationStartMethod(typeof(Cardinal.EasyApps.ED.WebServicies.App_Start.NinjectMVC3), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(Cardinal.EasyApps.ED.WebServicies.App_Start.NinjectMVC3), "Stop")]

namespace Cardinal.EasyApps.ED.WebServicies.App_Start
{
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
	using Ninject.Web.Mvc;
    using Ninject;
    using Ninject.Modules;
    using System.Configuration;

    public static class NinjectMVC3 
	{
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
		{
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestModule));
			bootstrapper.Initialize(CreateKernel);
        }
		
        /// <summary>
        /// Stops the application.
        /// </summary>
		public static void Stop()
		{
			bootstrapper.ShutDown();
		}
		
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<Cardinal.EasyApps.ED.Common.Dal.IRepository>()

                   .To<Cardinal.EasyApps.ED.Dal.EntityFramework.Repository>().InRequestScope()
                   .WithConstructorArgument("connectionString", ConfigurationManager.ConnectionStrings["EDEntities"].ConnectionString
                   );
           
        }		
    }
}
