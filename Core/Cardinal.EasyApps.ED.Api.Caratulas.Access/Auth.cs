﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cardinal.EasyApps.ED.Model;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NLog;

namespace Cardinal.EasyApps.ED.Api.Caratulas.Access
{
    public class Auth : AccessComon
    {
     
        public LoginAuth LoginToken { get; set; }
        Logger logger = LogManager.GetCurrentClassLogger();

        public Auth(string apiUrl, string apiVersion) :base(apiUrl,apiVersion)
        {
        }

        public string GetLoginToken(string username, string password)
        {
            try
            {
                Uri address = new Uri(string.Format("{0}/Auth/Login",ApiUrlFull));

                string data = string.Format("username={0}&password={1}", username, password);

                HttpWebRequest request = ResquestPost(address, data);

                logger.Debug(String.Format("Inbound Url Api: {0}?{1}", request.Address.AbsoluteUri, data));
                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);
                LoginAuth token = new LoginAuth();
                token = JsonConvert.DeserializeObject<LoginAuth>(jsonResponse);
                LoginToken = token;
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                LoginToken = new LoginAuth()
                {
                    Result = "FAIL",
                    //Token = string.Empty
                    Token = e.Message
                };
            }
            return LoginToken.Token;
        }

        public string GetLoginToken(string username, string password, bool ldap)
        {
            try
            {
                Uri address = new Uri(string.Format("{0}/Auth/Login", ApiUrlFull));

                string data = string.Format("username={0}&password={1}&ldap={2}", username, password, ldap);

                HttpWebRequest request = ResquestPost(address, data);

                logger.Debug(String.Format("Inbound Url Api: {0}?{1}", request.Address.AbsoluteUri, data));
                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);
                LoginAuth token = new LoginAuth();
                token = JsonConvert.DeserializeObject<LoginAuth>(jsonResponse);
                LoginToken = token;
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                LoginToken = new LoginAuth()
                {
                    Result = "FAIL",
                    //Token = string.Empty
                    Token = e.Message
                };
            }
            return LoginToken.Token;
        }

    }
}