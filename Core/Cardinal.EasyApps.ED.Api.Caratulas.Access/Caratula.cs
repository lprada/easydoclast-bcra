﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cardinal.EasyApps.ED.Model;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NLog;


namespace Cardinal.EasyApps.ED.Api.Caratulas.Access
{
    public class Caratula :AccessComon
    {
        private LoginAuth _loginToken { get; set; }
        Logger logger = LogManager.GetCurrentClassLogger();


        public Caratula(string apiUrl, string apiVersion, LoginAuth loginToken)
            : base(apiUrl, apiVersion)
        {
            _loginToken = loginToken;
        }

        public IQueryable<CaratulaCS> GetCaratulas()
        {
            try
            {
                Uri address = new Uri(string.Format("{0}/Caratulas/Caratulas", ApiUrlFull));

                string data = string.Format("loginToken={0}", _loginToken.Token);

             
                HttpWebRequest request = ResquestPost(address, data);
                logger.Debug(String.Format("Url Api: {0}?{1}", request.Address.AbsoluteUri,data));

                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);
              
                var res = JsonConvert.DeserializeObject<List<CaratulaCS>>(jsonResponse);
                return res.AsQueryable();
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message,e.StackTrace));
                throw;
            }
        }

        public CaratulaCS GetCaratula(string numeroCaratula)
        {
            try
            {
                Uri address = new Uri(string.Format("{0}/Caratulas/Caratula", ApiUrlFull));


                string data = string.Format("loginToken={0}&numeroCaratula={1}", _loginToken.Token, numeroCaratula);

                HttpWebRequest request = ResquestPost(address, data);

                logger.Debug(String.Format("Url Api: {0}?{1}", request.Address.AbsoluteUri, data));

                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);

                var res = JsonConvert.DeserializeObject<CaratulaCS>(jsonResponse);
                return res;
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                return null;
            }
        }

        public IQueryable<CaratulaCS> GetInbound(int dias)
        {
            try
            {
                Uri address = new Uri(string.Format("{0}/Caratulas/Inbound", ApiUrlFull));

                string data = string.Format("loginToken={0}&dias={1}", _loginToken.Token,dias);


                HttpWebRequest request = ResquestPost(address, data);
                logger.Debug(String.Format("Url Api: {0}?{1}", request.Address.AbsoluteUri, data));

                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);

                var res = JsonConvert.DeserializeObject<List<CaratulaCS>>(jsonResponse);
                return res.AsQueryable();
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                throw;
            }
        }

        public CaratulaCS RealizarInbound(string numeroCaratula)
        {
            try
            {
                Uri address = new Uri(string.Format("{0}/Caratulas/RealizarInbound", ApiUrlFull));

                string data = string.Format("loginToken={0}&numeroCaratula={1}", _loginToken.Token, numeroCaratula);


                HttpWebRequest request = ResquestPost(address, data);
                logger.Debug(String.Format("Url Api: {0}?{1}", request.Address.AbsoluteUri, data));

                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);

                return GetCaratula(numeroCaratula);
                
               
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                throw;
            }
        }

        public IQueryable<CaratulaCS> GetOutbound(int dias)
        {
            try
            {
                Uri address = new Uri(string.Format("{0}/Caratulas/OutBound", ApiUrlFull));

                string data = string.Format("loginToken={0}&dias={1}", _loginToken.Token, dias);


                HttpWebRequest request = ResquestPost(address, data);
                logger.Debug(String.Format("Url Api: {0}?{1}", request.Address.AbsoluteUri, data));

                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);

                var res = JsonConvert.DeserializeObject<List<CaratulaCS>>(jsonResponse);
                return res.AsQueryable();
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                throw;
            }
        }

        public List<CaratulaCS> GetCaratulasLotes(string codigoLote)
        {
            try
            {
                //Debe ser un llamado local

                Uri address = new Uri(string.Format("{0}/Caratulas/CaratulaPorLote", ApiUrlFull));


                string data = string.Format("loginToken={0}&codigoLote={1}", _loginToken.Token, codigoLote);



                HttpWebRequest request = ResquestPost(address, data);
                logger.Debug(String.Format("Url Api: {0}?{1}", request.Address.AbsoluteUri, data));

                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);

                var res = JsonConvert.DeserializeObject<List<CaratulaCS>>(jsonResponse);
                return res;
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                throw;
            }
        }

        //public List<CaratulaCS> RealizarOutboundLote(string codigoLote)
        //{
        //    List<CaratulaCS> res = new System.Collections.Generic.List<CaratulaCS>();
        //    try
        //    {
        //        var listaCaratulas = GetCaratulasLotes(codigoLote);
        //        foreach (CaratulaCS caratula in listaCaratulas)
        //        {
        //            res.Add(RealizarOutbound(caratula));
        //        }
               
        //    }
        //    catch 
        //    {

        //        return res = new List<CaratulaCS>();
        //    }
        //    return res;
        //}


        public CaratulaCS RealizarOutbound(CaratulaCS caratula)
        {
            try
            {
               return RealizarOutbound(caratula.codigo.ToString());
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                throw;
            }
        }
        public CaratulaCS RealizarOutbound(string numeroCaratula)
        {
            try
            {
                Uri address = new Uri(string.Format("{0}/Caratulas/RealizarOutbound", ApiUrlFull));

                string data = string.Format("loginToken={0}&numeroCaratula={1}", _loginToken.Token, numeroCaratula);


                HttpWebRequest request = ResquestPost(address, data);
                logger.Debug(String.Format("Url Api: {0}?{1}", request.Address.AbsoluteUri, data));

                string jsonResponse = "";

                jsonResponse = GetJsonResponse(request, jsonResponse);

                return GetCaratula(numeroCaratula);


            }
            catch (Exception e)
            {
                logger.Error(String.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace));
                throw;
            }
        }

    }
}
