﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace Cardinal.EasyApps.ED.Api.Caratulas.Access
{
    public abstract class AccessComon
    {
        /// <summary>
        /// Api Url
        /// </summary>
        string _apiUrl;
        /// <summary>
        /// Api Version
        /// </summary>
        string _apiVersion;

       public string ApiUrlFull
        {
            get
            {
                return string.Format("{0}/Api/{1}", _apiUrl, _apiVersion);
            }
        }

        public AccessComon(string apiUrl, string apiVersion)
        {
            _apiUrl = apiUrl;
            _apiVersion = apiVersion;
        }

      

        protected string GetJsonResponse(HttpWebRequest request, string jsonResponse)
        {
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                //throw new Exception(response.ToString());
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();
                reader.Close();
            }
            return jsonResponse;
        }


        protected HttpWebRequest ResquestPost(Uri address, string data)
        {
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;

            //var proxy = WebRequest.GetSystemWebProxy();
            //proxy.Credentials = CredentialCache.DefaultNetworkCredentials;

            //request.Proxy = proxy;

            request.Method = "POST";
            request.UseDefaultCredentials = true;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

            request.ContentLength = byteData.Length;

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            return request;
        }
    }
}
