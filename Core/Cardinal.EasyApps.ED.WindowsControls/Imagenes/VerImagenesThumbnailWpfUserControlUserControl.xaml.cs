﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cardinal.EasyApps.ED.WindowsControls.Imagenes
{
    /// <summary>
    /// Interaction logic for VerImagenesThumbnailWpfUserControlUserControl.xaml
    /// </summary>
    public partial class VerImagenesThumbnailWpfUserControlUserControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VerImagenesThumbnailWpfUserControlUserControl"/> class.
        /// </summary>
        public VerImagenesThumbnailWpfUserControlUserControl()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Clears the thumbnails.
        /// </summary>
        public void ClearThumbnails()
        {
            this.verImagenesThumbnailUserControl.ClearThumbnails();
        }
        /// <summary>
        /// Addtoes the thumbnail.
        /// </summary>
        /// <param name="thumbName">Name of the thumb.</param>
        public void AddToThumbnail(string thumbName)
        {
            this.verImagenesThumbnailUserControl.AddToThumbnail(thumbName);
        }
        /// <summary>
        /// Addtoes the thumbnails.
        /// </summary>
        /// <param name="thumbs">The thumbs.</param>
        public void AddToThumbnails(IList<string> thumbs)
        {
            this.verImagenesThumbnailUserControl.AddToThumbnails(thumbs);
        }

        /// <summary>
        /// Refreshes the label cantidad imagenes cargadas.
        /// </summary>
        public void RefreshLabelCantidadImagenesCargadas()
        {
            this.verImagenesThumbnailUserControl.RefreshLabelCantidadImagenesCargadas();
        }
    }
}
