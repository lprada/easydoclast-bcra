﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace Cardinal.EasyApps.ED.WindowsControls.Imagenes
{
    /// <summary>
    /// 
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Ver"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Imagenes")]
    public partial class VerImagenesThumbnailUserControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VerImagenesThumbnailUserControl"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "UnlockRuntime"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public VerImagenesThumbnailUserControl()
        {
            InitializeComponent();
            try
            {
                AccusoftUnlocks.UnlockRuntimes(thumbnailXpress1);
            }
            catch
            {
                MessageBox.Show("Invalid Unlock Codes, See the UnlockRuntime calls");
            }
        }

        /// <summary>
        /// Clears the thumbnails.
        /// </summary>
        public void ClearThumbnails()
        {
            thumbnailXpress1.Items.Clear();
            RefreshLabelCantidadImagenesCargadas();
        }
        /// <summary>
        /// Addtoes the thumbnail.
        /// </summary>
        /// <param name="thumbName">Name of the thumb.</param>
        public void AddToThumbnail(string thumbName)
        {
            try
            {
                if (System.IO.Path.GetExtension(thumbName).ToUpper() != ".DB")
                {
                    thumbnailXpress1.Items.AddItemsFromFile(thumbName, -1, true);
                }
            }
            finally
            {
            }
        }
        /// <summary>
        /// Addtoes the thumbnails.
        /// </summary>
        /// <param name="thumbs">The thumbs.</param>
        public void AddToThumbnails(IList<string> thumbs)
        {
            if (thumbs!= null && thumbs.Count > 0)
            {
                MostrarMensajeCargandoImagenes();
                foreach (var thumb in thumbs)
                {

                  
                    AddToThumbnail(thumb);
                }
                thumbnailXpress1.SelectedItems.Clear();
                thumbnailXpress1.SelectedItems.Add(thumbnailXpress1.Items[0]);
                RefreshLabelCantidadImagenesCargadas();
            }
        }

        /// <summary>
        /// Refreshes the label cantidad imagenes cargadas.
        /// </summary>
        public void RefreshLabelCantidadImagenesCargadas()
        {
            label1.Text = string.Format(CultureInfo.InvariantCulture, "Cantidad de Imagenes Cargadas: {0}", thumbnailXpress1.Items.Count);
            Application.DoEvents();
        }

        private void MostrarMensajeCargandoImagenes()
        {
            label1.Text = string.Format(CultureInfo.InvariantCulture, "Cargando Imagenes. Por Favor Espere .....");
            Application.DoEvents();
        }

        /// <summary>
        /// Handles the KeyPress event of the thumbnailXpress1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.KeyPressEventArgs"/> instance containing the event data.</param>
        private void thumbnailXpress1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Calculo del 5%
            int porcHeight = this.thumbnailXpress1.CellHeight * 20 / 100;
            int porcWidth = this.thumbnailXpress1.CellWidth * 20 / 100;
            if (e.KeyChar == '+')
            {
                this.thumbnailXpress1.CellHeight += porcHeight;
                this.thumbnailXpress1.CellWidth += porcWidth;
            }
            else if (e.KeyChar == '-')
            {
                this.thumbnailXpress1.CellHeight -= porcHeight;
                this.thumbnailXpress1.CellWidth -= porcWidth;
            }
        }
    }
}
