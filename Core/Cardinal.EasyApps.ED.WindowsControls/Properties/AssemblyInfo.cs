﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Cardinal.EasyApps.ED.WindowsControls")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]


// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1fea1f35-8a8f-47a2-b93a-7888d6a38c3a")]

[assembly: NeutralResourcesLanguageAttribute("es-AR")]
