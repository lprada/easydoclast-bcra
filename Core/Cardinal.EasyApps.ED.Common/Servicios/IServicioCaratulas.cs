﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cardinal.EasyApps.ED.Model;

namespace Cardinal.EasyApps.ED.Common.Servicios
{
    /// <summary>
    /// Interface de Servicios de EasyDoc para Caratulas
    /// </summary>
    public interface IServicioCaratulas
    {

        /// <summary>
        /// Gets or sets the repository.
        /// </summary>
        /// <value>
        /// The repository.
        /// </value>
        Cardinal.EasyApps.ED.Common.Dal.IRepository Repository { get; set; }

        /// <summary>
        /// Adds the valor tabla externa.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        bool AddValorTablaExterna(LoginAuth token, string tablaExterna, string campoId, string campoExterno, string key, string value);

        /// <summary>
        /// Authenticates the JSON.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        LoginAuth AuthenticateJSON(string username, string password);

        /// <summary>
        /// Authenticates the JSON.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        LoginAuth AuthenticateJSONLDAP(string username, string password);
        

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        bool ChangePassword(LoginAuth token,string oldPassword, string newPassword);

        /// <summary>
        /// Edits the valor tabla externa.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="newValue">The new value.</param>
        /// <returns></returns>
        bool EditValorTablaExterna(LoginAuth token, string tablaExterna, string campoId, string campoExterno, string key, string newValue);

        /// <summary>
        /// Gets the campos by plantilla JSON.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="codPlantilla">The cod plantilla.</param>
        /// <returns></returns>
        List<PlantillaCampo> GetCamposByPlantillaJSON(LoginAuth token, string codPlantilla);

        /// <summary>
        /// Gets the empresas JSON.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        List<Cliente> GetEmpresasJSON(LoginAuth token);

        /// <summary>
        /// Gets the plantillas by empresa JSON.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="codEmpresa">The cod empresa.</param>
        /// <returns></returns>
        List<Plantilla> GetPlantillasByEmpresaJSON(LoginAuth token, string codEmpresa);

        /// <summary>
        /// Gets the tablas externas JSON.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        List<TablaExterna> GetTablasExternasJSON(LoginAuth token);

        /// <summary>
        /// Gets the campo descripcion.
        /// </summary>
        /// <param name="plantillaId">The plantilla id.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoValue">The campo value.</param>
        /// <returns></returns>
        string GetCampoDescripcion(string plantillaId, string campoId, string campoValue);
    }
}
