﻿using Cardinal.EasyApps.ED.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Cardinal.EasyApps.ED.Common.Dal;
using System.Globalization;
using System.Text;

namespace Cardinal.EasyApps.ED.Dal.EntityFramework
{
    public class Repository : IRepository
    {
        private string _connectionString = string.Empty;
        private EDEntities _ctx;

        public Repository(string connectionString)
        {
            _connectionString = connectionString;
            _ctx = new EDEntities(connectionString);
            _ctx.ContextOptions.LazyLoadingEnabled = true;
        }

        public IQueryable<Model.Lote> GetLotesPorEstado(Model.EstadoLote estado)
        {
            throw new System.NotImplementedException();
        }

        public string GetPathRedVolumen(string volumen)
        {
            var vol = _ctx.montajes.SingleOrDefault(p => p.volumen == volumen);

            if (vol != null)
            {
                if (!String.IsNullOrWhiteSpace(vol.mountAccesoRed))
                {
                    return vol.mountAccesoRed;
                }
                else
                {
                    return vol.mount;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetExtensionTipo(int tipo)
        {
            var tipoBD = _ctx.tipos.SingleOrDefault(p => p.tipo == tipo);

            return tipoBD.extension;
        }

        public IQueryable<Model.Imagen> GetImagenesLote(Model.Lote lote)
        {
            List<Model.Imagen> res = new List<Model.Imagen>();

            //string sql = String.Format("SELECT nro_orden,indice,volumen from {0}AB where cod_lote ='{1}'", lote.Plantilla.CodigoPlantilla, lote.CodigoLote);

            //ObtenerImagenesLote(res, sql);

            //sql = String.Format("SELECT nro_orden,indice,volumen from {0}AA where cod_lote ='{1}'", lote.Plantilla.CodigoPlantilla, lote.CodigoLote);

            ObtenerImagenesLote(res, lote.Plantilla.CodigoPlantilla + "AB", lote.CodigoLote);
            ObtenerImagenesLote(res, lote.Plantilla.CodigoPlantilla + "AA", lote.CodigoLote);

            var imgLotes = from p in _ctx.lotes
                           where p.cod_lote == lote.CodigoLote
                           select p;

            foreach (var item in imgLotes)
            {
                Model.Imagen img = LoadImagenModel(lote.CodigoLote, item.volumen, item.nro_orden, item.indice.Value, item.tipo);
                res.Add(img);
            }

            return res.AsQueryable();
        }

        private void ObtenerImagenesLote(List<Model.Imagen> res, string tmplate, string lote)
        {
            var query = _ctx.GetDatosGeneralesImagenesLote(tmplate, lote);

            foreach (var item in query)
            {
                Model.Imagen img = LoadImagenModel(lote, item.volumen, item.nro_orden, item.indice.Value, item.tipo.Value);
                res.Add(img);
            }
        }

        private Model.Imagen LoadImagenModel(string lote, string volumen, int nroOrden, int indice, int tipo)
        {
            Model.Imagen img = new Model.Imagen();
            img.Volumen = volumen;
            img.CodigoLote = lote;
            img.Indice = indice;
            img.NroOrden = nroOrden;
            img.Tipo = tipo;
            img.Extension = GetExtensionTipo(tipo);
            img.PathRedImagen = GetPathRedVolumen(volumen);
            return img;
        }

        public IQueryable<Model.Lote> GetLotesAProcesarYTransferir()
        {
            short estadoPendienteProcesar = Convert.ToInt16(Model.EstadoLote.PendienteProcesar);
            short estadoPendienteTransferirEscanner = Convert.ToInt16(Model.EstadoLote.PendienteTransferirEscaner);
            short estadoPendienteTransferirProcesamiento = Convert.ToInt16(Model.EstadoLote.PendienteTransferirProcesamiento);

            var lotes = from p in _ctx.idlotes
                        where p.indexando == estadoPendienteProcesar || p.indexando == estadoPendienteTransferirEscanner || p.indexando == estadoPendienteTransferirProcesamiento
                        select new Model.Lote
                        {
                            CodigoLote = p.cod_lote,
                            Tag = p.tag,
                            FechaCreacion = p.feccreacion,
                            CantidadImagenes = p.ult_nro,
                            Plantilla = new Model.Plantilla
                                        {
                                            CodigoPlantilla = p.tmplate,
                                            Nombre = p.tmplate1.descripcio,
                                            Cliente = new Model.Cliente
                                                      {
                                                          CodigoCliente = p.tmplate1.LoginEmpresa,
                                                          Nombre = p.tmplate1.Empresa.DescEmpresa
                                                      }
                                        }
                        };

            return lotes;
        }

        public IQueryable<Model.LoteConError> GetLotesConErrorTransferenciaYProcesamiento()
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<Model.Lote> GetLotesHistorialTransferenciaYProcesamiento()
        {
            throw new System.NotImplementedException();
        }

        public void PonerEnPapelera(Model.Lote lote, int nroOrden)
        {
            var papelera = _ctx.papeleras.SingleOrDefault(p => p.cod_lote == lote.CodigoLote && p.nro_orden == nroOrden);
            if (papelera == null)
            {
                papelera = new papelera();
                papelera.cod_lote = lote.CodigoLote;
                papelera.FechaHora = DateTime.Now;
                papelera.nro_orden = nroOrden;
                papelera.tmplate = lote.Plantilla.CodigoPlantilla;
                _ctx.papeleras.AddObject(papelera);
                _ctx.SaveChanges();
            }
        }

        public void MarcarComoSubLote(Model.Lote lote, int indice, Model.PatchCodeValues patchCode, bool eliminarImagenAnterior)
        {
            if (eliminarImagenAnterior)
            {
                var subloteAnterior = _ctx.SubLotes.SingleOrDefault(p => p.cod_lote == lote.CodigoLote && p.indice == indice - 1);
                if (subloteAnterior != null)
                {
                    _ctx.SubLotes.DeleteObject(subloteAnterior);
                }
            }

            var sublote = _ctx.SubLotes.SingleOrDefault(p => p.cod_lote == lote.CodigoLote && p.indice == indice);
            if (sublote == null)
            {
                sublote = new SubLote();
                sublote.indice = indice;
                sublote.cod_lote = lote.CodigoLote;
                _ctx.SubLotes.AddObject(sublote);
            }
            sublote.nivel = Convert.ToInt32(patchCode);
            _ctx.SaveChanges();
        }

        /// <summary>
        /// Gets the empresas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        public IQueryable<Model.Cliente> GetEmpresas(string loginToken)
        {
            List<Model.Cliente> res = new List<Cliente>();
            var list = GetEmpresas();

            if (IsLoginTokenValid(loginToken))
            {
                var usr = GetUsuario(loginToken);

                if (usr != null)
                {
                    foreach (var item in list)
                    {
                        //Tiene Permisos para ver esta Empresa
                        if (TienePermisoObjeto(usr, TareaConsultaEmpresaID, item.CodigoCliente))
                        {
                            res.Add(item);
                        }
                    }
                }
            }
            return res.AsQueryable();
        }

        private Guid GrupoAdminID = new Guid("cfbe8828-249c-4ef2-8103-8c48aed2003d");
        private Guid TareaConsultaEmpresaID = new Guid("016163d4-65cd-43f2-8c75-99961e261104");
        private Guid TareaConsultaEPlantillaID = new Guid("09e0cfc1-2430-4b83-b741-9ec992f56327");

        private bool TienePermisoObjeto(Usuario usuario, Guid tareaId, string objeto)
        {
            //Me fijo los grupos
            foreach (var item in usuario.Grupoes)
            {
                //Grupo ADMIN ED
                if (item.GrupoId == GrupoAdminID)
                {
                    return true;
                }

                var perm = item.PermisosGrupoes.FirstOrDefault(p => p.ObjetoExternoId == objeto && p.TareaId == tareaId);
                if (perm != null)
                {
                    return true;
                }
            }

            //Me dijo el Usuario
            var permUsuario = usuario.PermisosUsuarios.FirstOrDefault(p => p.ObjetoExternoId == objeto && p.TareaId == tareaId);
            if (permUsuario != null)
            {
                return true;
            }
            return false;
        }

        private Usuario GetUsuario(string loginToken)
        {
            return GetLoginToken(loginToken).Usuario;
        }

        public string GetUsuarioNombre(string loginToken)
        {
            return GetUsuario(loginToken).Nombre;
        }

        public Guid GetUsuarioId(string loginToken)
        {
            return GetUsuario(loginToken).UsuarioId;
        }

        public void ModificarDebeCambiarPassword(string loginToken, bool value)
        {
            var usr = GetUsuario(loginToken);
            usr.DebeCambiarPassword = value;
            _ctx.SaveChanges();
        }

        /// <summary>
        /// Gets the empresas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Model.Cliente> GetEmpresas()
        {
            return from p in _ctx.Empresas
                   where p.Estado == "A"
                   select new Model.Cliente
                   {
                       CodigoCliente = p.LoginEmpresa,
                       Nombre = p.DescEmpresa
                   };
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Model.PlantillaVolumen> GetPlantillasVolumenes()
        {
            var plantillas = from p in _ctx.tmplates
                             select p;

            List<Model.PlantillaVolumen> res = new List<Model.PlantillaVolumen>();
            foreach (var tmp in plantillas)
            {
                var hoy = DateTime.Now;
                //Busco los Volumnes validos para la Plantilla
                var volsTmplate = from p in _ctx.volumestemplates
                                  where p.tmplate == tmp.tmplate1
                                  select p;

                if (volsTmplate.Count() == 0)
                {
                    //No tiene seteados volumnes para el tmplate 
                    //agregar todos
                    var vols = from p in _ctx.vols
                               where (p.finiciovalidez == null || (p.ffinvalidez >= hoy && p.finiciovalidez <= hoy))
                               select p;
                    foreach (var vol in vols)
                    {
                        AddPlantillaTmplate(tmp, vol, res);
                    }
                }
                else
                {
                    //tiene seteados volumnes para el tmplate 
                    //agregar los configurados
                    var vols = from p in _ctx.vols
                               where (p.finiciovalidez == null || (p.ffinvalidez >= hoy && p.finiciovalidez <= hoy))
                               select p;

                    foreach (var vol in vols)
                    {
                        foreach (var item in volsTmplate)
                        {
                            if (item.volume == vol.volumen)
                            {
                                AddPlantillaTmplate(tmp, vol, res);
                            }
                        }
                    }
                }
            }

            return res.AsQueryable();
        }

        private void AddPlantillaTmplate(tmplate tmp, vol vol, List<PlantillaVolumen> res)
        {
            Model.PlantillaVolumen tmpVolumen = new Model.PlantillaVolumen();
            tmpVolumen.CodigoPlantilla = tmp.tmplate1;
            tmpVolumen.Nombre = tmp.descripcio;
            tmpVolumen.Cliente = new Model.Cliente
            {
                CodigoCliente = tmp.Empresa.LoginEmpresa,
                Nombre = tmp.Empresa.DescEmpresa
            };
            tmpVolumen.Volumen = new Model.Volumen
            {
                CodigoVolumen = vol.volumen
            };
            tmpVolumen.CodigoPlantillaVolumen = String.Format("{0};***;{1}", tmpVolumen.CodigoPlantilla, vol.volumen);
            res.Add(tmpVolumen);
        }

        public string GetProximoNombreLote(string path, string prefijoLote, string codigoEstacionEscaneoImportacion, string mascaraNumeroLote)
        {
            string batchPrefix = String.Format("{0}", DateTime.Now.ToString(prefijoLote));
            string batchScanningPosittion = codigoEstacionEscaneoImportacion;

            int nextNumber = GetProximoNumeroLote(String.Format("{0}_{1}", batchPrefix, batchScanningPosittion));
            string batchNumber = String.Format("{0}", nextNumber.ToString(mascaraNumeroLote));
            string batchName = batchPrefix + "_" + batchScanningPosittion + "_" + batchNumber;
            bool exists = System.IO.Directory.Exists(System.IO.Path.Combine(path, batchName));

            while (exists)
            {
                nextNumber += 1;
                batchNumber = String.Format("{0}", nextNumber.ToString(mascaraNumeroLote));
                batchName = batchPrefix + "_" + batchScanningPosittion + "_" + batchNumber;
                exists = System.IO.Directory.Exists(System.IO.Path.Combine(path, batchName));
            }
            return batchName;
        }

        /// <summary>
        /// Gets the next batch number.
        /// </summary>
        /// <param name="batchPrefix">The batch prefix.</param>
        /// <returns></returns>
        private int GetProximoNumeroLote(string inicioCodigoLote)
        {
            var lotes = from p in _ctx.idlotes
                        where p.cod_lote.StartsWith(inicioCodigoLote)
                        select p;

            int maximo = 0;
            foreach (var lote in lotes)
            {
                string cod_lote = lote.cod_lote;
                Int32 num;
                if (Int32.TryParse(cod_lote.Substring(inicioCodigoLote.Length + 1), NumberStyles.Integer, null, out num))
                {
                    if (num > maximo)
                    {
                        maximo = num;
                    }
                }
            }
            return maximo + 1;
        }

        /// <summary>
        /// Existes the lote.
        /// </summary>
        /// <param name="codLote">The cod lote.</param>
        /// <returns></returns>
        public bool ExisteLote(string codLote)
        {
            return _ctx.idlotes.SingleOrDefault(p => p.cod_lote == codLote) != null;
        }

        /// <summary>
        /// Agregars the lote importacion.
        /// </summary>
        /// <param name="loteAImportar">The lote A importar.</param>
        public void AgregarLoteImportacion(LoteImportar loteAImportar)
        {
            if (ExisteLote(loteAImportar.CodigoLoteEasyDoc))
            {
                throw new InvalidOperationException("Imposible Agregar Lote, ya existe.");
            }

            //INSERT INTO IDLOTES

            idlote loteNuevo = new idlote();
            loteNuevo.cod_lote = loteAImportar.CodigoLoteEasyDoc;
            loteNuevo.tmplate = loteAImportar.CodigoPlantilla;
            bool hayProcesamiento = _ctx.tmplates.Single(p => p.tmplate1 == loteAImportar.CodigoPlantilla).ocr == 1;
            if (hayProcesamiento)
            {
                if (loteAImportar.UsaTransferencia)
                {
                    loteNuevo.indexando = (int)EstadoLote.PendienteTransferirEscaner;
                }
                else
                {
                    loteNuevo.indexando = (int)EstadoLote.PendienteProcesar;
                }
            }
            else
            {
                loteNuevo.indexando = (int)EstadoLote.Disponible;
            }
            loteNuevo.caja = 0;
            loteNuevo.tag = loteAImportar.Tag;
            loteNuevo.scanner = loteAImportar.CodigoEstacionEscaneoImportacion;
            loteNuevo.feccreacion = _ctx.GetServerDateTime().FirstOrDefault().Value;
            loteNuevo.act_nro = Convert.ToInt16(loteAImportar.Imagenes.Count);
            loteNuevo.ult_nro = Convert.ToInt16(loteAImportar.Imagenes.Count);

            _ctx.idlotes.AddObject(loteNuevo);

            //Lotes
            var listaImagenes = loteAImportar.Imagenes.ToList();
            for (int i = 0; i < listaImagenes.Count; i++)
            {
                lote regLote = new lote();
                regLote.cod_lote = loteNuevo.cod_lote;
                regLote.nro_orden = Convert.ToInt16(i + 1);
                regLote.volumen = loteAImportar.CodigoVolumen;
                regLote.rotada = 0;
                regLote.reparada = 0;
                regLote.tipo = GetTipo(listaImagenes[i].Extension);
                regLote.ocr = "0";
                regLote.fecscan = loteNuevo.feccreacion.Value;
                regLote.indice = regLote.nro_orden;

                _ctx.lotes.AddObject(regLote);
            }

            //Insert into lotesCajas
            if (!String.IsNullOrWhiteSpace(loteAImportar.CodigoCaja))
            {
                //string cajaCompleto = string.Format("{0}_{1}", loteAImportar.Plantilla.Cliente.CodigoCliente, loteAImportar.CodigoCaja);
                //Caja caja = _ctx.Cajas.FirstOrDefault(p => p.cod_caja == cajaCompleto);
                var caja = GetCaja(loteAImportar.CodigoPlantilla, loteAImportar.CodigoCaja);
                if (caja != null)
                {
                    loteNuevo.Cajas.Add(caja);
                    caja.Estado = (int)EstadosCajas.En_Proceso;
                }
            }

            _ctx.SaveChanges();
        }

        public int GetTipo(string extension)
        {
            var tipo = _ctx.tipos.FirstOrDefault(p => p.extension == extension.Replace(".", ""));
            if (tipo != null)
            {
                return tipo.tipo;
            }
            else
            {
                return 4;
            }
        }

        /// <summary>
        /// Existes the caja.
        /// </summary>
        /// <param name="codCaja">The cod caja.</param>
        /// <returns></returns>
        public bool ExisteCaja(string codCaja)
        {
            return _ctx.Cajas.SingleOrDefault(p => p.cod_caja == codCaja) != null;
        }

        /// <summary>
        /// Existes the caja.
        /// </summary>
        /// <param name="codCaja">The cod caja.</param>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <returns></returns>
        public bool ExisteCaja(string codCaja, string codigoPlantilla)
        {
            var caja = GetCaja(codigoPlantilla, codCaja);

            return caja != null;
        }

        private Caja GetCaja(string codigoPlantilla, string codCaja)
        {
            var loginEmpresa = _ctx.tmplates.Single(p => p.tmplate1 == codigoPlantilla).LoginEmpresa;

            string cajaCompleto = string.Format("{0}_{1}", loginEmpresa, codCaja);

            var caja = _ctx.Cajas.SingleOrDefault(p => p.cod_caja == cajaCompleto);
            return caja;
        }

        /// <summary>
        /// Determines whether [is login token valid] [the specified login token].
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns>
        ///   <c>true</c> if [is login token valid] [the specified login token]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsLoginTokenValid(string loginToken)
        {
            return GetLoginToken(loginToken) != null;
        }

        private LoginToken GetLoginToken(string loginToken)
        {
            return _ctx.LoginTokens.SingleOrDefault(p => p.Token == loginToken);
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillas()
        {
            return from p in _ctx.tmplates
                   select new Model.Plantilla
                   {
                       CodigoPlantilla = p.tmplate1,
                       Nombre = p.descripcio,
                       Cliente = new Model.Cliente
                                 {
                                     CodigoCliente = p.LoginEmpresa,
                                     Nombre = p.Empresa.DescEmpresa
                                 }
                   };
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="codigoCliente">The codigo cliente.</param>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillas(string codigoCliente)
        {
            return from p in _ctx.tmplates
                   where p.LoginEmpresa == codigoCliente
                   select new Model.Plantilla
                   {
                       CodigoPlantilla = p.tmplate1,
                       Nombre = p.descripcio,
                       Cliente = new Model.Cliente
                                 {
                                     CodigoCliente = p.LoginEmpresa,
                                     Nombre = p.Empresa.DescEmpresa
                                 }
                   };
        }

        /// <summary>
        /// Gets the plantilla campos.
        /// </summary>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <returns></returns>
        public IQueryable<PlantillaCampo> GetPlantillaCampos(string codigoPlantilla)
        {
            List<PlantillaCampo> res = new List<PlantillaCampo>();

            var campos = from p in _ctx.zonas
                         where p.tmplate == codigoPlantilla
                         select p;

            foreach (var campo in campos)
            {
                Model.PlantillaCampo item = new PlantillaCampo();
                item.CampoId = campo.campo;
                item.Nombre = campo.dicdato.descripcio;
                item.Orden = campo.orden;
                item.EsObligatorio = campo.mndtory == null ? false : (campo.mndtory == 0 ? false : true);
                item.MaxValor = campo.dicdato.rango_max;
                item.MinValor = campo.dicdato.rango_min;
                item.CantidadCaracteresMaximos = campo.dicdato.longitud.HasValue ? Convert.ToInt16(campo.dicdato.longitud) : -1;
                item.TipoCampo = (TipoCampo)campo.dicdato.tipo;
                item.ValoresPosibles = new List<KeyValuePair<string, string>>();
               
                item.EsCampoDetalle = campo.ocr2 == "D";
                item.Mascara = campo.dicdato.mascara;
                if (item.TipoCampo == TipoCampo.Texto && campo.reftab != null && campo.visual.Substring(1, 1) == "2")
                {
                    string sql = string.Format("select {0} as Id,{1} as Value from {2}", item.CampoId, campo.campoext, campo.reftab);

                    var query = _ctx.ExecuteStoreQuery<ValorPosibleCombo>(sql);
                    foreach (var valor in query)
                    {
                        KeyValuePair<string, string> keyValue = new KeyValuePair<string, string>(valor.Id.ToUpper(), valor.Value.ToUpper());
                        item.ValoresPosibles.Add(keyValue);
                    }
                }

                if (item.TipoCampo == TipoCampo.Texto && campo.reftab != null && campo.visual == "00")
                {
                    item.UsaTablaDescripciones = true;
                }
                else
                 
                {
                    item.UsaTablaDescripciones = false;
            }

                res.Add(item);
            }

            return res.AsQueryable();
        }

        /// <summary>
        /// Gets the tablas externas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<TablaExterna> GetTablasExternas(string loginToken)
        {
            List<TablaExterna> res = new List<TablaExterna>();

            var tablas = from p in _ctx.zonas
                         where p.reftab != null || p.reftab != string.Empty
                         group p by new { p.reftab, p.campoext, p.campo, p.tmplate }into g
                         select new Model.TablaExterna
                         {
                             Nombre = g.Key.reftab,
                             CampoId = g.Key.campo,
                             CampoExterno = g.Key.campoext,
                             CodigoPlantilla = g.Key.tmplate
                         };
            var usr = GetUsuario(loginToken);

            if (usr != null)
            {
                foreach (var item in tablas)
                {
                    if (!YAExisteTablaExterna(item.Nombre, res) && TienePermisoObjeto(usr, TareaConsultaEPlantillaID, item.CodigoPlantilla))
                    {
                        item.ValoresPosibles = new List<KeyValuePair<string, string>>();
                        string sql = string.Format("select {0} as Id,{1} as Value from {2}", item.CampoId, item.CampoExterno, item.Nombre);

                        var query = _ctx.ExecuteStoreQuery<ValorPosibleCombo>(sql);
                        foreach (var valor in query)
                        {
                            KeyValuePair<string, string> keyValue = new KeyValuePair<string, string>(valor.Id.ToUpper(), valor.Value.ToUpper());
                            item.ValoresPosibles.Add(keyValue);
                        }
                        res.Add(item);
                    }
                }
            }

            return res.AsQueryable();
        }

        /// <summary>
        /// YAs the existe tabla externa.
        /// </summary>
        /// <param name="tabla">The tabla.</param>
        /// <param name="res">The res.</param>
        /// <returns></returns>
        private bool YAExisteTablaExterna(string tabla, List<TablaExterna> res)
        {
            foreach (var item in res)
            {
                if (item.Nombre == tabla)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the tablas externas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<TablaExterna> GetTablasExternas()
        {
            List<TablaExterna> res = new List<TablaExterna>();

            var tablas = from p in _ctx.zonas
                         where p.reftab != null || p.reftab != string.Empty
                         group p by new { p.reftab, p.campoext, p.campo }into g
                         select new Model.TablaExterna
                         {
                             Nombre = g.Key.reftab,
                             CampoId = g.Key.campo,
                             CampoExterno = g.Key.campoext
                         };

            foreach (var item in tablas)
            {
                item.ValoresPosibles = new List<KeyValuePair<string, string>>();
                string sql = string.Format("select {0} as Id,{1} as Value from {2}", item.CampoId, item.CampoExterno, item.Nombre);

                var query = _ctx.ExecuteStoreQuery<ValorPosibleCombo>(sql);
                foreach (var valor in query)
                {
                    KeyValuePair<string, string> keyValue = new KeyValuePair<string, string>(valor.Id.ToUpper(), valor.Value.ToUpper());
                    item.ValoresPosibles.Add(keyValue);
                }
                res.Add(item);
            }

            return res.AsQueryable();
        }

        public IQueryable<HitoricoCS> GetHistoricoByUsuario(string nroUsuario)
        {
            List<Model.HitoricoCS> query = new List<Model.HitoricoCS>();

            query = (from empresa in _ctx.TB_EMPRESA_DOCUMENTOS join documentos in _ctx.TB_DOCUMENTO on empresa.CD_CODIGO_DOCUMENTO equals documentos.CD_CODIGO_DOCUMENTO where empresa.CD_USUARIO_CARGA == nroUsuario where empresa.FL_ESTADO_DOCUMENTO == "1" select new HitoricoCS
            {
                codigo = documentos.CD_CODIGO_DOCUMENTO,
                fecha = empresa.FC_CREACION,
                empresa = empresa.CD_EMPRESA,
                plantilla = documentos.TX_PLANTILLA,
                nombre_empresa = empresa.TX_EMPRESA,
                usuario = empresa.CD_USUARIO_CARGA,
            }).Distinct().OrderByDescending(empresa => empresa.fecha).ToList();

            for (int i = 0; i < query.Count; i++)
            {
                query[i].campos_valores = GetCamposByDocumento(query[i].codigo).ToList();
            }

            return query.AsQueryable();
        }

        public bool AnularCaratulaCliente(int id_caratula)
        {
            var query = from doc in _ctx.TB_EMPRESA_DOCUMENTOS
                        where doc.CD_CODIGO_DOCUMENTO == id_caratula
                        select doc;

            foreach (TB_EMPRESA_DOCUMENTOS doc in query)
            {
                doc.FL_ESTADO_DOCUMENTO = "0";
            }

            try
            {
                _ctx.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IQueryable<CamposCS> GetCamposByDocumento(decimal nroDocumento)
        {
            var query = (from documentos in _ctx.TB_DOCUMENTO
                         where documentos.CD_CODIGO_DOCUMENTO == nroDocumento
                         orderby documentos.OrdenDetalle 
                         select new CamposCS
                         {
                             documento = documentos.CD_CODIGO_DOCUMENTO,
                             campo = documentos.TX_CAMPO,
                             valor = documentos.VL_CAMPO,
                         });
            return query;
        }

        public void LogOperationCaratulas(LogCaratula logDTO)
        {
            TB_LOGS item = new TB_LOGS();
            item.CD_MOVIMIENTO = logDTO.CD_MOVIMIENTO;
            item.CD_USUARIO_MOVIMIENTO = logDTO.CD_USUARIO_MOVIMIENTO;
            item.FC_MOVIMIENTO = logDTO.FC_MOVIMIENTO;
            item.TX_OBSERVACION = logDTO.TX_OBSERVACION;

            _ctx.AddObject("TB_LOGS", item);

            _ctx.SaveChanges();
        }

        public CaratulaCS GetCaratula(long nroCaratula)
        {
            var caratula = _ctx.TB_EMPRESA_DOCUMENTOS.FirstOrDefault(p => p.CD_CODIGO_DOCUMENTO == nroCaratula);

            if (caratula != null)
            {
                return LoadCaratula(caratula);
            }

            return new CaratulaCS();
        }

        private CaratulaCS LoadCaratula(TB_EMPRESA_DOCUMENTOS caratula)
        {
            CaratulaCS res = new CaratulaCS();
            res.empresa = caratula.CD_EMPRESA;
            res.nombre_empresa = caratula.TX_EMPRESA;
            res.usuario = caratula.CD_USUARIO_CARGA;
            res.fecha = caratula.FC_CREACION;
            res.codigo_3d = caratula.CD_CODIGO_3D;
            res.codigo = caratula.CD_CODIGO_DOCUMENTO;
            res.FechaInbound = caratula.FechaInbound;
            res.FechaOutbound = caratula.FechaOutBound;

            foreach (var item in caratula.TB_DOCUMENTO.OrderBy(p => p.OrdenDetalle).ThenBy(p => p.TX_CAMPO))
            {
                res.plantilla = item.TX_PLANTILLA;
                res.CodigoPlantilla = item.CD_PLANTILLA;
                CamposCS campo = new CamposCS();

                campo.campo = item.CD_CAMPO;
                campo.EsDetalle = item.ESDETALLE.HasValue && item.ESDETALLE.Value;
                campo.txCampo = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(item.TX_CAMPO.ToLower());
                campo.valor = item.VL_CAMPO;
                campo.OrdenDetalle = item.OrdenDetalle;

                res.DetalleCampos.Add(campo);
            }

            return res;
        }

        //public IQueryable<CaratulaCS> GetCaratulaCompleta()
        //{
        //    var query = (from empresa in _ctx.TB_EMPRESA_DOCUMENTOS
        //                 join documentos in _ctx.TB_DOCUMENTO on empresa.CD_CODIGO_DOCUMENTO equals documentos.CD_CODIGO_DOCUMENTO
        //                 select new CaratulaCS
        //                 {
        //                     empresa = empresa.CD_EMPRESA,
        //                     usuario = empresa.CD_USUARIO_CARGA,
        //                     fecha = empresa.FC_CREACION,
        //                     codigo = documentos.CD_CODIGO_DOCUMENTO,
        //                     valor = documentos.VL_CAMPO,
        //                     campo = documentos.TX_CAMPO,
        //                     plantilla = documentos.TX_PLANTILLA,
        //                     nombre_empresa = empresa.TX_EMPRESA,
        //                     codigo_3d = empresa.CD_CODIGO_3D,
        //                 });
        //    return query;
        //}

        public int GetMaxDocumentID()
        {
            try
            {
                return Convert.ToInt32(_ctx.TB_EMPRESA_DOCUMENTOS.Max(d => d.CD_CODIGO_DOCUMENTO));
            }
            catch
            {
                return 0;
            }
        }

        public long saveDocumento(DocumentoEmpresa documento)
        {
            List<TB_DOCUMENTO> listDocs = new List<TB_DOCUMENTO>();

            TB_EMPRESA_DOCUMENTOS empresaDoc = new TB_EMPRESA_DOCUMENTOS
            {
                CD_CODIGO_3D = documento.codigo3d,
                CD_USUARIO_CARGA = documento.usuarioCarga,
                CD_EMPRESA = documento.empresa.CodigoCliente,
                CD_CODIGO_DOCUMENTO = GetMaxDocumentID() + 1,
                TX_EMPRESA = documento.empresa.Nombre,
                FC_CREACION = documento.fechaCreacion,
                FL_ESTADO_DOCUMENTO = documento.estadoDocumento
            };

            _ctx.TB_EMPRESA_DOCUMENTOS.AddObject(empresaDoc);

            foreach (CamposCS item in documento.listaCampos)
            {
                TB_DOCUMENTO documentoEntity = new TB_DOCUMENTO
                {
                    CD_CODIGO_DOCUMENTO = empresaDoc.CD_CODIGO_DOCUMENTO,
                    CD_CAMPO = item.campo,
                    TX_CAMPO = item.txCampo,
                    CD_PLANTILLA = documento.plantilla,
                    TX_PLANTILLA = documento.txPlantilla,
                    VL_CAMPO = item.valor,
                    ESDETALLE = item.EsDetalle,
                    OrdenDetalle = item.OrdenDetalle
                };

                _ctx.TB_DOCUMENTO.AddObject(documentoEntity);
            }

            _ctx.SaveChanges();

            return empresaDoc.CD_CODIGO_DOCUMENTO;
        }

        /// <summary>
        /// Existes the key valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public bool ExisteKeyValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key)
        {
            try
            {
                string sql = string.Format("select {0} as Id,{1} as Value from {2} where {0}='{3}'", campoId, campoExterno, tablaExterna, key);

                var query = _ctx.ExecuteStoreQuery<ValorPosibleCombo>(sql);
                return query.Count() > 0;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Agregars the valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public bool AgregarValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key, string value)
        {
            try
            {
                string sql = string.Format("INSERT INTO {2}({0},{1}) VALUES ('{3}','{4}')", campoId, campoExterno, tablaExterna, key.ToUpper(), value.ToUpper());

                int num = _ctx.ExecuteStoreCommand(sql);
                return num > 0;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Editars the valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="newValue">The new value.</param>
        /// <returns></returns>
        public bool EditarValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key, string newValue)
        {
            try
            {
                string sql = string.Format("UPDATE {2} set {1} ='{4}' where {0}='{3}'", campoId, campoExterno, tablaExterna, key.ToUpper(), newValue.ToUpper());

                int num = _ctx.ExecuteStoreCommand(sql);
                return num > 0;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillasWithLoginToken(string loginToken)
        {
            List<Model.Plantilla> res = new List<Plantilla>();
            var list = GetPlantillas();

            if (IsLoginTokenValid(loginToken))
            {
                var usr = GetUsuario(loginToken);

                if (usr != null)
                {
                    foreach (var item in list)
                    {
                        //Tiene Permisos para ver esta Empresa
                        if (TienePermisoObjeto(usr, TareaConsultaEPlantillaID, item.CodigoPlantilla))
                        {
                            res.Add(item);
                        }
                    }
                }
            }
            return res.AsQueryable();
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <param name="codigoCliente">The codigo cliente.</param>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillas(string loginToken, string codigoCliente)
        {
            List<Model.Plantilla> res = new List<Plantilla>();
            var list = GetPlantillas(codigoCliente);

            if (IsLoginTokenValid(loginToken))
            {
                var usr = GetUsuario(loginToken);

                if (usr != null)
                {
                    foreach (var item in list)
                    {
                        //Tiene Permisos para ver esta Empresa
                        if (TienePermisoObjeto(usr, TareaConsultaEPlantillaID, item.CodigoPlantilla))
                        {
                            res.Add(item);
                        }
                    }
                }
            }
            return res.AsQueryable();
        }

        public IQueryable<CaratulaCS> GetCaratulas()
        {
            List<CaratulaCS> res = new List<CaratulaCS>();

            var caratulas = from p in _ctx.TB_EMPRESA_DOCUMENTOS
                            where p.FL_ESTADO_DOCUMENTO == "1"
                            select p;

            foreach (var caratula in caratulas)
            {
                res.Add(LoadCaratula(caratula));
            }
           
            return res.AsQueryable();
        }

        public IQueryable<CaratulaCS> GetCaratulas(string username)
        {
            List<CaratulaCS> res = new List<CaratulaCS>();

            var caratulas = from p in _ctx.TB_EMPRESA_DOCUMENTOS
                            where p.CD_USUARIO_CARGA == username &&
                                  p.FL_ESTADO_DOCUMENTO == "1"
                            select p;

            foreach (var caratula in caratulas)
            {
                res.Add(LoadCaratula(caratula));
            }

            return res.AsQueryable();
        }

        public void RealizarInbound(long numeroCaratula)
        {
            var caratula = _ctx.TB_EMPRESA_DOCUMENTOS.FirstOrDefault(p => p.CD_CODIGO_DOCUMENTO == numeroCaratula);
            if (caratula != null && !caratula.FechaInbound.HasValue)
            {
                caratula.FechaInbound = DateTime.Now;
                _ctx.SaveChanges();
            }
        }

        public void CompletarValorImagenesCb(string lote)
        {
            string query = String.Format("select * from ZCARATAB where COD_LOTE = '{0}' order by INDICE", lote);
            var datosZ = _ctx.ExecuteStoreQuery<ZCARATAB>(query);

            //no se por que mierda no funciona este query, es una locura
            //var datosZ = from p in _ctx.ZCARATABs
            //             where p.COD_LOTE == lote
            //             orderby p.INDICE
            //             select p;
                
            string valorAnteriorCodigoNroCar = string.Empty;

            foreach (var img in datosZ)
            {
                //verifico que sea caracter valido
                if (NroCaratulaValido(img.NROCAR))
                {
                    if (img.NROCAR == "0")
                    {
                        _ctx.ExecuteStoreCommand(String.Format("update ZCARATAB set NROCAR='{0}' where COD_LOTE='{1}' and NRO_ORDEN='{2}'", String.Empty, img.COD_LOTE, img.NRO_ORDEN));
                        //img.NROCAR = string.Empty;
                    }

                    if (img.NROCAR != null && !string.IsNullOrEmpty(img.NROCAR.Trim()))
                    {
                        valorAnteriorCodigoNroCar = img.NROCAR.Trim();
                    }
                    else
                    {
                        _ctx.ExecuteStoreCommand(String.Format("update ZCARATAB set NROCAR='{0}' where COD_LOTE='{1}' and NRO_ORDEN='{2}'", valorAnteriorCodigoNroCar, img.COD_LOTE, img.NRO_ORDEN));
                        //img.NROCAR = valorAnteriorCodigoNroCar;
                    }
                }
                else
                {
                    throw new Exception(String.Format("El número de caratula: {0} es inválido. Ubicado en el lote {1}, número de orden {2}", img.NROCAR, img.COD_LOTE, img.NRO_ORDEN));
                }
            }
            _ctx.SaveChanges();
        }

        private bool NroCaratulaValido(string nroCaratula)
        {
            Double result;
            return Double.TryParse(nroCaratula, out result);        
        }

        public void RealizarOutbound(long numeroCaratula)
        {
            var caratula = _ctx.TB_EMPRESA_DOCUMENTOS.FirstOrDefault(p => p.CD_CODIGO_DOCUMENTO == numeroCaratula);
            if (caratula != null && caratula.FechaInbound.HasValue && !caratula.FechaOutBound.HasValue)
            {
                caratula.FechaOutBound = DateTime.Now;
                _ctx.SaveChanges();
            }
        }

        public IQueryable<CaratulaCS> GetCaratulasPorLote(string codigoLote)
        {
            List<CaratulaCS> res = new List<CaratulaCS>();
            string sql = string.Format("select NROCAR from dbo.ZCARATAB where cod_lote='{0}' group by NROCAR", codigoLote);

            var query = _ctx.ExecuteStoreQuery<string>(sql);

            foreach (var item in query)
            {
                CaratulaCS car = new CaratulaCS();
                car.codigo = Convert.ToInt64(item);
                res.Add(car);
            }
            return res.AsQueryable();
        }

        private class NumeroCaratulaLote
        {
            /// <summary>
            /// Gets or sets the NROCAR.
            /// </summary>
            /// <value>
            /// The NROCAR.
            /// </value>
            public string NROCAR { get; set; }
            /// <summary>
            /// Gets or sets the CO d_ LOTE.
            /// </summary>
            /// <value>
            /// The CO d_ LOTE.
            /// </value>
            public string COD_LOTE { get; set; }

            public NumeroCaratulaLote(string NROCAR, string COD_LOTE)
            {
                this.NROCAR = NROCAR;
                this.COD_LOTE = COD_LOTE;
            }

            public NumeroCaratulaLote()
            {
            }
        }

        private class ImagenNroOrdenVolumen
        {
            /// <summary>
            /// Gets or sets the nro orden.
            /// </summary>
            /// <value>
            /// The nro orden.
            /// </value>
            public int NRO_ORDEN { get; set; }

            /// <summary>
            /// Gets or sets the INDICE.
            /// </summary>
            /// <value>
            /// The INDICE.
            /// </value>
            public int INDICE { get; set; }
          
            /// <summary>
            /// Gets or sets the VOLUMEN.
            /// </summary>
            /// <value>
            /// The VOLUMEN.
            /// </value>
            public string VOLUMEN { get; set; }

            /// <summary>
            /// Gets or sets the TIPO.
            /// </summary>
            /// <value>
            /// The TIPO.
            /// </value>
            public int TIPO { get; set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="ImagenNroOrdenVolumen"/> class.
            /// </summary>
            /// <param name="NRO_ORDEN">The NR o_ ORDEN.</param>
            /// <param name="VOLUMEN">The VOLUMEN.</param>
            /// <param name="TIPO">The TIPO.</param>
            /// <param name="?">The ?.</param>
            public ImagenNroOrdenVolumen(int NRO_ORDEN, string VOLUMEN, int TIPO, int INDICE)
            {
                this.NRO_ORDEN = NRO_ORDEN;
                this.VOLUMEN = VOLUMEN;
                this.TIPO = TIPO;
                this.INDICE = INDICE;
            }

            public ImagenNroOrdenVolumen()
            {
            }
        }

        public List<KeyValuePair<string, Lote>> GetNumeroCaratulasPendientesProcesar()
        {
            //EDEntities ctx = new EDEntities();
            List<KeyValuePair<string, Model.Lote>> res = new List<KeyValuePair<string, Model.Lote>>();

            string sql = string.Format("select z.NROCAR as NROCAR, z.COD_LOTE as COD_LOTE from ZCARATAB z, idlotes id where id.COD_LOTE = z.COD_LOTE AND id.indexando <> -100 AND z.Procesado=0 AND LTRIM(RTRIM(z.NROCAR)) <> '' AND z.NROCAR is not null group by z.NROCAR,z.COD_LOTE");

            var query = _ctx.ExecuteStoreQuery<NumeroCaratulaLote>(sql);

            //var query = from p in ctx.ZCARATABs
            //            where p.Procesado == false
            //            group p by new { NROCAR = p.NROCAR, COD_LOTE = p.COD_LOTE } into resultado
            //            select new NumeroCaratulaLote
            //                {
            //                    NROCAR = resultado.Key.NROCAR,
            //                    COD_LOTE = resultado.Key.COD_LOTE
            //                };
     
            foreach (var item in query)
            {
                if (item != null)
                {
                    if (!String.IsNullOrEmpty(item.NROCAR) && !String.IsNullOrEmpty(item.COD_LOTE))
                    {
                        //Obtengo el Lote
                        KeyValuePair<string, Model.Lote> pair = new KeyValuePair<string, Model.Lote>(item.NROCAR, GetLote(item.COD_LOTE));
                        res.Add(pair);
                    }
                    

                }
            }

            return res;
        }

        public Model.Lote GetLote(string cod_lote)
        {
            EDEntities ctx = new EDEntities();
            var p = ctx.idlotes.FirstOrDefault(q => q.cod_lote == cod_lote);

            var lote = new Model.Lote
            {
                CodigoLote = p.cod_lote,
                Tag = p.tag,
                FechaCreacion = p.feccreacion,
                CantidadImagenes = p.ult_nro,
                Plantilla = new Model.Plantilla
                            {
                                CodigoPlantilla = p.tmplate,
                                Nombre = p.tmplate1.descripcio,
                                Cliente = new Model.Cliente
                                          {
                                              CodigoCliente = p.tmplate1.LoginEmpresa,
                                              Nombre = p.tmplate1.Empresa.DescEmpresa
                                          }
                            }
            };

            return lote;
        }

        public IQueryable<Model.Imagen> GetImagenesCaratula(string codLote, long codigoCaratula)
        {
            List<Model.Imagen> res = new List<Model.Imagen>();

            string sql = string.Format("select NRO_ORDEN,VOLUMEN,TIPO,INDICE from dbo.ZCARATAB z1 where Procesado=0 and COD_LOTE='{0}' and NROCAR='{1}' and not exists(select * from dbo.papelera p where z1.COD_LOTE = p.cod_lote and z1.NRO_ORDEN = p.nro_orden) order by INDICE", codLote, codigoCaratula);

            var query = _ctx.ExecuteStoreQuery<ImagenNroOrdenVolumen>(sql);

            foreach (var item in query)
            {
                Model.Imagen img = new Model.Imagen();
                img.CodigoLote = codLote;
                img.Extension = GetExtensionTipo(item.TIPO);
                img.Indice = item.INDICE;
                img.NroOrden = item.NRO_ORDEN;
                img.PathRedImagen = GetPathRedVolumen(item.VOLUMEN);
                img.Tipo = item.TIPO;
                img.Volumen = item.VOLUMEN;
                res.Add(img);               
            }

            return res.AsQueryable();
        }

        public void AddLote(Lote lote, List<Model.Imagen> imagenes, CaratulaCS car)
        {
            var l = _ctx.idlotes.FirstOrDefault(p => p.cod_lote == lote.CodigoLote);

            if (l == null)
            {
                l = new idlote();
                l.cod_lote = lote.CodigoLote;
                _ctx.idlotes.AddObject(l);
            }
            l.act_nro = 0;
            l.ult_nro = Convert.ToInt16(imagenes.Count());
            l.caja = 0;
            l.controlrealizado = 0;
            l.tmplate = lote.Plantilla.CodigoPlantilla;
            l.indexando = 0;
            l.scanner = "CARATULA";
            l.tag = lote.Tag;
            l.feccreacion = DateTime.Now;
            l.fechaCC = DateTime.Now;
            l.fechaPapelera = DateTime.Now;

            StringBuilder sqlInsert = new StringBuilder();

            sqlInsert.AppendFormat("INSERT INTO {0}AB(COD_LOTE,NRO_ORDEN,VOLUMEN,ROTACION,TIPO,TAMANO,INDICE,NROCAR", lote.Plantilla.CodigoPlantilla);
            var campos = GetPlantillaCampos(l.tmplate);
            foreach (var campo in campos)
            {
                if (campo.CampoId != "VERSDOC" && campo.CampoId != "VERSDOC2")
                {
                    sqlInsert.AppendFormat(",{0}", campo.CampoId);
                }
            }
         
            sqlInsert.AppendLine(") VALUES (");

            foreach (var img in imagenes)
            {
                StringBuilder sqlInsertFinal = new StringBuilder(sqlInsert.ToString());

                sqlInsertFinal.AppendFormat("'{0}',{1},'{2}',{3},{4},{5},{6},'{7}'", lote.CodigoLote, img.NroOrden, img.Volumen, 0, img.Tipo, 0, img.Indice, car.codigo);

                foreach (var campo in campos)
                {
                    if (campo.CampoId != "VERSDOC" && campo.CampoId != "VERSDOC2")
                    {
                        switch (campo.TipoCampo)
                        {
                            case TipoCampo.Texto:
                                if (campo.CampoId != "DEPEND")
                                {
                                    sqlInsertFinal.AppendFormat(",'{0}'", car.DetalleCampos.FirstOrDefault(p => p.campo == campo.CampoId).valor);
                                }
                                else
                                {
                                    string nroDep = car.DetalleCampos.FirstOrDefault(p => p.campo == campo.CampoId).valor;
                                    sqlInsertFinal.AppendFormat(",'{0}'", nroDep.Substring(0,nroDep.IndexOf('-')));
                                }
                                break;
                            case TipoCampo.Numero:
                                sqlInsertFinal.AppendFormat(",{0}", car.DetalleCampos.FirstOrDefault(p => p.campo == campo.CampoId).valor);
                                break;
                            case TipoCampo.Fecha:
                                sqlInsertFinal.AppendFormat(",'{0}'", car.DetalleCampos.FirstOrDefault(p => p.campo == campo.CampoId).valor);
                                break;
                            default:
                                break;
                        }
                    }
                }
                sqlInsertFinal.AppendLine(")");

                string sqlDelete = String.Format("DELETE FROM {0}AB where COD_LOTE='{1}' and nro_orden='{2}'", lote.Plantilla.CodigoPlantilla, lote.CodigoLote, img.NroOrden);
                string sqlUpdate = string.Format("UPDATE ZCARATAB set Procesado=1, TMPL='{0}' where COD_LOTE='{1}' and NROCAR='{2}'", lote.Plantilla.CodigoPlantilla, lote.CodigoLote.Substring(0, lote.CodigoLote.LastIndexOf("_")), car.codigo);
                _ctx.ExecuteStoreCommand(sqlDelete);
                _ctx.ExecuteStoreCommand(sqlInsertFinal.ToString());
                _ctx.ExecuteStoreCommand(sqlUpdate);
            }

            _ctx.SaveChanges();
        }

        public void MergeLote(string CodLote, CaratulaCS car)
        {
            string tmp = GetPlantillaLote(CodLote);

            StringBuilder cambios = new StringBuilder();

            var campos = GetPlantillaCampos(tmp);
            foreach (var campo in campos)
            {
                switch (campo.TipoCampo)
                {
                    case TipoCampo.Texto:
                        cambios.AppendFormat("{1} = '{0}',", car.DetalleCampos.FirstOrDefault(p => p.campo == campo.CampoId).valor, campo.CampoId);
                        break;
                    case TipoCampo.Numero:
                        cambios.AppendFormat("{1} = {0},", car.DetalleCampos.FirstOrDefault(p => p.campo == campo.CampoId).valor, campo.CampoId);
                        break;
                    case TipoCampo.Fecha:
                        cambios.AppendFormat("{1} = '{0}',", car.DetalleCampos.FirstOrDefault(p => p.campo == campo.CampoId).valor, campo.CampoId);
                        break;
                    default:
                        break;
                }
            }
          
            StringBuilder sqlInsert = new StringBuilder();

            sqlInsert.AppendFormat("UPDATE {0}AB SET {1}NROCAR = NROCAR where NROCAR='{2}'", tmp, cambios.ToString(), car.codigo);

            _ctx.ExecuteStoreCommand(sqlInsert.ToString());
            
            _ctx.SaveChanges();
        }

        public IQueryable<CaratulaCS> GetCaratulasProcesadasPorLote(string codLote)
        {
            //Obtengo el tmplate del lote
            string tmp = GetPlantillaLote(codLote);

            List<CaratulaCS> res = new List<CaratulaCS>();

            if (!string.IsNullOrEmpty(tmp))
            {
                var sql = string.Format("select NROCAR from dbo.{1}AB where cod_lote='{0}' group by NROCAR", codLote, tmp);

                var query = _ctx.ExecuteStoreQuery<string>(sql);

                foreach (var item in query)
                {
                    res.Add(GetCaratula(Convert.ToInt64(item)));
                }
            }
            return res.AsQueryable();
        }

        private string GetPlantillaLote(string codLote)
        {
            string sql = string.Format("select tmplate from dbo.idlotes where cod_lote='{0}' and tmplate <> 'ZCARAT'", codLote);
            var plantillas = _ctx.ExecuteStoreQuery<string>(sql);
            string tmp = string.Empty;
            foreach (var item in plantillas)
            {
                tmp = item;
            }

            return tmp;
        }

        public List<CaratulaCS> GetCaratulas(long[] caratulas)
        {
            List<CaratulaCS> res = new List<CaratulaCS>();
            foreach (var caratulaId in caratulas)
            {
                res.Add(GetCaratula(caratulaId));
            }

            return res;
        }

        public long SaveCaratula(CaratulaCS item)
        {
            List<TB_DOCUMENTO> listDocs = new List<TB_DOCUMENTO>();

            TB_EMPRESA_DOCUMENTOS empresaDoc = new TB_EMPRESA_DOCUMENTOS
            {
                CD_CODIGO_3D = item.codigo_3d,
                CD_USUARIO_CARGA = item.usuario,
                CD_EMPRESA = item.nombre_empresa,
                CD_CODIGO_DOCUMENTO = GetMaxDocumentID() + 1,
                TX_EMPRESA = item.empresa,
                FC_CREACION = item.fecha,
                FL_ESTADO_DOCUMENTO = "1"
            };

            _ctx.TB_EMPRESA_DOCUMENTOS.AddObject(empresaDoc);

            foreach (CamposCS cmp in item.DetalleCampos)
            {
                TB_DOCUMENTO documentoEntity = new TB_DOCUMENTO
                {
                    CD_CODIGO_DOCUMENTO = empresaDoc.CD_CODIGO_DOCUMENTO,
                    CD_CAMPO = cmp.campo,
                    TX_CAMPO = cmp.txCampo,
                    CD_PLANTILLA = item.CodigoPlantilla,
                    TX_PLANTILLA = item.plantilla,
                    VL_CAMPO = cmp.valor,
                    ESDETALLE = cmp.EsDetalle,
                    OrdenDetalle = cmp.OrdenDetalle
                };

                _ctx.TB_DOCUMENTO.AddObject(documentoEntity);
            }

            _ctx.SaveChanges();

            return empresaDoc.CD_CODIGO_DOCUMENTO;
        }

        public string GetInstancia()
        {
            var inst = _ctx.parameters.FirstOrDefault(p => p.guid == "instancia");

            if (inst == null)
            {
                return "PROD";
            }
            else
            {
                return inst.value;
            }
        }

        public string GetDBVersion()
        {
            throw new System.NotImplementedException();
        }


        public string GetCampoDescripcion(string plantillaId, string campoId, string campoValue)
        {
            var campo = (from p in _ctx.zonas
                         where p.tmplate == plantillaId && p.campo == campoId

                         select p).FirstOrDefault();

            if (campo == null)
            {
                return string.Empty;
            }
            else
            {
                string sql = string.Format("select {0} as Id,{1} as Value from {2} where {0} = '{3}'", campoId, campo.campoext, campo.reftab, campoValue);

                var query = _ctx.ExecuteStoreQuery<ValorPosibleCombo>(sql);
                foreach (var valor in query)
                {
                    return valor.Value.ToUpper();
                }
            }
            return string.Empty;
        }
    }
}