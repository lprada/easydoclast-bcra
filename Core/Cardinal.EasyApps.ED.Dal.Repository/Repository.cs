﻿using System;
using System.Linq;
using Cardinal.EasyApps.ED.Common.Dal;
using Cardinal.EasyApps.ED.Model;

namespace Cardinal.EasyApps.ED.Dal.Repository
{
    /// <summary>
    /// Repositorio de ED
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
    public class Repository : IRepository
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        string _connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public Repository(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Gets the DB version.
        /// </summary>
        /// <returns></returns>
        public string GetDBVersion()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the lotes por estado.
        /// </summary>
        /// <param name="estado">The estado.</param>
        /// <returns></returns>
        public IQueryable<Lote> GetLotesPorEstado(EstadoLote estado)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the lotes A procesar Y transferir.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Lote> GetLotesAProcesarYTransferir()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the lotes con error transferencia Y procesamiento.
        /// </summary>
        /// <returns></returns>
        public IQueryable<LoteConError> GetLotesConErrorTransferenciaYProcesamiento()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the lotes historial transferencia Y procesamiento.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Lote> GetLotesHistorialTransferenciaYProcesamiento()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the imagenes lote.
        /// </summary>
        /// <param name="lote">The lote.</param>
        /// <returns></returns>
        public IQueryable<Imagen> GetImagenesLote(Lote lote)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the path red volumen.
        /// </summary>
        /// <param name="volumen">The volumen.</param>
        /// <returns></returns>
        public string GetPathRedVolumen(string volumen)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the extension tipo.
        /// </summary>
        /// <param name="tipo">The tipo.</param>
        /// <returns></returns>
        public string GetExtensionTipo(int tipo)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Poners the en papelera.
        /// </summary>
        /// <param name="lote">The lote.</param>
        /// <param name="nroOrden">The nro orden.</param>
        public void PonerEnPapelera(Lote lote, int nroOrden)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Marcars the como sub lote.
        /// </summary>
        /// <param name="lote">The lote.</param>
        /// <param name="indice">The indice.</param>
        /// <param name="patchCode">The patch code.</param>
        /// <param name="eliminarImagenAnterior">if set to <c>true</c> [eliminar imagen anterior].</param>
        public void MarcarComoSubLote(Lote lote, int indice, PatchCodeValues patchCode, bool eliminarImagenAnterior)
        {
            throw new NotImplementedException();
        }




        /// <summary>
        /// Gets the empresas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Cliente> GetEmpresas()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillas()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the volumenes validos.
        /// </summary>
        /// <param name="CodigoPlantilla">The codigo plantilla.</param>
        /// <returns></returns>
        public IQueryable<Volumen> GetVolumenesValidos(string CodigoPlantilla)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<PlantillaVolumen> GetPlantillasVolumenes()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the proximo nombre lote.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="prefijoLote">The prefijo lote.</param>
        /// <param name="codigoEstacionEscaneoImportacion">The codigo estacion escaneo importacion.</param>
        /// <param name="mascaraNumeroLote">The mascara numero lote.</param>
        /// <returns></returns>
        public string GetProximoNombreLote(string path, string prefijoLote, string codigoEstacionEscaneoImportacion, string mascaraNumeroLote)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Agregars the lote importacion.
        /// </summary>
        /// <param name="loteAImportar">The lote A importar.</param>
        public void AgregarLoteImportacion(LoteImportar loteAImportar)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Existes the lote.
        /// </summary>
        /// <param name="codLote">The cod lote.</param>
        /// <returns></returns>
        public bool ExisteLote(string codLote)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Existes the caja.
        /// </summary>
        /// <param name="codCaja">The cod caja.</param>
        /// <returns></returns>
        public bool ExisteCaja(string codCaja)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Existes the caja.
        /// </summary>
        /// <param name="codCaja">The cod caja.</param>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <returns></returns>
        public bool ExisteCaja(string codCaja, string codigoPlantilla)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the cliente by codigo plantilla.
        /// </summary>
        /// <param name="codigoPlantila">The codigo plantila.</param>
        /// <returns></returns>
        public Cliente GetClienteByCodigoPlantilla(string codigoPlantila)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Determines whether [is login token valid] [the specified login token].
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns>
        ///   <c>true</c> if [is login token valid] [the specified login token]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsLoginTokenValid(string loginToken)
        {
            throw new NotImplementedException();
        }




        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="codigoCliente">The codigo cliente.</param>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillas(string codigoCliente)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the plantilla campos.
        /// </summary>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <returns></returns>
        public IQueryable<PlantillaCampo> GetPlantillaCampos(string codigoPlantilla)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the tablas externas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<TablaExterna> GetTablasExternas()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Existes the key valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public bool ExisteKeyValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Agregars the valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public bool AgregarValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key, string value)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the historico by usuario.
        /// </summary>
        /// <param name="nroUsuario">The nro usuario.</param>
        /// <returns></returns>
        public IQueryable<HitoricoCS> GetHistoricoByUsuario(string nroUsuario)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Anulars the caratula cliente.
        /// </summary>
        /// <param name="id_caratula">The id_caratula.</param>
        /// <returns></returns>
        public bool AnularCaratulaCliente(int id_caratula)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the campos by documento.
        /// </summary>
        /// <param name="nroDocumento">The nro documento.</param>
        /// <returns></returns>
        public IQueryable<CamposCS> GetCamposByDocumento(decimal nroDocumento)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Logs the operation caratulas.
        /// </summary>
        /// <param name="logDTO">The log DTO.</param>
        public void LogOperationCaratulas(LogCaratula logDTO)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the caratula vista por id.
        /// </summary>
        /// <param name="nroCaratula">The nro caratula.</param>
        /// <returns></returns>
        public IQueryable<CaratulaCS> GetCaratulaVistaPorId(int nroCaratula)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the caratula completa.
        /// </summary>
        /// <returns></returns>
        public IQueryable<CaratulaCS> GetCaratulaCompleta()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the max document ID.
        /// </summary>
        /// <returns></returns>
        public int GetMaxDocumentID()
        {
            throw new NotImplementedException();
        }

      


        /// <summary>
        /// Editars the valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="newValue">The new value.</param>
        /// <returns></returns>
        public bool EditarValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key, string newValue)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the empresas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        public IQueryable<Cliente> GetEmpresas(string loginToken)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillasWithLoginToken(string loginToken)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <param name="codigoCliente">The codigo cliente.</param>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillas(string loginToken, string codigoCliente)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Saves the documento.
        /// </summary>
        /// <param name="documento">The documento.</param>
        /// <returns></returns>
        public long saveDocumento(DocumentoEmpresa documento)
        {
            throw new NotImplementedException();
        }
    }
}