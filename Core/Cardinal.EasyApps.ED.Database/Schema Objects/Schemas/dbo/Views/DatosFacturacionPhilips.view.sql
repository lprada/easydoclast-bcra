﻿/*CREATE VIEW dbo.DatosFacturacionPhilips
AS
SELECT     'PHILIPS' AS Cliente,
                          (SELECT     FechaBajada
                            FROM          dbo.Entregas
                            WHERE      (cod_entrega IN
                                                       (SELECT     cod_entrega
                                                         FROM          dbo.CajasEntregas
                                                         WHERE      (cod_caja IN
                                                                                    (SELECT     cod_caja
                                                                                      FROM          dbo.LotesCajas
                                                                                      WHERE      (cod_lote = z.COD_LOTE)))))) AS FechaBajada, z.LOTE_Exo, COUNT(*) AS img,
                          (SELECT     COUNT(DISTINCT CRYDER_E) AS Expr1
                            FROM          dbo.ZANNPBAB AS z1
                            WHERE      (COD_LOTE = z.COD_LOTE) AND (LOTE_Exo = z.LOTE_Exo)) AS doc
FROM         dbo.ZANNPBAB AS z LEFT OUTER JOIN
                      dbo.papelera AS p ON z.COD_LOTE = p.cod_lote AND z.NRO_ORDEN = p.nro_orden
WHERE     (p.cod_lote IS NULL) AND (z.COD_LOTE IN
                          (SELECT     cod_lote
                            FROM          dbo.LotesCajas AS LotesCajas_1
                            WHERE      (cod_caja IN
                                                       (SELECT     cod_caja
                                                         FROM          dbo.CajasEntregas AS CajasEntregas_1
                                                         WHERE      (cod_entrega IN
                                                                                    (SELECT     cod_entrega
                                                                                      FROM          dbo.Entregas AS Entregas_1
                                                                                      WHERE      (LoginEmpresa = 'RYDER') OR
                                                                                                             (LoginEmpresa = 'EXO')))))))
GROUP BY z.LOTE_Exo, z.COD_LOTE*/

GO
/*EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "z"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 252
               Bottom = 125
               Right = 428
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 3600
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DatosFacturacionPhilips';*/


GO
/*EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DatosFacturacionPhilips';*/

