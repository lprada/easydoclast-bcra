﻿CREATE TABLE [dbo].[TmplatesDatosComerciales] (
    [Tmplate]                VARCHAR (8)      NOT NULL,
    [BillingType]            NVARCHAR (50)    NOT NULL,
    [DiasProcesoWarning]     INT              NOT NULL,
    [DiasProcesoError]       INT              NOT NULL,
    [CajasMensuales]         INT              NOT NULL,
    [SectorId]               UNIQUEIDENTIFIER NOT NULL,
    [ResponsableId]          UNIQUEIDENTIFIER NOT NULL,
    [PeriodicidadDias]       TINYINT          NOT NULL,
    [VolumenEsperadoDiario]  BIGINT           NOT NULL,
    [VolumenEsperadoSemanal] BIGINT           NOT NULL,
    [VolumenEsperadoMensual] BIGINT           NOT NULL,
    [Instructivo]            NVARCHAR (MAX)   NULL,
    [Cliente]                NVARCHAR (50)    NOT NULL,
    [MedioEntrega]           NVARCHAR (50)    NOT NULL
);



