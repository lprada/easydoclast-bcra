﻿CREATE TABLE [dbo].[CajasDefinitivas] (
    [cod_caja]           VARCHAR (30)  NOT NULL,
    [LoginEmpresa]       VARCHAR (20)  NULL,
    [Fecha]              DATETIME      NULL,
    [Estado]             INT           NULL,
    [FechaDatosMerge]    DATETIME      NULL,
    [CantSobrantes]      INT           NULL,
    [CantFaltantes]      INT           NULL,
    [FechaInicioProceso] DATETIME      NULL,
    [FechaDevolucion]    DATETIME      NULL,
    [descr]              VARCHAR (200) NULL
);



