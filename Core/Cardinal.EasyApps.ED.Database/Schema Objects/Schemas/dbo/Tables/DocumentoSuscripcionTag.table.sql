﻿CREATE TABLE [dbo].[DocumentoSuscripcionTag] (
    [DocumentoSuscripcionTagId] UNIQUEIDENTIFIER NOT NULL,
    [FechaAlta]                 DATETIME         NOT NULL,
    [Template]                  VARCHAR (8)      NOT NULL,
    [UsuarioId]                 CHAR (50)        NULL,
    [GrupoId]                   CHAR (50)        NULL,
    [TipoTagId]                 INT              NOT NULL
);

