﻿ALTER TABLE [dbo].[permissions_groups_T]
    ADD CONSTRAINT [pk_permissions_groups_T] PRIMARY KEY CLUSTERED ([task_id_fk] ASC, [group_id_fk] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

