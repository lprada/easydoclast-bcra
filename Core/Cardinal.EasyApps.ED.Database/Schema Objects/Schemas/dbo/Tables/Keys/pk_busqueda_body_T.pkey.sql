﻿ALTER TABLE [dbo].[busqueda_body_T]
    ADD CONSTRAINT [pk_busqueda_body_T] PRIMARY KEY CLUSTERED ([id] ASC, [abro_parentesis] ASC, [campo] ASC, [valor] ASC, [operador] ASC, [cierro_parentesis] ASC, [operador_row] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

