﻿CREATE TABLE [dbo].[aud_seg] (
    [cod_us]     INT           NOT NULL,
    [fecha]      SMALLDATETIME NOT NULL,
    [hora]       SMALLDATETIME NOT NULL,
    [tipo_obj]   VARCHAR (1)   NOT NULL,
    [cod_obj]    VARCHAR (30)  NOT NULL,
    [operacion]  INT           NOT NULL,
    [comentario] VARCHAR (100) NULL
);



