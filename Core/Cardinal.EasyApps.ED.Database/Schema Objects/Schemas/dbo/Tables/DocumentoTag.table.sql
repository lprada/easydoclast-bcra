﻿CREATE TABLE [dbo].[DocumentoTag] (
    [DocumentoTagId] UNIQUEIDENTIFIER NOT NULL,
    [DocumentoId]    UNIQUEIDENTIFIER NOT NULL,
    [TipoTagId]      INT              NOT NULL,
    [Fecha]          DATETIME         NOT NULL,
    [UsuarioId]      CHAR (50)        NOT NULL,
    [Eliminada]      BIT              NOT NULL
);

