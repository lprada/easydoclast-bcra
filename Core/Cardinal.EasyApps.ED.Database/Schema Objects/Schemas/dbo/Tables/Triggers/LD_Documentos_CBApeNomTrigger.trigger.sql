﻿-- =============================================
-- Create trigger basic template(After trigger)
-- =============================================
CREATE TRIGGER [LD_Documentos_CBApeNomTrigger]
ON [dbo].[LD_Documentos_HistoricoCB]
FOR DELETE, INSERT, UPDATE 
AS 
BEGIN
	update LD_Documentos_HistoricoCB set apenom = apellido + ' ' + nombres where apenom <> apellido + ' ' + nombres
END


