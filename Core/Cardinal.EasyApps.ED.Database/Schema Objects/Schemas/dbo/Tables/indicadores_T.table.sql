﻿CREATE TABLE [dbo].[indicadores_T] (
    [indicador]   VARCHAR (50)  NOT NULL,
    [etiqueta]    VARCHAR (100) NOT NULL,
    [polaridad]   CHAR (2)      NULL,
    [verde]       BIGINT        NULL,
    [amarillo]    BIGINT        NULL,
    [rojo]        BIGINT        NULL,
    [leyverde]    VARCHAR (250) NULL,
    [leyamarillo] VARCHAR (250) NULL,
    [leyrojo]     VARCHAR (250) NULL,
    [spCant]      VARCHAR (100) NULL,
    [spPorc]      VARCHAR (100) NULL
);



