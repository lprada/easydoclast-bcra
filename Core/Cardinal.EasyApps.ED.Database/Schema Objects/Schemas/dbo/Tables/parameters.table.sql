﻿CREATE TABLE [dbo].[parameters] (
    [guid]               VARCHAR (255) NOT NULL,
    [description]        VARCHAR (255) NOT NULL,
    [type]               TINYINT       NOT NULL,
    [value]              VARCHAR (255) NOT NULL,
    [validateexpression] VARCHAR (255) NOT NULL
);



