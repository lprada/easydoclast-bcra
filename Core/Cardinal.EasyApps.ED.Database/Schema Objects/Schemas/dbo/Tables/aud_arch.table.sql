﻿CREATE TABLE [dbo].[aud_arch] (
    [pt]       SMALLINT      NOT NULL,
    [vol_ini]  VARCHAR (8)   NOT NULL,
    [vol_fin]  VARCHAR (8)   NOT NULL,
    [cod_lote] VARCHAR (50)  NOT NULL,
    [cod_us]   INT           NOT NULL,
    [fecha]    SMALLDATETIME NULL,
    [inicio]   SMALLDATETIME NOT NULL,
    [tiempo]   SMALLDATETIME NOT NULL,
    [total]    SMALLINT      NOT NULL,
    [totalOK]  SMALLINT      NOT NULL
);



