﻿CREATE TABLE [dbo].[dicdatos] (
    [campo]      VARCHAR (8)  NOT NULL,
    [descripcio] VARCHAR (50) NOT NULL,
    [tipo]       SMALLINT     NOT NULL,
    [longitud]   SMALLINT     NULL,
    [decimales]  SMALLINT     NULL,
    [rango_min]  VARCHAR (20) NULL,
    [rango_max]  VARCHAR (20) NULL,
    [mascara]    VARCHAR (20) NULL,
    [interno]    SMALLINT     NOT NULL
);



