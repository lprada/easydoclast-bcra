﻿CREATE TABLE [dbo].[LD_documentos_T] (
    [id_documento]         VARCHAR (50) NOT NULL,
    [fecha_alta]           DATETIME     NULL,
    [fecha_ultimo_ingreso] DATETIME     NULL,
    [estado]               TINYINT      NULL,
    [fecha_reconocimiento] DATETIME     NULL,
    [fecha_control_qa]     DATETIME     NULL,
    [fecha_devolucion]     DATETIME     NULL,
    [caja_proceso]         VARCHAR (15) NOT NULL,
    [caja_devolucion]      VARCHAR (15) NULL,
    [remitoExtraccion]     VARCHAR (50) NULL
);



