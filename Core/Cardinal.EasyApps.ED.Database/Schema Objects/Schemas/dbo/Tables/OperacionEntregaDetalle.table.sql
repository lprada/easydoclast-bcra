﻿CREATE TABLE [dbo].[OperacionEntregaDetalle] (
    [OperacionEntregaDetalleId] INT           IDENTITY (1, 1) NOT NULL,
    [OperacionEntregaId]        INT           NOT NULL,
    [Orden]                     SMALLINT      NOT NULL,
    [Proceso]                   VARCHAR (500) NOT NULL,
    [Parametros]                VARCHAR (500) NOT NULL
);



