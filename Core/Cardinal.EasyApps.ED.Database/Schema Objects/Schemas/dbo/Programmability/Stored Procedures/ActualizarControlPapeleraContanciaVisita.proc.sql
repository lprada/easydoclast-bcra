﻿/*
CREATE PROCEDURE [dbo].[ActualizarControlPapeleraContanciaVisita]
	-- Add the parameters for the stored procedure here
	@codentrega varchar(255) AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update ZCSQBGAB set control = 3
	where cod_lote in (
	select cod_lote from lotescajas where cod_caja in (
	select cod_caja from CajasEntregas where cod_entrega =@codentrega
	)
	)
	and exists (select * from papelera 
			where papelera.cod_lote = ZCSQBGAB.cod_lote and
					papelera.nro_orden = ZCSQBGAB.nro_orden)
END*/

