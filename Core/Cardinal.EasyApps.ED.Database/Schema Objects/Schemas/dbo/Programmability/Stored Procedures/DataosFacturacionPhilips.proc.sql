﻿/*-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE DataosFacturacionPhilips
	-- Add the parameters for the stored procedure here
	@desde datetime , 
	@hasta datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select 'PHILIPS',
(select fechabajada from entregas where cod_entrega in 
(select cod_entrega from cajasentregas where cod_caja in 
(select cod_caja from lotescajas where cod_lote = z.cod_lote))),
LOTE_EXO,count(*) as img, 
(select count(distinct cryder_e) from ZANNPBAB z1
where z1.cod_lote = z.cod_lote and z1.LOTE_EXO = z.LOTE_EXO ) as doc
from ZANNPBAB z
left join papelera p on z.cod_lote = p.cod_lote and z.nro_orden = p.nro_orden 
where p.cod_lote is null
and z.cod_lote in (
select cod_lote from lotescajas where cod_caja in (
select cod_caja from cajasentregas where cod_entrega in (
select cod_entrega from entregas where (loginempresa ='RYDER' or loginempresa ='EXO') and 
fechabajada >= @desde and  fechabajada <=@hasta)))
group by LOTE_EXO,z.cod_lote

END*/
